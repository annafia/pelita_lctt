<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 2em">
  <div class="container">

    <div class="row">
      
      <div class="col-md-12">
        <div class="col-md-12 lokasi" style="background-color: ">
          <?php echo $data_tour->negara ?> - <?php echo $data_tour->benua; ?>
        </div>
        <div class="col-md-12 nama-paket" style="background-color: ">
          <?php echo $data_tour->nama_paket_tour ?>
        </div>
        <div class="col-md-12">               
          <div class="col-md-12 line" style="padding: 0px;"></div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-calendar-o"></i> <?php echo date('d M Y', strtotime($data_tour->tanggal_keberangkatan)); ?>
          </div>
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-bell-o"></i> <?php echo $data_tour->total_hari ?> Hari <?php echo $data_tour->total_hari ?> Malam
          </div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <?php echo $data_tour->keterangan; ?>
          </div>
          <div class="col-md-12 detail-pkt">
            <?php if ($data_tour->extra_bed== '2') {
              echo 'No';
            } ?> Extra Bad
          </div>
        </div>
        <div class="col-md-4" style="background-color:">
          <div class="col-md-12 detail-pkt" style="color: #9b9b9b">
            Harga
          </div>
          <div class="col-md-12 detail-pkt" style="color: #0f4471">
            Rp. <?php echo number_format($data_tour->harga); ?>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-12 prog-bar">
            <div class="col-md-2 col-md-offset-3 bar-text" style="color: #ff6107;">
              Isi data <i class="fa fa-arrow-right"></i>
            </div>
            <div class="col-md-2 bar-text">
              Pembayaran <i class="fa fa-arrow-right"></i>
            </div>
            <div class="col-md-2 bar-text">
              Selesai
            </div>
          </div>
        </div>
        <div class="col-md-10 col-md-offset-1 judul-isi" style="background-color: ;">
          Data Pemesan
        </div>
        <div class="col-md-10 col-md-offset-1 form-pemesan">
          
          <div class="col-md-12 row-edit">
            <div class="col-md-12">
              <label>Nama Pemesan</label>
              <input type="text" value="<?php echo $data_pemesan->nama ?>" class="form-control" name="" style="height: 36px;" disabled>
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Email</label>
              <input type="text" class="form-control" disabled="" value="<?php echo $data_pemesan->id_member ?>" style="height: 36px;" placeholder="">
            </div>
            <div class="col-md-4">
              <label>No. Telpon</label>
              <input type="text" disabled="" class="form-control" value="<?php echo $data_pemesan->no_hp ?>" style="height: 36px;" placeholder="">
            </div>
          </div>
        </div>
        <form method="POST">
        <div class="col-md-10 col-md-offset-1 judul-isi" style="background-color: ;">
          Data Tamu
        </div>
        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <?php for ($i=0; $i < $data_tour->dewasa; $i++) { 
           ?>
          <div class="col-md-12 row-edit">
            <div class="col-md-2 tipe-tamu">
              Dewasa
            </div>
          </div>
         
          <div class="col-md-12 row-edit">
            <div class="col-md-6">
              <label>Nomor Identitas</label>
              <input type="text" class="inText" name="nomor_id[]" required="" style="height: 36px;" placeholder="Nomor ID (KTP, SIM, Passport)">
            </div>
            <div class="col-md-6">
              <label>Nama Lengkap</label>
              <input type="text" class="inText" required="" name="nama_lengkap[]" style="height: 36px;" placeholder="Nama Lengkap">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Tanggal Lahir</label>
              <input type="date" class="inText" required="" name="tanggal_lahir[]" style="height: 36px;" placeholder="">
            </div>
            <div class="col-md-8">
              <label>Nomor Hp/Telp</label>
              <input type="text" class="inText" required="" name="no_hp[]" style="height: 36px;" placeholder="Nomor Hp/Telp">
            </div>
          </div>
          
          <div class="col-md-12 row-edit">
            <div class="col-md-12">
              <div class="col-md-12 line-form"></div>
            </div>                  
          </div>
        <?php } ?>
          <div class="col-md-12 row-edit">
            <div class="col-md-2 tipe-tamu">
              Anak
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-6">
              <label>Nomor Identitas</label>
              <input type="text" class="inText" required="" name="nomor_id[]" style="height: 36px;" placeholder="Nomor ID (KTP, SIM, Passport)">
            </div>
            <div class="col-md-6">
              <label>Nama Lengkap</label>
              <input type="text" class="inText" required="" name="nama_lengkap[]" style="height: 36px;" placeholder="Nama Lengkap">
            </div>
          </div>
          <div class="col-md-12 row-edit">
             <div class="col-md-4">
              <label>Tanggal Lahir</label>
              <input type="date" class="inText" required="" name="tanggal_lahir[]" style="height: 36px;" placeholder="">
            </div>
          </div>
        </div>

        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <div class="col-md-12 row-edit">
            <div class="col-md-10">
              <font style="font-weight: 900; color: #9b9b9b;">
                <input type="checkbox" name="sk" value="1" required="">
                Saya menyetujui seluruh
                <a href="#" style="color: #0f4471;">Syarat dan Ketentuan</a>
                yang berlaku
              </font>
              
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-12" style="text-align: center;">
              <a href="#popup">
                <button href="#popup" type="button" class="btn-simpan">Selanjutnya</button>
              </a>                    
            </div>
          </div>
        </div>
        <div id="popup">
          <div class="window">
            <a href="#" class="close-button" title="Close">X</a>
            <div class="row">
              <div class="col-md-12" style="background-color: ;">
                <div class="col-md-12 popup-1">
                  Konfirmasi Data Anda
                </div>
                <div class="col-md-12 popup-2">
                  E-Voucer akan di kirim ke alamat email. pastikan bahwa data di isi dengan benar
                </div>
                <div class="col-md-12 box-konfirmasi">
                  <div class="col-md-12 popup-3">
                    Data Pemesan
                  </div>
                  <div class="col-md-12 popup-4">                   
                    <?php echo $data_pemesan->nama ?> People Email : <?php echo $data_pemesan->id_member; ?>
                  </div>
                  <div class="col-md-12 popup-4"> 
                    Nomor Telepone : <?php echo $data_pemesan->no_hp ?>
                  </div>
                </div>
                <div class="col-md-12" style="text-align: center;">
                  <button type="submit" class="btn-simpan">Selanjutnya</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">               
        </div>
      </div>
      </form>
    </div>
  </div>
</div>



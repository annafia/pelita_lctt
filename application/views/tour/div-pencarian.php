<?php function custom_echo($x, $length)
{
	if(strlen($x)<=$length)
	{
		echo $x;
	}
	else
	{
		$y=substr($x,0,$length) . '...';
		echo $y;
	}
} ?>

<div id="HasilPencarian">
	<?php 
	if ($paket_tour != null) {			# code...
		foreach ($paket_tour as $key) {
			?>
			<div class="col-md-4 box-pkt-tour">
				<a href="<?php echo site_url() ?>/Tour/detailtour/<?php echo $key->id_paket_tour; echo '/'; echo$key->tanggal_keberangkatan ?>">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo $key->gambar_paket ?>">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							<?php echo $key->negara; ?>, <?php echo $key->benua; ?>
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							<?php echo $key->total_hari ?> Hari <?php echo $key->total_hari ?> Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							<?php custom_echo($key->nama_paket_tour, 18) ?>
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. <?php echo number_format($key->harga, 2, ",", "."); ?>
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							<!-- <?php echo $key->jml_keberangkatan; ?> Keberangkatan -->
							<?php echo date('d F Y', strtotime($key->tanggal_keberangkatan)); ?>
						</div>
					</div>
				</a>
			</div>
			<?php 
		}
	}else{ ?>
		<center>
			<i class="fa fa-ban fa-4x" style="color: #ff6107;"></i>
			<h3 style=" color: #083358;">Pencarian tidak tersedia</h3>
		</center>
	<?php } ?>
</div>
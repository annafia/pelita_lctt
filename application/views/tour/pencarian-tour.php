<?php function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
} ?>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: unset; height: 200px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="padding: 0px 30px;">
				<div class="col-md-12 box-cari-pkt">
					<div class="col-md-12 box-cari-pkt1">
						Cari Paket Tour ke Tempat Favorite
					</div>
					<div class="col-md-12 box-cari-pkt2">
						<div class="col-md-3">
							<i class="fa fa-map-marker in-icon"></i>
							<label>
								Masukan Benua
							</label>
							<select class="in-cari-pkt-negara" name="benua" id="benua" onchange="find_negara()">
								<option value="0">Semua Benua</option>
								<?php foreach ($benua as $key) {?>
									<option value="<?php echo $key->id_benua ?>"><?= strtoupper($key->benua) ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-3">
							<i class="fa fa-map-marker in-icon"></i>
							<label>
								Masukan Negara
							</label>
							<select class="in-cari-pkt-negara" name="negara" id="negara">
								<option value="0">Semua Negara</option>
								<!-- <?php foreach ($negara as $key) {?>
									<option value="<?php echo $key->id_negara ?>"><?php echo strtoupper($key->negara); ?></option>
								<?php } ?> -->
							</select>
						</div>
						<div class="col-md-3">									
							<i class="fa fa-calendar-o in-icon"></i>
							<label>
								Tanggal Keberangkatan
							</label>
							<!-- <input type="date" class="in-cari-pkt-date" name="tanggal" id="tanggal" placeholder="Pilih Tanggal Keberangkatan"> -->
							<input name="daterange" type="text" class="in-cari-pkt-date" id="tanggal">
						</div>
						<div class="col-md-3" style="vertical-align: middle;">
							<button class="btn-cari-pkt" onclick="cari_paket()">
								<i class="fa fa-search"></i>
								Cari Paket
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 8em">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12 itinerary">
					Hasil Pencarian
				</div>
				<div class="col-md-9 hasil-cari">
					
				</div>
				<div class="col-md-3 hasil-cari">
					<select class="filter-cari" id="filter_tampilan" name="filter_tampilan" onchange="cari_paket(this)">
						<option value="desc">Dari Tertinggi</option>
						<option value="asc">Dari Termurah</option>
					</select>
				</div>
				<div id="divPencarian">
					<?php if ($paket_tour != null) {?>
				<div id="HasilPencarian">
				<?php 
				if ($paket_tour != null) {			
					foreach ($paket_tour as $key) {
							 ?>
							<div class="col-md-4 box-pkt-tour">
								<div class="col-md-12 pkt-tour">
									<div class="col-md-12" style="padding: 0px; height: 296px;">
										<!-- <img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket4.jpg"> -->
										<img class="img-pkt" src="<?php echo $key->gambar_paket ?>">
									</div>
									<div class="col-md-8 pkt-tour-dtl1-L">
										<?php echo $key->negara; ?>
									</div>
									<div class="col-md-4 pkt-tour-dtl1-R">
										<?php echo $key->total_hari ?> Hari <?php echo $key->total_hari ?> Malam
									</div>
									<div class="col-md-12 pkt-tour-dtl2">
										<a href="<?php echo site_url() ?>/Tour/detailtour/<?php echo $key->id_paket_tour ?>/<?php echo $key->tanggal_keberangkatan ?>">
											<?php custom_echo($key->nama_paket_tour, 18) ?>
										</a>
									</div>
									<div class="col-md-12">
										<div class="col-md-12 line"></div>
									</div>
									<div class="col-md-12 pkt-tour-dtl3">
										Harga Mulai
									</div>
									<div class="col-md-7 pkt-tour-dtl4-L">
										Rp. <?php echo number_format($key->harga, 2, ",", "."); ?>
									</div>
									<div class="col-md-5 pkt-tour-dtl4-R">
										<!-- <?php echo $key->jml_keberangkatan; ?> Keberangkatan -->
										<?php echo date('d F Y', strtotime($key->tanggal_keberangkatan)); ?>
									</div>
								</div>
							</div>
					<?php 
					}
				}else{ ?>
					<center>
						<i class="fa fa-ban fa-4x" style="color: #ff6107;"></i>
						<h3 style=" color: #083358;">Pencarian tidak tersedia</h3>
					</center>
				<?php } ?>
				</div>
		
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="popup">
	<div class="window">
		<a href="#" class="close-button" title="Close">X</a>
		<div class="row">
			<div class="col-md-12" style="background-color: ;">
				<div class="col-md-12 popup-1">
					Konfirmasi Data Anda
				</div>
				<div class="col-md-12 popup-2">
					E-Voucer akan di kirim ke alamat email. pastikan bahwa data di isi dengan benar
				</div>
				<div class="col-md-12 box-konfirmasi">
					<div class="col-md-12 popup-3">
						Data Pemesan
					</div>
					<div class="col-md-12 popup-4">										
						Tuan Foster People Email : Foseterpeople.office@gmail.com
					</div>
					<div class="col-md-12 popup-4">	
						Nomor Telepone : 084543234323
					</div>
				</div>
				<div class="col-md-12" style="text-align: center;">
					<button class="btn-simpan">Selanjutnya</button>
				</div>
			</div>
		</div>
		
	</div>
</div>
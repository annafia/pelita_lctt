<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding: 2em 0px">
	<div class="container">
		<div class="row">				
			<div class="col-md-12">
				<div class="col-md-12 lokasi" style="background-color:;">
					<?php echo $data_tour->negara ?> - <?php echo $data_tour->benua ?>
				</div>
				<div class="col-md-6 nama-paket" style="background-color:">
					<?php echo $data_tour->nama_paket_tour ?>
				</div>
				<div class="col-md-6 nama-paket" style="text-align: right;">
					<a href="#popup">						
						<button class="btn-plh-periode">Detail Informasi</button>
					</a>
				</div>
				<?php foreach ($harga_tour as $key ) {
					?>	
					<div class="col-md-12" style="padding: unset;">	
						<div class="col-md-12">								
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-3" style="background-color: ">
							<div class="col-md-12 detail-pkt">
								<i class="fa fa-calendar-o"></i> <?php echo date("d M Y", strtotime($key->tanggal_keberangkatan)); ?>
							</div>
							<div class="col-md-12 detail-pkt">
								<i class="fa fa-bell-o"></i> <?php echo $data_tour->total_hari ?> Hari 
							</div>
						</div>
						<div class="col-md-6" style="background-color: ">
							<div class="col-md-12 detail-pkt">
								<?php echo $key->keterangan; ?>
							</div>
							<div class="col-md-12 detail-pkt">
								<?php if ($key->extra_bed == '2') {?>
									No Extra bed
								<?php }else{ 
									echo "Extra bed";
								} ?>

							</div>
						</div>
						<div class="col-md-3" style="background-color:">
							<div class="col-md-12 detail-pkt" style="color: #9b9b9b">
								Harga
							</div>
							<div class="col-md-12 detail-pkt" style="color: #0f4471">
								Rp. <?php echo number_format($key->harga) ?>
							</div>
							<div class="col-md-12">
								<?php if ($userdata== null) {
									?>
									<a class="show">
										<button href="#popup" class="btn-psn-tour">Pesan Tour</button>
									</a>
								<?php }else{ ?>
									<a href="<?php echo site_url() ?>/Tour/booking/<?php echo $key->id_jadwal_tour; echo '/'; echo $key->id_harga_tour ?>">
										<button href="#popup" class="btn-psn-tour">Pesan Tour</button>
									</a>
								<?php } ?>
							</div>
						</div>	
					</div>
				<?php } ?>	
			</div>				
		</div>
	</div>
</div>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #ececec; padding: 0em 0px;">
	<div class="container">
		<div class="row">						
			<div class="col-md-12">
				<div class="col-md-2 menu-dtl-tour">
					Galery Foto
				</div>
				<div class="col-md-2 menu-dtl-tour">
					Itinerary
				</div>
				<div class="col-md-2 menu-dtl-tour">
					Syarat dan Ketentuan
				</div>
			</div>						
		</div>
	</div>
</div>

<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/slider2/engine1/style.css" />
<script type="text/javascript" src="<?php echo base_url() ?>/assets/css/slider2/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 2em">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<!-- Start WOWSlider.com BODY section -->
					<div id="wowslider-container1">
						<div class="ws_images">
							<ul>
								<?php foreach ($gambar_tour as $g1): ?>
								<li>
									<img style="max-height: 620px" src="<?php echo $g1->gambar_paket ?>"  title="c9hk3-main" id="wows1_0"/>
								</li>
								<?php endforeach ?>

								<!-- <li>
									<img style="max-height: 620px" src="<?php echo base_url() ?>/assets/images/paket1.jpg"  title="c9hk3-main" id="wows1_0"/>
								</li>
								<li>
									<img style="max-height: 620px" src="<?php echo base_url() ?>/assets/images/paket2.jpg"  title="fff" id="wows1_1"/>
								</li>
								<li>
									<img style="max-height: 620px" src="<?php echo base_url() ?>/assets/images/paket1.jpg"  title="hdr1" id="wows1_2"/>
								</li> -->
							</ul>
						</div>
						<div class="ws_bullets">
							<div>
								<?php foreach ($gambar_tour as $g2): ?>
								<a href="#" title="c9hk3-main">
									<span><img style="height: 48px; width: 85px" src="<?php echo $g2->gambar_paket ?>" alt="c9hk3-main"/>1</span>
								</a>
								<?php endforeach ?>
								<!-- <a href="#" title="c9hk3-main">
									<span><img style="height: 48px; width: 85px" src="<?php echo base_url() ?>/assets/images/paket1.jpg" alt="c9hk3-main"/>1</span>
								</a>
								<a href="#" title="fff">
									<span><img style="height: 48px; width: 85px" src="<?php echo base_url() ?>/assets/images/paket2.jpg" alt="fff"/>2</span>
								</a>
								<a href="#" title="hdr1">
									<span><img style="height: 48px; width: 85px" src="<?php echo base_url() ?>/assets/images/paket1.jpg" alt="hdr1"/>3</span>
								</a> -->
							</div>
						</div>
						<div class="ws_shadow"></div>
					</div>	
					<script type="text/javascript" src="<?php echo base_url() ?>/assets/css/slider2/engine1/wowslider.js"></script>
					<script type="text/javascript" src="<?php echo base_url() ?>/assets/css/slider2/engine1/script.js"></script>
					<!-- End WOWSlider.com BODY section -->
				</div>
				<?php foreach ($gambar_tour as $g3): ?>
				<div class="col-md-3">
					<img src="<?php echo $g3->gambar_paket ?>" class="foto2">
				</div>
				<?php endforeach ?>
				<!-- <div class="col-md-3">
					<img src="<?php echo base_url() ?>/assets/images/paket1.jpg" class="foto2">
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url() ?>/assets/images/paket2.jpg" class="foto2">
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url() ?>/assets/images/paket1.jpg" class="foto2">
				</div>
				<div class="col-md-3">
					<img src="<?php echo base_url() ?>/assets/images/paket2.jpg" class="foto2">
				</div> -->
				<div class="col-md-12 itinerary">
					Itynerary
				</div>
				<div class="col-md-12 box-itinerary">
					<?php foreach ($itinerary as $key) {
						# code...
						?>
						<div class="col-md-12 box-itinerary-in">
							<div class="col-md-2 col-xs-3 bal" ></div>
							<div class="col-md-10 col-xs-9">
								<font id="ity-hari">Hari Ke <?php echo $key->hari_ke ?></font><br>
								<font id="ity-judul"><?php echo $key->nama_itinerary; ?></font>
							</div>
							<div class="col-md-12 isi-itinerary">
								<?php echo $key->deskripsi ?>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-12 itinerary">
					Syarat dan Ketentuan
				</div>
				<div class="col-md-12 box-snk">
					<?php echo $data_tour->snk; ?>
				</div>
				<div class="col-md-12 itinerary">
					Paket Tour terkait
				</div>
				<?php foreach ($paket_tour as $key) {
					# code...
					?>
					<div class="col-md-4 box-pkt-tour">
						<div class="col-md-12 pkt-tour">
							<div class="col-md-12" style="padding: 0px; height: 296px;">
								<img class="img-pkt" src="<?php echo $key->gambar_paket ?>">
							</div>
							<div class="col-md-8 pkt-tour-dtl1-L">
								<?php echo $key->negara; echo ", "; echo $key->benua; ?>
							</div>
							<div class="col-md-4 pkt-tour-dtl1-R">
								<?php echo $key->total_hari; ?> Hari 
							</div>
							<div class="col-md-12 pkt-tour-dtl2">
								<a href="<?php echo site_url() ?>/Tour/detailtour/<?php echo $key->id_paket_tour ?>">
									<?php echo $key->nama_paket_tour; ?>
								</a>
							</div>
							<div class="col-md-12">
								<div class="col-md-12 line"></div>
							</div>
							<div class="col-md-12 pkt-tour-dtl3">
								Harga Mulai
							</div>
							<div class="col-md-7 pkt-tour-dtl4-L">
								Rp. <?php echo number_format($key->harga, 2, ",", ".") ?>
							</div>
							<div class="col-md-5 pkt-tour-dtl4-R">
								<!-- <?php echo $key->jml_keberangkatan; ?> Keberangkatan -->
								<?php echo date('d F Y', strtotime($key->tanggal_keberangkatan)); ?>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>


<div id="popup">
	<div class="window" style="padding: 32px 5px 32px 32px; width: 70%">
		<a href="#" class="close-button" title="Close">
			<i class="fa fa-close"></i>
		</a>
		<div style="width: 100%; height: 410px; overflow: auto;">
			<div class="row" style="width: 99.9%">
				<div class="col-md-11 popup-periode-judul">
					Detail Informasi Paket Tour
				</div>
				<div class="col-md-12 head-popup-periode">
					<embed src="<?php echo $data_tour->pdf ?>" height="400" style=" width: 100%"></embed>
				</div>
			</div>
		</div>
	</div>
</div>
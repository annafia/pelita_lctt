<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: unset; height: 200px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="padding: 0px 30px;">
				<div class="col-md-12 box-cari-pkt">
					<div class="col-md-12 box-cari-pkt1">
						Cari Paket Tour ke Tempat Favorite
					</div>
					<div class="col-md-12 box-cari-pkt2">
						<div class="col-md-5">
							<i class="fa fa-map-marker in-icon"></i>
							<label>
								Masukan Negara dan Lokasi
							</label>
							<select class="in-cari-pkt-negara" name="negara" id="negara">
								<?php foreach ($negara as $key) {?>
									<option value="<?php echo $id_negara ?>"><?php echo strtoupper($key->nama_benua) ?>, <?php echo strtoupper($key->nama_negara); ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4">									
							<i class="fa fa-calendar-o in-icon"></i>
							<label>
								Tanggal Keberangkatan
							</label>
							<input type="date" class="in-cari-pkt-date" name="tanggal" id="tanggal" placeholder="Pilih Tanggal Keberangkatan">
						</div>
						<div class="col-md-3" style="vertical-align: middle;">
							<button class="btn-cari-pkt" onclick="cari_paket()">
								<i class="fa fa-search"></i>
								Cari Paket
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 8em">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12 itinerary">
					Hasil Pencarian
				</div>
				<div class="col-md-9 hasil-cari">
					Menampilkan 10 Paket Tour
				</div>
				<div class="col-md-3 hasil-cari">
					<select class="filter-cari" id="filter_tampilan" name="filter_tampilan">
						<option value="tertinggi">Dari Tertinggi</option>
						<option value="termurah">Dari Termurah</option>
						<option value="terdekat">Terdekat</option>
					</select>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket4.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket2.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket3.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket3.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket2.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket3.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket4.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket5.png">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
				<div class="col-md-4 box-pkt-tour">
					<div class="col-md-12 pkt-tour">
						<div class="col-md-12" style="padding: 0px; height: 296px;">
							<img class="img-pkt" src="<?php echo base_url() ?>/assets/images/paket3.jpg">
						</div>
						<div class="col-md-8 pkt-tour-dtl1-L">
							Amerika New York
						</div>
						<div class="col-md-4 pkt-tour-dtl1-R">
							2 Hari 2 Malam
						</div>
						<div class="col-md-12 pkt-tour-dtl2">
							Wisata Malam Kota Malang dan Sekitar
						</div>
						<div class="col-md-12">
							<div class="col-md-12 line"></div>
						</div>
						<div class="col-md-12 pkt-tour-dtl3">
							Harga Mulai
						</div>
						<div class="col-md-7 pkt-tour-dtl4-L">
							Rp. 10.000.000
						</div>
						<div class="col-md-5 pkt-tour-dtl4-R">
							10 Keberangkatan
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="popup">
	<div class="window">
		<a href="#" class="close-button" title="Close">X</a>
		<div class="row">
			<div class="col-md-12" style="background-color: ;">
				<div class="col-md-12 popup-1">
					Konfirmasi Data Anda
				</div>
				<div class="col-md-12 popup-2">
					E-Voucer akan di kirim ke alamat email. pastikan bahwa data di isi dengan benar
				</div>
				<div class="col-md-12 box-konfirmasi">
					<div class="col-md-12 popup-3">
						Data Pemesan
					</div>
					<div class="col-md-12 popup-4">										
						Tuan Foster People Email : Foseterpeople.office@gmail.com
					</div>
					<div class="col-md-12 popup-4">	
						Nomor Telepone : 084543234323
					</div>
				</div>
				<div class="col-md-12" style="text-align: center;">
					<button class="btn-simpan">Selanjutnya</button>
				</div>
			</div>
		</div>
		
		
		
	</div>
</div>
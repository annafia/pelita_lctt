<body>
  <div id="fh5co-wrapper">
    <div id="fh5co-page">

      <header id="fh5co-header-section" class="sticky-banner" style="background-color: #083358;">
        <div class="container">
          <div class="nav-header" style="height: 88px">
            <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
            <!-- <a href="" class="merek">TRAVEL WEB</a> -->
            <!-- START #fh5co-menu-wrap -->
            <nav id="fh5co-menu-wrap" role="navigation">
              <ul class="sf-menu" id="fh5co-primary-menu" style="color: white">
                <li class="active"><a href="<?php echo site_url() ?>/Home/">Beranda</a></li>
                <li>
                  <a href="vacation.html" >Tour</a>
                  <ul class="fh5co-sub-menu">
                    <li><a href="<?php echo site_url() ?>/Tour/pencariantour">Paket Tour</a></li>
                    <li><a href="<?php echo site_url() ?>/">Visa/Passport</a></li>
                    <!-- <li><a href="<?php echo site_url() ?>/">Passport</a></li> -->
                  </ul>
                </li>
                <li><a href="<?php echo site_url() ?>/">Info</a></li>
                <li>
                  <a href="vacation.html" >Bantuan</a>
                  <ul class="fh5co-sub-menu">
                    <li><a href="#">Cek Pesanan</a></li>
                    <li><a href="<?php echo site_url() ?>/Faq/faq">FAQ</a></li>
                    <li><a href="<?php echo site_url() ?>/">Kontak Kami</a></li>
                  </ul>
                </li>
                <li>
                  <?php if ($userdata['usermember'] == null) {?>
                    <a class="show">Masuk/Daftar</a>
                    <ul>

                  </ul>
                  <?php }else{ ?>
                     <a href="<?php echo site_url() ?>/UserPage/pointsaya">Akun Member</a>
                  <?php } ?>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <div class="container down-login">
          <div class="row">
            <div class="col-md-offset-9 col-md-3 box-login">
              <div class="col-md-12 box-login1">
                Masuk ke akun anda
              </div>
              <div class="col-md-12 box-login2">
                <form autocomplete="off" method="post" action="<?php echo site_url() ?>/Home/login">
                  <div class="col-md-12">
                    <label>Email</label>
                    <input type="text" name="email" placeholder="Masukan email" autocomplete="off">
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-7">
                      <label>Kata sandi</label>
                    </div>
                    <div class="col-md-5" style="text-align: right;">
                      <label>
                        <a href="" style="color: #0f4471">
                          Lupa Kata sandi?
                        </a>
                      </label>
                    </div>
                    <input type="password" name="password" placeholder="Masukan kata sandi" autocomplete="off">
                  </div>
                  <div class="col-md-12" style="text-align: center;">
                    <button class="btn-login" type="submit">Masuk</button>
                  </div>
                </form>
              </div>              
              <div class="col-md-12 box-login3">
                Tidak memiliki akun?
                <br>
                <font>
                  <a href="<?php echo site_url() ?>/Home/register" style="color: #0f4471">
                    Daftar Sekarang
                  </a>
                </font>
              </div>
            </div>
          </div>
        </div>
      </header>

      <!-- end:header-top -->


<!DOCTYPE html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Pelita LCTT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />


	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>/assets/favicon.ico">

	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Nunito" />
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/bootstrap.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/superfish.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/magnific-popup.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/bootstrap-datepicker.min.css">
	<!-- CS Select -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/cs-select.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/cs-skin-border.css">
	
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style.css">

	<!-- css new -->
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/stylePengisianData.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" !important>

	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/radio-button.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/accordion.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

	<!-- Modernizr JS -->
	<script src="<?php echo base_url() ?>/assets/js/modernizr-2.6.2.min.js"></script>
	
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
<![endif]-->



<!-- <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<style>
.clearfix:after {
	content: ".";
	display: block;
	height: 0;
	clear: both;
	visibility: hidden
}

.clearfix {
	*+height: 1%;
}

.slidercon {
	height: 300px;
	margin:0px auto;
}

.slidercon ul {}

.slidercon ul li {
        /* transition: width .5s, height .5s, top .5s, left .5s;
            -webkit-transition: width .5s, height .5s, top .5s, left .5s;
            */
            /*border:1px solid #fff;*/
        }

        .slidercon ul li img {
        	border-radius: 15px;
        }

        .left,
        .right {
        	cursor: pointer;
        }
    </style> -->
</head>
<footer>
  <div id="footer" style="background-color: #0f4471;">
    <div class="container">
      <div class="row" >
        <div class="col-md-5" style="margin-bottom: 32px;">
          <div class="col-md-12 merek">
            TRAVEL WEB
          </div>
          <div class="col-md-1 col-xs-2" style="padding: unset;">
            <img src="<?php echo base_url() ?>/assets/images/24jam.png" style="width: 45px; height: 41px;">
          </div>
          <div class="col-md-11" >
            <div class="col-md-12 layanan">
              Customer Service 
            </div>
            <div class="col-md-12 layanan2">
              081-535-564-435
            </div>
          </div>
          <div class="col-md-12 foot-judul">
            Hubungkan
          </div>
          <div class="col-md-12 foot-link">
            <a href=""><i class="icon-facebook2"></i> Facebook</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href=""><i class="icon-twitter2"></i> Twitter</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href=""><i class="icon-instagram"></i> Instagram</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href=""><i class="icon-google"></i> Google</a>
          </div>
        </div>
        <div class="col-md-3" style="margin-bottom: 32px;">
          <div class="col-md-12 foot-judul" style="margin-top: unset;">
            Travel Web
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Tentang Kami</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Syarat dan Ketentuan</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Karir</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Kebijakan dan Privasi</a>
          </div>
          <div class="col-md-12 foot-judul">
            Bantuan
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Cara Pemesanan</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Cara Pembayaran</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Hubungi kami</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Tanggapan</a>
          </div>
          <div class="col-md-12 foot-link">
            <a href="">FAQ</a>
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12" style="padding-left: unset;">
            <img src="<?php echo base_url() ?>/assets/images/map.PNG" style="width: 99%">
          </div>
          <div class="col-md-12 foot-judul">
            Lokasi
          </div>
          <div class="col-md-12 foot-link">
            <a href="">Jalan Nogosari No. 05 Tegal Gondo
              <br>
              Malang - Jawa Timur
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #083358; padding: 1.5em;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 copyright">
        Copyright © 2018 Travel Web - Hak Cipta Dilindungi Undang-undang
      </div>
    </div>
  </div>
</div>

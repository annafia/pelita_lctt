			
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/engine1/style.css" />
<script type="text/javascript" src="<?php echo base_url() ?>/assets/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 0px 0px 20px 0px;">
	
	<!-- Start WOWSlider.com BODY section -->
	<div id="wowslider-container1">
		<div class="ws_images">
			<ul>
				<?php foreach ($slider as $key) {
								# code...
					?>
					<li>
						<img src="<?php echo $key->slide ?>" alt="4153760837" title="4153760837" id="wows1_<?php echo $key->id_slider ?>"/>
					</li>
				<?php } ?>
			</ul>
		</div>
		<div class="ws_bullets"><div>
			<a href="#" title="4153760837">
				
			</a>
			<a href="#" title="bg">
				
			</a>
			<a href="#" title="c9hk3-main">
				
			</a>
		</div>
	</div>
	<div class="ws_shadow"></div>
</div>	
<script type="text/javascript" src="<?php echo base_url() ?>/assets/engine1/wowslider.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/assets/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->

</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: unset;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-10 col-md-offset-1 hm-box-tiket">
					<div class="col-md-3 left">
						<a href="">
							<div href="#pesawat" class="col-md-12 hidup">
								<i class="fa fa-plane"></i>
								Tiket Pesawat
							</div>
						</a>
						<a href="">
							<div href="#hotel" class="col-md-12">
								<i class="fa fa-hotel"></i>
								Cari Hotel
							</div>
						</a>
						<a href="">
							<div href="#tour" class="col-md-12">
								<i class="fa fa-bus"></i>
								Paket Tour
							</div>
						</a>
					</div>



					<div class="col-md-9 right">
						<div id="pesawat" class="hm-konten-cari">
							<div class="col-md-12 judul-cari-tiket" style="display: ;">
								Cari Tiket Pesawat Dengan Mudah
							</div>
							<div class="col-md-12 form-cari-tiket" style="display: ;">
								<form>											
									<div class="col-md-3" style="background-color: ">
										<input id="radio1" type="radio" name="radio" value="1" checked="checked">
										<label class="lbl-1"><span><span></span></span>Sekali Jalan</label>
									</div>
									<div class="col-md-9" style="background-color: ">
										<input id="radio1" type="radio" name="radio" value="1">
										<label class="lbl-1"><span><span></span></span>Pulang Pergi</label>
									</div>
									<div class="col-md-6" style="background-color: ">
										<label class="lbl-2">Asal : </label>								
										<span class="icon">
											<i class="fa fa-plane"></i>
										</span>
										<select>
											<option value="" selected="">Jakarta (JKT)</option>
											<option value="">Inggris, United</option>
											<option value="">Indonesia, Jakarta</option>
											<option value="">Malaysia, Kuala Lumpur</option>
										</select>

									</div>
									<div class="col-md-6" style="background-color: ">
										<label class="lbl-2">Tujuan : </label>									
										<span class="icon">
											<i class="fa fa-plane"></i>
										</span>
										<select>
											<option value="" selected="">Singapura (SGP)</option>
											<option value="">Inggris, United</option>
											<option value="">Indonesia, Jakarta</option>
											<option value="">Malaysia, Kuala Lumpur</option>
										</select>
									</div>
									<div class="col-md-3" style="">
										<label class="lbl-2">Tgl. berangkat</label>									
										<span class="icon">
											<i class="fa fa-calendar"></i>
										</span>
										<input type="date" name="">
									</div>
									<div class="col-md-3" style="">
										<label class="lbl-2">Tgl. Pulang : </label>									
										<span class="icon">
											<i class="fa fa-calendar"></i>
										</span>
										<input type="date" name="">
									</div>
									<div class="col-md-6" style="background-color: ">
										<label class="lbl-2">Penumpang : </label>									
										<span class="icon">
											<i class="fa fa-user"></i>
										</span>
										<select>
											<option value="" selected="">1 Dewasa, 1 Anak</option>
											<option value="">Inggris, United</option>
											<option value="">Indonesia, Jakarta</option>
											<option value="">Malaysia, Kuala Lumpur</option>
										</select>
									</div>
									<div class="col-md-6" style="background-color: ">
										<label class="lbl-2">Kelas Keberangkatan : </label>									
										<span class="icon">
											<i class="fa fa-table"></i>
										</span>
										<select>
											<option value="" selected="">Ekonomi AC (EKO)</option>
											<option value="">Inggris, United</option>
											<option value="">Indonesia, Jakarta</option>
											<option value="">Malaysia, Kuala Lumpur</option>
										</select>
									</div>
									<div class="col-md-6" style="text-align: center;">
										<button type="submit" class="btn-cari-tiket-pesawat">
											<i class="fa fa-search"></i>
											Cari Tiket
										</button>
									</div>
								</form>
							</div>
						</div>

						<!-- cari tour -->

						<div id="tour" class="hm-konten-cari">
							<div class="col-md-12 judul-cari-tiket">
								Cari Paket Tour dengan Mudah
							</div>
							<div class="col-md-12 form-cari-tiket">
								<form method="post" action="<?php echo site_url() ?>/Tour/pencariantour">
									<div class="col-md-12" style="background-color: ">
										<label class="lbl-2">Masukan Nama Negara dan Kota : </label>
										<span class="icon">
											<i class="fa fa-map-marker"></i>
										</span>
										<select id="negara" name="negara">
											<?php foreach ($negara as $key) {?>
												<option value="<?php echo $key->id_negara ?>"><?php echo $key->benua; ?>, <?php echo $key->negara; ?></option>	
											<?php }; ?>
										</select>
									</div>
									<div class="col-md-6" style="">
										<label class="lbl-2">Tgl. Keberangkatan</label>									
										<span class="icon">
											<i class="fa fa-calendar"></i>
										</span>
										<input type="date" name="">
									</div>
									<div class="col-md-6" style="text-align: center;">
										<button type="submit" class="btn-cari-hotel">
											<i class="fa fa-search"></i>
											Cari Paket Tour
										</button>
									</div>
								</form>
							</div>
						</div>

						<!-- cari hotel -->

						<div id="hotel" class="hm-konten-cari">
							<div class="col-md-12 judul-cari-tiket" >
								Cari Kamar Hotel dengan Mudah
							</div>
							<div class="col-md-12 form-cari-tiket">
								<form>
									<div class="col-md-9" style="background-color: ">
										<label class="lbl-2">Masukan Nama Negara dan Kota : </label>
										<span class="icon">
											<i class="fa fa-map-marker"></i>
										</span>
										<select>
											<option value="" selected="">Jakarta (JKT)</option>
											<option value="">Inggris, United</option>
											<option value="">Indonesia, Jakarta</option>
											<option value="">Malaysia, Kuala Lumpur</option>
										</select>

									</div>
									<div class="col-md-3" style="background-color: ">
										<label class="lbl-2">Jumlah Kamar </label>									
										<span class="icon">
											<i class="fa fa-key"></i>
										</span>
										<select>
											<option value="" selected="">1</option>
											<option value="">Inggris, United</option>
											<option value="">Indonesia, Jakarta</option>
											<option value="">Malaysia, Kuala Lumpur</option>
										</select>
									</div>
									<div class="col-md-4" style="">
										<label class="lbl-2">Tgl. Check In</label>									
										<span class="icon">
											<i class="fa fa-calendar"></i>
										</span>
										<input type="date" name="">
									</div>
									<div class="col-md-4" style="">
										<label class="lbl-2">Tgl. Check Out : </label>									
										<span class="icon">
											<i class="fa fa-calendar"></i>
										</span>
										<input type="date" name="">
									</div>
									<div class="col-md-4" style="text-align: center;">
										<button type="submit" class="btn-cari-hotel">
											<i class="fa fa-search"></i>
											Cari Hotel
										</button>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>							
			</div>

			<div class="col-md-12 box-mitra">
				<div class="col-md-6 mitra-kiri">
					<div class="col-md-12 mitra-kiri1">
						Mitra Penerbangan
					</div>
					<div class="col-md-12 mitra-kiri2">
						Mitra Resmi Penerbangan Regional dan Internasional
					</div>
					<div class="col-md-12 mitra-kiri3">
						Kami bermitra bersama layanan penerbangan di seluruh dunia 
						untuk memberikan kebutuhan transportasi udara dimanapun
						anda bereda!
					</div>
					<div class="col-md-12">
						<button type="submit" class="btn-simpan">
							Selengkapnya
						</button>
					</div>
				</div>
				<div class="col-md-6 mitra-kanan">
					<div class="col-md-12">
						<img src="<?php echo base_url() ?>/assets/images/mitra.PNG" style="width: 100%">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 54px 0px; height: ">
	<div class="container">
		<div class="row">
			<div class="col-md-12 hm-fitur" style="padding: 0px 30px;">
				<div class="col-md-12 hm-fitur1">
					Berlibur Bersama Kami
				</div>
				<div class="col-md-offset-1 col-md-10 hm-fitur2">
					<div class="col-md-12">
						<div class="col-md-3">
							<img src="<?php echo base_url() ?>/assets/images/fitur1.PNG">
							<br>
							Harga Layanan Termurah dan berkualitas
						</div>
						<div class="col-md-3">
							<img src="<?php echo base_url() ?>/assets/images/fitur2.PNG">
							<br>
							Pelayanan Service Terbaik
						</div>
						<div class="col-md-3">
							<img src="<?php echo base_url() ?>/assets/images/fitur3.PNG">
							<br>
							Paket Perjalanan Terlengkap
						</div>
						<div class="col-md-3">
							<img src="<?php echo base_url() ?>/assets/images/fitur4.PNG">
							<br>
							Jaminan Pembayaran
							Online Aman & Mudah
						</div>
					</div>
				</div>
				<div class="col-md-offset-2 col-md-8 hm-fitur3">
					Travel Web adalah penyedia kebutuhan layanan perjalanan liburan menyenangkan
					bagi pelanggan menciptakan sensasi liburan berbeda bersama kami
					<div class="col-md-12" style="margin-top: 32px">
						<button type="submit" class="btn-simpan">
							Tentang Kami
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 134px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12 hm-penawaran1">
					Penawaran Terbaik
				</div>
				<div class="col-md-12 hm-penawaran2">
					Penawaran liburan terbaik dari travel web
				</div>
				<div class="col-md-10 hm-penawaran-menu">
					<div class="col-md-2">
						<a class="active" href="">
							Paket Tour
						</a>
					</div>
				</div>
				<div class="col-md-2" style="text-align: right;">
					<a href="<?php echo site_url() ?>/Tour/pencariantour" style="color: #0f4471;">
						Lihat Lainya
					</a>
				</div>
				<?php foreach ($rand_paket_tour as $key) {
					?>
					<div class="col-md-6">
						<a href="<?php echo site_url() ?>/Tour/detailtour/<?php echo $key->id_paket_tour; echo '/'; echo$key->tanggal_keberangkatan ?>">
							<div class="col-md-12 hm-penawaran3-left">									
								<img src="<?php echo base_url() ?>/assets/images/paket3.jpg" >

								<div class="col-md-12 hm-penawaran3-text">
									<?php echo $key->nama_paket_tour; ?><br>
									<font style="font-size: 16px;">
										Rp. <?php echo number_format($key->harga); ?>
									</font>
								</div>								
							</div>
						</a>
					</div>
				<?php } ?>
				<?php foreach ($paket_tour as $key) {
					?>
					<div class="col-md-3">
						<a href="<?php echo site_url() ?>/Tour/detailtour/<?php echo $key->id_paket_tour; echo '/'; echo$key->tanggal_keberangkatan ?>">
							<div class="col-md-12 hm-penawaran3-right">		
								<img src="<?php echo base_url() ?>/assets/images/paket1.jpg">
								<div class="col-md-12 hm-penawaran3-text2">
									<?php echo $key->nama_paket_tour ?>
								</div>								
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #083358; padding: 54px 0px; height: ">
	<div class="container">
		<div class="row">
			<div class="col-md-12 get-info">
				<div class="col-md-6 get-info-left">
					<div class="col-md-12 get-info-left1">
						Dapatkan informasi terbaru
					</div>
					<div class="col-md-12 get-info-left2">
						Jadilah orang pertama tahu tentang promo dan 
						berita terbaru dari kami
					</div>
				</div>
				<div class="col-md-6 get-info-right">
					<form>
						<div class="col-md-12">
							<input class="in-get-info" type="text" name="" placeholder="Masukan almat E-mail anda">
							<button class="btn-get-info">Berlangganan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
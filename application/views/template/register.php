      <div id="fh5co-contact" class="fh5co-section-gray" style="padding-top: 2em;">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="kiri">
                <div class="foto1">
                  <div class="lingkaran">
                    <img class="lingkaran" src="<?php echo base_url() ?>/assets/images/avatar3.png">
                  </div>
                </div>              
                <div class="nama">
                  New Member
                </div>
                <div class="p300oint">
                </div>
                <a href="<?php echo site_url() ?>/Home/register">
                  <div class="menu mpan">
                    <div class="text">Registrasi</div>
                  </div>
                </a>
              </div>  
            </div>

            <div class="col-md-8">
              <div class="kanan col-md-12">
                <div class="judul col-md-12" style="text-align: center;">
                  Registrasi member
                </div>
                <form action="<?php echo site_url() ?>/Home/i_register" enctype="multipart/form-data" method="POST">                  
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-12">
                      <label>Email</label>
                      <input type="email" class="inText" name="email" required="" style="height: 36px;" placeholder="Email">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-6">
                      <label>Nama Depan</label>
                      <input type="text" name="nama_depan" required="" class="inText" style="height: 36px;" placeholder="Nama Depan">
                    </div>
                    <div class="col-md-6">
                      <label>Nama Belakang</label>
                      <input type="text" class="inText" required="" name="nama_belakang" style="height: 36px;" placeholder="Nama Belakang">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-12">
                      <label>Foto Profile</label>
                      <input type="file" required="" name="userfile" size="20" />
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-12">
                      <label>Alamat</label>
                      <input type="text" class="inText" required="" name="alamat" style="height: 36px;" placeholder="Masukkan Alamat">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-4">
                      <label>Kota</label>
                      <input type="text" class="inText" required="" name="kota" style="height: 36px;" placeholder="Masukkan kota">
                    </div>
                     <div class="col-md-6">
                      <label>Tanggal Lahir</label>
                      <input type="date" required="" class="inText" name="tanggal_lahir" style="height: 36px;" >
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-6">
                      <label>Password</label>
                      <input type="password" required="" class="inText" name="password" style="height: 36px;" placeholder="Password">
                    </div>
                    <div class="col-md-6">
                      <label>Nomor Telp</label>
                      <input type="text" required="" class="inText" name="no_hp" style="height: 36px;" placeholder="Masukkan Nomor Hp">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="padding-left: unset;">
                    <div class="col-md-6" style="text-align: right;">
                      <a href="<?php echo site_url() ?>/Home">
                        <button type="button" class="btn-batal">Batal</button>
                      </a>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                      <button type="submit" class="btn-simpan">Register</button>
                    </div>
                  </div>
                </form>
              </div>              
            </div>
            
          </div>
        </div>
      </div>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 2em;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 faq-title">
        FAQ
      </div>
    </div>
  </div>
</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding: 2em;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 box-faq">
        <div class="col-md-12 box-faq1">
          Pertanyaan Yang Sering di Ajukan
        </div>
        <div class="bs-acc" style="clear: both;">
          <?php foreach ($jenis_faq as $key) {
          ?>
          <div class="box-faq2">
            <?php echo $key->jenis_faq; ?>
          </div>
          <div class="panel-group" id="accordion">
            <?php foreach ($faq as $row) {
              if ($row->id_jenis== $key->id_jenis) {
            ?>
            <div class="panel panel-default">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $row->id_faq ?>">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <?php echo strtoupper($row->question) ?>
                  </h4>
                </div>
              </a>
              <div id="collapseOne<?php echo $row->id_faq; ?>" class="panel-collapse collapse ">
                <div class="panel-body">
                  <p><?php echo $row->answer; ?></p>
                </div>
              </div>
            </div>
          <?php
              }
           } ?>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>


</div>
<!-- END fh5co-page -->

</div>
<!-- END fh5co-wrapper -->

<?php if($this->session->flashdata('info')) { ?>
    <div class="portfolio-modal modal fade" id="popup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-login">
            <div class="container " style="text-align: center; background: white; margin-top: 200px;">
                <div class="row">
                    <div class="">
                        <div class="modal-body">
                            <div class="close-modal clo1" data-dismiss="modal" style="float: right;
                            margin-right: 30px;">
                            <div class="lr r">
                                <div class="rl r"></div>
                            </div>
                        </div>
                        <div>
                            <h3>Info</h3>
                            <h5><?php echo $this->session->flashdata('info'); ?></h5>
                            
                        </div>
                        <div style="text-align: center;">
                           <button type="button" class="btn btn-default" data-dismiss="modal" style="background:#d9241b; color: white; ">Tutup</button>
                       </div>

                   </div>     
               </div>
           </div>
       </div>
   </div>
</div>

<?php } ?>
<script src="<?php echo base_url() ?>/assets/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo base_url() ?>/assets/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url() ?>/assets/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo base_url() ?>/assets/js/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/sticky.js"></script>

<!-- Stellar -->
<script src="<?php echo base_url() ?>/assets/js/jquery.stellar.min.js"></script>
<!-- Superfish -->
<script src="<?php echo base_url() ?>/assets/js/hoverIntent.js"></script>
<script src="<?php echo base_url() ?>/assets/js/superfish.js"></script>
<!-- Magnific Popup -->
<script src="<?php echo base_url() ?>/assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url() ?>/assets/js/magnific-popup-options.js"></script>
<!-- Date Picker -->
<script src="<?php echo base_url() ?>/assets/js/bootstrap-datepicker.min.js"></script>
<!-- CS Select -->
<script src="<?php echo base_url() ?>/assets/js/classie.js"></script>
<script src="<?php echo base_url() ?>/assets/js/selectFx.js"></script>
<script type="text/javascript" src="<?= base_url() ?>/assets/js/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>/assets/js/daterangepicker.js"></script>

<!-- Main JS -->
<script src="<?php echo base_url() ?>/assets/js/main.js"></script>
<script src="<?php echo base_url() ?>/assets/js/Carousel.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.slidercon').carousel({
			num: 5,
			maxWidth: 640,
			maxHeight: 310,
			distance: 50,
			scale: 0.6,
			animationTime: 1000,
			showTime: 4000
		});
	});
</script>
<!-- Date Range Picker -->
<script type="text/javascript">
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left',
            locale: {format: 'YYYY-MM-DD'}
            
        });
    });
</script>

<?php if (isset($ajax)): ?>
    <?php echo $ajax; ?>
<?php endif ?>

<!-- Pencarian Paket Tour -->

<script type="text/javascript">
	function cari_paket() {
        var benua =$('#benua').val();
        var negara =$('#negara').val();
        // var tanggal = $('#tanggal').val();
        var start = $('#tanggal').val().substring(0,10);
        var end = $('#tanggal').val().substring(13,24);
        var filter_tampilan = $('#filter_tampilan').val();

        $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>/Tour/getPencarian",
            data: "&benua="+benua+"&negara="+negara+"&start="+start+"&end="+end+"&filter_tampilan="+filter_tampilan,
            // data: "negara="+negara,
            success:  function(data) {
                console.log(data);
                $('#HasilPencarian').remove();
                $("#divPencarian").append(data);
            },
            error: function(x, t, m) {
            }
        });
    }
    function find_negara(){
        var benua =$('#benua').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>/Tour/find_negara",
            data: "&benua="+benua,
            success:  function(data) {
                // console.log(data);
                var negara = '<option value="0">Semua Negara</option>';
                for(var i=0; i<data.length; i++){
                    negara += "<option value='"+data[i]['id_negara']+"'>"+data[i]['negara']+"</option>";
                }
                $("#negara").html(negara);
            },
            error: function(x, t, m) {
            }
        });
    }
    window.onload = function() {
        getdatapencarian();
    }
    function getdatapencarian() {
        $("#divPencarian").load("<?php echo site_url() ?>/Tour/getPencarian");
    }
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.show').click(function(){
			$('.down-login').toggle();
			$('.show').css('color','#ff6107');
		});
		$(document).mouseup(function(e){
			var subject = $(".down-login"); 
			if(e.target.id != subject.attr('id') && !subject.has(e.target).length){
				subject.hide();
				$('.show').css('color','white');
			}
		});
	});
	$(document).ready(function(){

	});
</script>

<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function () {
        document.getElementById("uploadFile").value = this.files[0].name;
    };
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#paspor').fadeIn('slow');
        $('.nav-tab li a').click(function(){
            $('.nav-tab li a').removeClass('onactv');
            $(this).addClass('onactv');
            $('.tab-konten').hide();
            var onactv = $(this).attr('href');
            $(onactv).fadeIn('slow');
            return false;
        });
        $('.wraper-form').children(':nth-child(3)').hide();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#hotel, #tour').hide();
        $('.hm-box-tiket .left a div').click(function(){
            $('.hm-box-tiket .left a div').removeClass('hidup');
            $(this).addClass('hidup');
            $('.hm-konten-cari').hide();
            var hidup = $(this).attr('href');
            $(hidup).fadeIn('slow');
            return false;
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.close-button').click(function(){
            $('#popup').css('visibility','hidden');
        });
    });
    $(document).ready(function(){

    });
    $(window).on('load',function(){
        $('#popup').css('visibility','visible');
    });
</script>
<!--  <?php if($this->session->flashdata('info')) { ?>
     <script type="text/javascript">
        $('#popup').modal('show');
    </script>
    <?php } ?> -->
    <!--Start of Zendesk Chat Script-->
    <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
                _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                $.src="https://v2.zopim.com/?5vZEAfZuMD1SeYDGBuuDJgLTjH581Qy4";z.t=+new Date;$.
                type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
            </script>
            <!--End of Zendesk Chat Script-->

            <script src="http://pabrikteknologi.com/pelita_travel/assets/plugins/input-mask/jquery.inputmask.js"></script>
            <script src="http://pabrikteknologi.com/pelita_travel/assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
            <script type="text/javascript">
                $(".mask_money").inputmask("999.999.999", { numericInput: true });
            </script>


        </body>

        </html>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 2em">
  <div class="container">

    <div class="row">

      <div class="col-md-12">
        <div class="col-md-12 lokasi" style="background-color: ">
          RIncian Pemesanan
        </div>
        <div class="col-md-12 nama-paket" style="background-color: ">
          Paspor
        </div>
        <div class="col-md-12">               
          <div class="col-md-12 line" style="padding: 0px;"></div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-book"></i> E-Paspor
          </div>
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-user"></i> <?php echo $pDewasa; ?> Dewasa <?php if ($pAnak != 0) { echo $pAnak." Anak"; } ?> <?php if ($pBayi != 0) { echo $pBayi." Bayi"; } ?>
          </div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <?php echo $durasi; ?> Hari Kerja
          </div>
          <div class="col-md-12 detail-pkt">
            <?php echo $imigrasi->kantor_imigrasi; ?>
          </div>
        </div>
        <div class="col-md-4" style="background-color:">
          <div class="col-md-12 detail-pkt" style="color: #9b9b9b">
            Harga
          </div>
          <div class="col-md-12 detail-pkt" style="color: #0f4471">
            Rp. <?php echo number_format($harga, 2, ",", "."); ?>
          </div>
        </div>

        <form action="<?php echo site_url() ?>/VisaPaspor/addPaspor" method="POST">
        <input type="hidden" name="id_tipe_paspor" value="<?php echo $id_tipe_paspor ?>">
        <div class="col-md-12">
          <div class="col-md-12 prog-bar">
            <div class="col-md-2 col-md-offset-3 bar-text" style="color: #ff6107;">
              Isi data
            </div>
            <div class="col-md-2 bar-text">
              Pembayaran
            </div>
            <div class="col-md-2 bar-text">
              Selesai
            </div>
          </div>
        </div>
        <div class="col-md-10 col-md-offset-1 judul-isi" style="background-color: ;">
          Data Pemesan
        </div>
        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <div class="col-md-12 row-edit">
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="genderPem" value="Mr. ">
              <label class="lbl-1"><span><span></span></span>Tuan</label>
            </div>
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="genderPem" value="Mrs. ">
              <label class="lbl-1"><span><span></span></span>Nyonya</label>
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-6">
              <label>Nama Depan</label>
              <input type="text" class="inText" name="ndPem" style="height: 36px;" placeholder="Nama Depan">
            </div>
            <div class="col-md-6">
              <label>Nama Belakang</label>
              <input type="text" class="inText" name="nbpem" style="height: 36px;" placeholder="Nama Belakang">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-6">
              <label>Email</label>
              <input type="text" class="inText" name="email" style="height: 36px;" value="<?php echo $userdata['usermember'] ?>" placeholder="Email" readonly>
            </div>
            <div class="col-md-6">
              <label>No. Telpon</label>
              <input type="text" class="inText" name="nohp" style="height: 36px;" placeholder="No Telepon">
            </div>
          </div>
        </div>
        <div class="col-md-10 col-md-offset-1 judul-isi">
          Data Calon Pemilik Dokumen
        </div>
        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <?php for ($i=0; $i < $pDewasa; $i++) { ?>
          <input type="hidden" name="id_umur_paspor" value="1">
          <input type="hidden" name="id_harga_paspor[]" value="<?php echo $dataDewasa->id_harga_paspor ?>">
          <div class="pas-Dewasa wraper-form">
            <div class="col-md-12 row-edit">
              <div class="col-md-8 tipe-tamu">
                #Dewasa <?php echo $i+1; ?>
              </div>
              <div class="col-md-4" style="padding: unset;">
                <div class="col-md-12">
                  Kriteria Paspor
                </div>
                  <!-- <div class="col-md-7">
                    <input type="radio" name="perpanjangan[]" value="1" checked=""
                    onclick="$(this).parent().parent().parent().parent().parent().children(':nth-child(2)').fadeIn('slow'); $(this).parent().parent().parent().parent().parent().children(':nth-child(3)').fadeOut('fast');">
                    <label class="lbl-1"><span><span></span></span>Perpanjangan</label>
                  </div>
                  <div class="col-md-5">
                    <input type="radio" name="perpanjangan[]" value="0"  onclick="$(this).parent().parent().parent().parent().parent().children(':nth-child(3)').fadeIn('slow'); $(this).parent().parent().parent().parent().parent().children(':nth-child(2)').fadeOut('fast');">
                    <label class="lbl-1" ><span><span></span></span>Baru</label>
                  </div> -->
                  <select name="perpanjangan[]" id="">
                    <option value="1">Perpanjangan</option>
                    <option value="0">Baru</option>
                  </select>
              </div>
            </div>

            <div class="tab-konten-pas">
              <div class="col-md-12 row-edit">
                <!-- <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mr. ">
                  <label class="lbl-1"><span><span></span></span>Tuan</label>
                </div>
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mrs. ">
                  <label class="lbl-1"><span><span></span></span>Nyonya</label>
                </div> -->
                <select name="gender[]" id="">
                  <option value="Mr. ">Tuan</option>
                  <option value="Mrs. ">Nyonya</option>
                </select>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-8">
                  <label>Nama Lengkap</label>
                  <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
                </div>
                <div class="col-md-4">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="inText" name="tgl[]">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-4">
                  <label>Kewarganegaraan</label>
                  <!-- <input type="text" class="inText" name="kw[]"  placeholder=""> -->
                  <select name="kw[]" id="" class="form-control">
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label>Nomor Paspor</label>
                  <input type="text" class="inText" name="nopas[]"  placeholder="Nomor Paspor">
                </div>
                <div class="col-md-4">
                  <label>Negara Penerbit Paspor</label>
                  <!-- <input type="text" class="inText" name="npp[]"  placeholder="Negara Penerbit Paspor"> -->
                  <select name="npp[]" id="" class="form-control">
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-4">
                  <label>Tanggal Dibuat Paspor</label>
                  <input type="date" class="inText" name="intgl[]">
                </div>
                <div class="col-md-4">
                  <label>Tanggal Kadaluarsa Paspor</label>
                  <input type="date" class="inText" name="extgl[]">
                </div>
                <div class="col-md-4">
                  <label>Domisili</label>
                  <input type="text" class="inText" name="domisili[]"  placeholder="Negara Penerbit Paspor">
                </div>
              </div>
            </div>

            <!-- <div class="tab-konten-pas">
              <div class="col-md-12 row-edit">
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mr. ">
                  <label class="lbl-1"><span><span></span></span>Tuan</label>
                </div>
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mrs. ">
                  <label class="lbl-1"><span><span></span></span>Nyonya</label>
                </div>
                <select name="gender[]" id="">
                  <option value="Tn. ">Tuan</option>
                  <option value="Ny. ">Nyonya</option>
                </select>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-12">
                  <label>Nama Lengkap</label>
                  <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-6">
                  <label>Domisili</label>
                  <input type="text" class="inText" name="domisili[]"  placeholder="Domisili">
                </div>
              </div>
            </div> -->
            <div class="col-md-12">
              <div class="col-md-12 line-form"></div>
            </div>
          </div>
          <?php } ?>

          <?php if ($pAnak != 0): ?>
          <?php for ($i=0; $i < $pAnak; $i++) { ?>
          <input type="hidden" name="id_umur_paspor" value="2">
          <input type="hidden" name="id_harga_paspor[]" value="<?php echo $dataAnak->id_harga_paspor ?>">
          <div class="pas-Dewasa wraper-form">
            <div class="col-md-12 row-edit">
              <div class="col-md-8 tipe-tamu">
                #Anak <?php echo $i+1; ?>
              </div>
              <div class="col-md-4" style="padding: unset;">
                <div class="col-md-12">
                  Kriteria Paspor
                </div>
                  <!-- <div class="col-md-7">
                    <input type="radio" name="perpanjangan[]" value="1" checked=""
                    onclick="$(this).parent().parent().parent().parent().parent().children(':nth-child(2)').fadeIn('slow'); $(this).parent().parent().parent().parent().parent().children(':nth-child(3)').fadeOut('fast');">
                    <label class="lbl-1"><span><span></span></span>Perpanjangan</label>
                  </div>
                  <div class="col-md-5">
                    <input type="radio" name="perpanjangan[]" value="0"  onclick="$(this).parent().parent().parent().parent().parent().children(':nth-child(3)').fadeIn('slow'); $(this).parent().parent().parent().parent().parent().children(':nth-child(2)').fadeOut('fast');">
                    <label class="lbl-1" ><span><span></span></span>Baru</label>
                  </div> -->
                  <select name="perpanjangan[]" id="">
                    <option value="1">Perpanjangan</option>
                    <option value="0">Baru</option>
                  </select>
              </div>
            </div>

            <div class="tab-konten-pas">
              <div class="col-md-12 row-edit">
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mr.">
                  <label class="lbl-1"><span><span></span></span>Tuan</label>
                </div>
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mrs. ">
                  <label class="lbl-1"><span><span></span></span>Nyonya</label>
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-8">
                  <label>Nama Lengkap</label>
                  <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
                </div>
                <div class="col-md-4">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="inText" name="tgl[]">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-4">
                  <label>Kewarganegaraan</label>
                  <!-- <input type="text" class="inText" name="kw[]"  placeholder=""> -->
                  <select name="kw[]" id="" class="form-control">
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label>Nomor Paspor</label>
                  <input type="text" class="inText" name="nopas[]"  placeholder="Nomor Paspor">
                </div>
                <div class="col-md-4">
                  <label>Negara Penerbit Paspor</label>
                  <!-- <input type="text" class="inText" name="npp[]"  placeholder="Negara Penerbit Paspor"> -->
                  <select name="npp[]" id="" class="form-control">
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-4">
                  <label>Tanggal Dibuat Paspor</label>
                  <input type="date" class="inText" name="intgl[]">
                </div>
                <div class="col-md-4">
                  <label>Tanggal Kadaluarsa Paspor</label>
                  <input type="date" class="inText" name="extgl[]">
                </div>
                <div class="col-md-4">
                  <label>Domisili</label>
                  <input type="text" class="inText" name="domisili[]"  placeholder="Negara Penerbit Paspor">
                </div>
              </div>
            </div>

            <!-- <div class="tab-konten-pas">
              <div class="col-md-12 row-edit">
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mr. ">
                  <label class="lbl-1"><span><span></span></span>Tuan</label>
                </div>
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mrs.">
                  <label class="lbl-1"><span><span></span></span>Nyonya</label>
                </div>
                <select name="gender[]" id="">
                  <option value="Tn. ">Tuan</option>
                  <option value="Ny. ">Nyonya</option>
                </select>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-12">
                  <label>Nama Lengkap</label>
                  <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-6">
                  <label>Domisili</label>
                  <input type="text" class="inText" name="domisili[]"  placeholder="Domisili">
                </div>
              </div>
            </div> -->
            <div class="col-md-12">
              <div class="col-md-12 line-form"></div>
            </div>
          </div>
          <?php } ?>
          <?php endif ?>


          <?php if ($pBayi != 0): ?>
          <?php for ($i=0; $i < $pBayi; $i++) { ?>
          <input type="hidden" name="id_umur_paspor" value="2">
          <input type="hidden" name="id_harga_paspor[]" value="<?php echo $dataBayi->id_harga_paspor ?>">
          <div class="pas-Dewasa wraper-form">
            <div class="col-md-12 row-edit">
              <div class="col-md-8 tipe-tamu">
                #Bayi <?php echo $i+1; ?>
              </div>
              <div class="col-md-4" style="padding: unset;">
                <div class="col-md-12">
                  Kriteria Paspor
                </div>
                  <!-- <div class="col-md-7">
                    <input type="radio" name="perpanjangan[]" value="1" checked=""
                    onclick="$(this).parent().parent().parent().parent().parent().children(':nth-child(2)').fadeIn('slow'); $(this).parent().parent().parent().parent().parent().children(':nth-child(3)').fadeOut('fast');">
                    <label class="lbl-1"><span><span></span></span>Perpanjangan</label>
                  </div>
                  <div class="col-md-5">
                    <input type="radio" name="perpanjangan[]" value="0"  onclick="$(this).parent().parent().parent().parent().parent().children(':nth-child(3)').fadeIn('slow'); $(this).parent().parent().parent().parent().parent().children(':nth-child(2)').fadeOut('fast');">
                    <label class="lbl-1" ><span><span></span></span>Baru</label>
                  </div> -->
                  <select name="perpanjangan[]" id="">
                    <option value="1">Perpanjangan</option>
                    <option value="0">Baru</option>
                  </select>
              </div>
            </div>

            <div class="tab-konten-pas">
              <div class="col-md-12 row-edit">
                <!-- <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mr.">
                  <label class="lbl-1"><span><span></span></span>Tuan</label>
                </div>
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mrs. ">
                  <label class="lbl-1"><span><span></span></span>Nyonya</label>
                </div> -->
                <select name="gender[]" id="">
                  <option value="Mr. ">Tuan</option>
                  <option value="Mrs. ">Nyonya</option>
                </select>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-8">
                  <label>Nama Lengkap</label>
                  <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
                </div>
                <div class="col-md-4">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="inText" name="tgl[]">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-4">
                  <label>Kewarganegaraan</label>
                  <!-- <input type="text" class="inText" name="kw[]"  placeholder=""> -->
                  <select name="kw[]" id="" class="form-control">
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <label>Nomor Paspor</label>
                  <input type="text" class="inText" name="nopas[]"  placeholder="Nomor Paspor">
                </div>
                <div class="col-md-4">
                  <label>Negara Penerbit Paspor</label>
                  <!-- <input type="text" class="inText" name="npp[]"  placeholder="Negara Penerbit Paspor"> -->
                  <select name="npp[]" id="" class="form-control">
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-4">
                  <label>Tanggal Dibuat Paspor</label>
                  <input type="date" class="inText" name="intgl[]">
                </div>
                <div class="col-md-4">
                  <label>Tanggal Kadaluarsa Paspor</label>
                  <input type="date" class="inText" name="extgl[]">
                </div>
                <div class="col-md-4">
                  <label>Domisili</label>
                  <input type="text" class="inText" name="domisili[]"  placeholder="Negara Penerbit Paspor">
                </div>
              </div>
            </div>

            <!-- <div class="tab-konten-pas">
              <div class="col-md-12 row-edit">
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mr. ">
                  <label class="lbl-1"><span><span></span></span>Tuan</label>
                </div>
                <div class="col-md-2" style="background-color: ">
                  <input id="radio1" type="radio" name="gender[]" value="Mrs.">
                  <label class="lbl-1"><span><span></span></span>Nyonya</label>
                </div>
                <select name="gender[]" id="">
                  <option value="Tn. ">Tuan</option>
                  <option value="Ny. ">Nyonya</option>
                </select>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-12">
                  <label>Nama Lengkap</label>
                  <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-6">
                  <label>Domisili</label>
                  <input type="text" class="inText" name="domisili[]"  placeholder="Domisili">
                </div>
              </div>
            </div> -->
            <div class="col-md-12">
              <div class="col-md-12 line-form"></div>
            </div>
          </div>
          <?php } ?>
          <?php endif ?>


          
        </div>

        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <div class="col-md-12 row-edit">
            <div class="col-md-10">
              <label>Pilih Type Pembayaran</label>
              <input type="text" class="inText" name="" style="height: 36px;" placeholder="Type pembayaran">
              <font style="font-weight: 900">
                Saya menyetujui seluruh
                <a href="#" style="color: #0f4471;">Syarat dan Ketentuan</a>
                yang berlaku
              </font>
              
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-12" style="text-align: center;">
              <button type="submit" class="btn-simpan">SUBMIT</button>
            </div>
          </div>
        </div>
        </form>

        <div class="col-md-12">               

        </div>

      </div>
      
    </div>
  </div>
</div>

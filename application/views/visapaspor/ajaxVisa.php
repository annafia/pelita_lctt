<script type="text/javascript">
  $('#negara').change(function(){
    $.ajax({
      url: "<?php echo base_url() ?>index.php/VisaPaspor/jsonTipeVisa",
      data: { "id_negara": $("#negara").val() },
      dataType:"json",
      type: "POST",
      success: function(data){
        console.log(data);
        var html1 = '';
        var html2 = '';
        if (data.jenisVisa.length == 0) {
          html1 += '<option value="">Tidak Tersedia</option>';
          html2 += '<option value="">-</option>';
        } else {
          for (i = 0; i < data.jenisVisa.length; i++) {
            html1 += '<option value="'+ data.jenisVisa[i].id_jenis_visa +'">'+ data.jenisVisa[i].jenis_visa +'</option>';
            html2 += '<option value="'+ data.jenisVisa[i].id_visa_umur +'">'+ data.jenisVisa[i].lama_pembuatan +'</option>';
          }
        }
        $('#asu').html(html1);
        $('#durasi').html(html2);

        // var html2 = '';

        // if (data.durasi.length == 0) {
        //   html2 += '<option value="1">Tidak Tersedia</option>';
        // } else {
        //   for (i = 0; i < data.durasi.length; i++) {
        //     html2 += '<option value="'+ data.durasi[i].id_visa_umur +'">'+ data.durasi[i].lama_pembuatan +'</option>';
        //   }
        // }
        // $('#durasi').html(html2);
      }
    });
  });

  $('#asu').change(function(){
    $.ajax({
      url: "<?php echo base_url() ?>index.php/VisaPaspor/jsonDurasi",
      data: { "id_jenis_visa": $("#asu").val() },
      dataType:"json",
      type: "POST",
      success: function(data){
        console.log(data);
        var html2 = '';

        if (data.length == 0) {
          html2 += '<option value="1">Tidak Tersedia</option>';
        } else {
          for (i = 0; i < data.length; i++) {
            html2 += '<option value="'+ data[i].id_visa_umur +'">'+ data[i].lama_pembuatan +'</option>';
          }
        }
        $('#durasi').html(html2);
      }
    });
  });

  $('#imigrasi').change(function(){
    $.ajax({
      url: "<?php echo base_url() ?>index.php/VisaPaspor/jsonImigrasi",
      data: { "imigrasi": $("#imigrasi").val() },
      dataType:"json",
      type: "POST",
      success: function(data){
        console.log(data);
        var html2 = '';

        if (data.length == 0) {
          html2 += '<option value="1">Tidak Tersedia</option>';
        } else {
          for (i = 0; i < data.length; i++) {
            html2 += '<option value="'+ data[i].durasi_proses +'">'+ data[i].durasi_proses +' Hari</option>';
          }
        }
        $('#durasiPaspor').html(html2);
      }
    });
  })
</script>
<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 2em">
  <div class="container">

    <div class="row">
      
      <div class="col-md-12">
        <div class="col-md-12 lokasi" style="background-color: ">
          Rincian Pemesanan
        </div>
        <div class="col-md-12 nama-paket" style="background-color: ">
          Visa
        </div>
        <div class="col-md-12">               
          <div class="col-md-12 line" style="padding: 0px;"></div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-calendar-o"></i> <?php echo $nama_negara; ?><!-- , Tanggal Keberangkatan <?php echo date('d F Y', strtotime($tgl)); ?> -->
          </div>
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-user"></i> <?php echo $dewasa; ?> Dewasa <?php if ($anak) { echo $anak. " Anak"; } ?> <?php if ($bayi) { echo $bayi. " Bayi"; } ?>
          </div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <?php echo $durasi; ?> Hari Kerja
          </div>
          <div class="col-md-12 detail-pkt">
            Visa <?php echo $jenisVisa ?>
          </div>
        </div>
        <div class="col-md-4" style="background-color:">
          <div class="col-md-12 detail-pkt" style="color: #9b9b9b">
            Harga
          </div>
          <div class="col-md-12 detail-pkt" style="color: #0f4471">
            Rp. <?php echo number_format($harga, 2, ",", "."); ?>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-12 prog-bar">
            <div class="col-md-2 col-md-offset-3 bar-text" style="color: #ff6107;">
              Isi data
            </div>
            <div class="col-md-2 bar-text">
              Pembayaran
            </div>
            <div class="col-md-2 bar-text">
              Selesai
            </div>
          </div>
        </div>

        <form method="POST" action="<?php echo site_url() ?>/VisaPaspor/addVisa">
        <input type="hidden" name="tglbrk" value="<?php echo $tgl; ?>">
        <div class="col-md-10 col-md-offset-1 judul-isi" style="background-color: ;">
          Data Pemesan
        </div>
        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <div class="col-md-12 row-edit">
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio[]" value="Mr. ">
              <label class="lbl-1"><span><span></span></span>Tuan</label>
            </div>
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio[]" value="Mrs. ">
              <label class="lbl-1"><span><span></span></span>Nyonya</label>
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-6">
              <label>Nama Depan</label>
              <input type="text" class="inText" name="nd_pemesan" style="height: 36px;" placeholder="Nama Depan">
            </div>
            <div class="col-md-6">
              <label>Nama Belakang</label>
              <input type="text" class="inText" name="nb_pemesan" style="height: 36px;" placeholder="Nama Belakang">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-6">
              <label>Email</label>
              <input type="text" class="inText" name="email_pemesan" style="height: 36px;" placeholder="Email">
            </div>
            <div class="col-md-6">
              <label>No. Telpon</label>
              <input type="text" class="inText" name="nohp_pemesan" style="height: 36px;" placeholder="No Telepon">
            </div>
          </div>
        </div>
        <div class="col-md-10 col-md-offset-1 judul-isi" style="background-color: ;">
          Data Calon Pemilik Dokumen
        </div>
        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <div class="col-md-12 row-edit">
            <div class="col-md-2 tipe-tamu">
              Dewasa
            </div>
          </div>
          <?php for ($i=0; $i < $dewasa; $i++) { ?>
          <div class="col-md-12 row-edit">
            <input type="hidden" name="hargaVisa[]" value="<?php echo $harga_dewasa->id_harga_visa ?>">
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio[]" value="Mr. ">
              <label class="lbl-1"><span><span></span></span>Tuan</label>
            </div>
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio[]" value="Mrs. ">
              <label class="lbl-1"><span><span></span></span>Nyonya</label>
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-8">
              <label>Nama Lengkap</label>
              <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
            </div>
            <div class="col-md-4">
              <label>Tanggal Lahir</label>
              <input type="date" class="inText" name="tgl[]">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Kewarganegaraan</label>
              <input type="text" class="inText" name="kw[]"  placeholder="">
            </div>
            <div class="col-md-4">
              <label>Nomor Paspor</label>
              <input type="text" class="inText" name="nopas[]"  placeholder="Nomor Paspor">
            </div>
            <div class="col-md-4">
              <label>Negara Penerbit Paspor</label>
              <input type="text" class="inText" name="npp[]"  placeholder="Negara Penerbit Paspor">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Tanggal Dibuat Paspor</label>
              <input type="date" class="inText" name="intgl[]">
            </div>
            <div class="col-md-4">
              <label>Tanggal Kadaluarsa Paspor</label>
              <input type="date" class="inText" name="exptgl[]">
            </div>
            <div class="col-md-4">
              <label>Domisili</label>
              <input type="text" class="inText" name="domisili[]"  placeholder="Negara Penerbit Paspor">
            </div>
          </div>
          <?php } ?>

          <?php if ($anak): ?>
          <div class="col-md-12 row-edit">
            <div class="col-md-12">
              <div class="col-md-12 line-form"></div>
            </div>                  
          </div>
          <?php for ($i=0; $i < $anak; $i++) { ?>
          <div class="col-md-12 row-edit">
            <div class="col-md-2 tipe-tamu">
              Anak
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <input type="hidden" name="hargaVisa[]" value="<?php echo $harga_anak->id_harga_visa ?>">
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio[]" value="Mr. ">
              <label class="lbl-1"><span><span></span></span>Tuan</label>
            </div>
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio[]" value="Mrs. ">
              <label class="lbl-1"><span><span></span></span>Nona</label>
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-8">
              <label>Nama Lengkap</label>
              <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
            </div>
            <div class="col-md-4">
              <label>Tanggal Lahir</label>
              <input type="date" class="inText" name="tgl[]">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Kewarganegaraan</label>
              <input type="text" class="inText" name="kw[]"  placeholder="">
            </div>
            <div class="col-md-4">
              <label>Nomor Paspor</label>
              <input type="text" class="inText" name="nopas[]"  placeholder="Nomor Paspor">
            </div>
            <div class="col-md-4">
              <label>Negara Penerbit Paspor</label>
              <input type="text" class="inText" name="npp[]"  placeholder="Negara Penerbit Paspor">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Tanggal Dibuat Paspor</label>
              <input type="date" class="inText" name="intgl[]">
            </div>
            <div class="col-md-4">
              <label>Tanggal Kadaluarsa Paspor</label>
              <input type="date" class="inText" name="exptgl[]">
            </div>
            <div class="col-md-4">
              <label>Domisili</label>
              <input type="text" class="inText" name="domisili[]"  placeholder="Negara Penerbit Paspor">
            </div>
          </div>
          <?php } ?>
          <?php endif ?>

          <?php if ($bayi): ?>
          <div class="col-md-12 row-edit">
            <div class="col-md-12">
              <div class="col-md-12 line-form"></div>
            </div>                  
          </div>
          <?php for ($i=0; $i < $bayi; $i++) { ?>
          <div class="col-md-12 row-edit">
            <div class="col-md-2 tipe-tamu">
              Bayi
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <input type="hidden" name="hargaVisa[]" value="<?php echo $harga_bayi->id_harga_visa ?>">
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio" value="1">
              <label class="lbl-1"><span><span></span></span>Tuan</label>
            </div>
            <div class="col-md-2" style="background-color: ">
              <input id="radio1" type="radio" name="radio" value="2">
              <label class="lbl-1"><span><span></span></span>Nona</label>
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-8">
              <label>Nama Lengkap</label>
              <input type="text" class="inText" name="nama[]" placeholder="Nama Lengkap">
            </div>
            <div class="col-md-4">
              <label>Tanggal Lahir</label>
              <input type="date" class="inText" name="tgl[]">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Kewarganegaraan</label>
              <input type="text" class="inText" name="kw[]"  placeholder="">
            </div>
            <div class="col-md-4">
              <label>Nomor Paspor</label>
              <input type="text" class="inText" name="nopas[]"  placeholder="Nomor Paspor">
            </div>
            <div class="col-md-4">
              <label>Negara Penerbit Paspor</label>
              <input type="text" class="inText" name="npp[]"  placeholder="Negara Penerbit Paspor">
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-4">
              <label>Tanggal Dibuat Paspor</label>
              <input type="date" class="inText" name="intgl[]">
            </div>
            <div class="col-md-4">
              <label>Tanggal Kadaluarsa Paspor</label>
              <input type="date" class="inText" name="exptgl[]">
            </div>
            <div class="col-md-4">
              <label>Domisili</label>
              <input type="text" class="inText" name="domisili[]"  placeholder="Negara Penerbit Paspor">
            </div>
          </div>
          <?php } ?>
          <?php endif ?>
        </div>
        

        <div class="col-md-10 col-md-offset-1 form-pemesan">
          <div class="col-md-12 row-edit">
            <div class="col-md-10">
              <label>Pilih Type Pembayaran</label>
              <input type="text" class="inText" name="" style="height: 36px;" placeholder="Type pembayaran">
              <font style="font-weight: 900">
                Saya menyetujui seluruh
                <a href="#" style="color: #0f4471;">Syarat dan Ketentuan</a>
                yang berlaku
              </font>
              
            </div>
          </div>
          <div class="col-md-12 row-edit">
            <div class="col-md-12" style="text-align: center;">
              <a href="#popup">
                <button href="#popup" class="btn-simpan">Selanjutnya</button>
              </a>                    
            </div>
          </div>
        </div>
        </form>

        <div class="col-md-12">               

        </div>

      </div>
      
    </div>
  </div>
</div>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 6em 0em;">
  <div class="container">
    <div class="row">
    </div>
  </div>
</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 0em">
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="padding: 0px 30px;">
        <div class="col-md-12 box-cari-pkt">
          <div class="col-md-12">
            <ul class="nav-tab">
              <li>
                <a href="#paspor" class="onactv">
                  Paspor
                </a>
              </li>
              <li>
                <a href="#visa" >
                  Visa
                </a>
              </li>
            </ul>
          </div>


          <div id="konten">
            <div id="paspor" class="col-md-12 tab-konten" style="padding: unset;">
              <form action="<?php echo site_url() ?>/VisaPaspor/isidatapaspor" method="POST">
              <div class="col-md-12 box-cari-pkt2">
                <div class="col-md-12 box-cari-pkt1">
                  Cari Paspor dengan Mudah
                </div>
                <div class="col-md-6">
                  <i class="fa fa-book in-icon"></i>
                  <label>
                    Tipe Paspor
                  </label>
                  <select class="in-cari-pkt-negara" name="tipe_paspor" id="tipe_paspor">
                    <?php foreach ($tipe_paspor as $tp): ?>
                    <option value="<?php echo $tp->id_tipe_paspor ?>"><?php echo $tp->tipe_paspor; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-user in-icon"></i>
                  <label>
                    Dewasa
                  </label>
                  <select class="in-cari-pkt-negara" name="pDewasa">
                    <option value="1" selected="">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-user in-icon"></i>
                  <label>
                    Anak
                  </label>
                  <select class="in-cari-pkt-negara" name="pAnak">
                    <option value="0" selected="">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-user in-icon"></i>
                  <label>
                    Bayi
                  </label>
                  <select class="in-cari-pkt-negara" name="pBayi">
                    <option value="0" selected="">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
              </div>

              <div class="col-md-12 box-cari-pkt2">
                <div class="col-md-4">
                  <i class="fa fa-building in-icon"></i>
                  <label>
                    Kantor Imigrasi
                  </label>
                  <select class="in-cari-pkt-negara" name="imigrasi" id="imigrasi">
                    <option value="">Pilih Kantor Imigasi</option>
                    <?php foreach ($imigrasi as $im): ?>
                    <option value="<?php echo $im->id_kantor_imigrasi; ?>"><?php echo $im->kantor_imigrasi; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-4">                  
                  <i class="fa fa-clock-o in-icon"></i>
                  <label>
                    Durasi Proses
                  </label>
                  <select class="in-cari-pkt-negara" name="durasiPaspor" id="durasiPaspor">
                    <!-- <option value="" selected="">-</option> -->
                  </select>
                </div>
                <div class="col-md-4" style="vertical-align: middle;">
                  <button class="btn-cari-pkt">
                    <i class="fa fa-search"></i>
                    Buat Paspor
                  </button>
                </div>
              </div>
              </form>
            </div>

            <!-- visa -->
            <form method="POST" action="<?php echo site_url(); ?>/VisaPaspor/isidatavisa">
            <div id="visa" class="col-md-12 tab-konten" style="padding: unset;">
              <div class="col-md-12 box-cari-pkt2">
                <div class="col-md-12 box-cari-pkt1">
                  Cari Visa dengan Mudah
                </div>
                <div class="col-md-6">
                  <i class="fa fa-book in-icon"></i>
                  <label>
                    Negara Tujuan
                  </label>
                  <select class="in-cari-pkt-negara" name="negara" id="negara">
                    <option value="">Pilih Negara Tujuan</option>
                    <?php foreach ($negara as $n): ?>
                      <option value="<?php echo $n->id_negara ?>"><?php echo $n->negara; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-user in-icon"></i>
                  <label>
                    Dewasa
                  </label>
                  <select class="in-cari-pkt-negara" name="dewasa">
                    <option value="1" selected="">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-user in-icon"></i>
                  <label>
                    Anak
                  </label>
                  <select class="in-cari-pkt-negara" name="anak">
                    <option value="0" selected="">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-user in-icon"></i>
                  <label>
                    Bayi
                  </label>
                  <select class="in-cari-pkt-negara" name="bayi">
                    <option value="0" selected="">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                  </select>
                </div>
              </div>

              <div class="col-md-12 box-cari-pkt2">
                <div class="col-md-3">
                  <i class="fa fa-building in-icon"></i>
                  <label>
                    Tanggal Berangkat
                  </label>
                  <input class="in-cari-pkt-negara" type="date" name="tgl">
                </div>
                <div class="col-md-3">                  
                  <i class="fa fa-clock-o in-icon"></i>
                  <label>
                    Tipe Visa
                  </label>
                  <select class="in-cari-pkt-negara" name="jenis_visa" id="asu">
                    <!-- <?php foreach ($jenis_visa as $j): ?>
                      <option value="<?php echo $j->id_jenis_visa ?>"><?php echo $j->jenis_visa; ?></option>
                    <?php endforeach ?> -->
                  </select>
                </div>
                <div class="col-md-2">                  
                  <i class="fa fa-clock-o in-icon"></i>
                  <label>
                    Durasi Proses
                  </label>
                  <select class="in-cari-pkt-negara" name="durasi" id="durasi">
                    <!-- <?php foreach ($durasi as $d): ?>
                      <option value="<?php echo $d->id_harga_visa ?>"><?php echo $d->lama_pembuatan; ?></option>
                    <?php endforeach ?> -->
                  </select>
                </div>
                <div class="col-md-4" style="vertical-align: middle;">
                  <button class="btn-cari-pkt" type="submit">
                    <i class="fa fa-search"></i>
                    Buat Visa
                  </button>
                </div>
              </div>
            </div>
            </form>
          </div>

        </div>
      </div>
      <style type="text/css">
      .box-cari-pkt{
        margin-top: -110px;
        padding-top: 15px;
      }
      .box-cari-pkt2{
        padding: unset;
      }
      .box-cari-pkt1{
        margin-bottom: 15px;
      }
    </style>

  </div>
</div>
</div>

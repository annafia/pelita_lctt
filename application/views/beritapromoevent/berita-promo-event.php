<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/engine1/style.css" />
<script type="text/javascript" src="<?php echo base_url() ?>/assets/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 0px 0px 20px 0px;">
  
  <!-- Start WOWSlider.com BODY section -->
  <div id="wowslider-container1">
    <div class="ws_images">
      <ul>
        <li>
          <img src="<?php echo base_url() ?>/assets/data1/images/1.png" alt="4153760837" title="4153760837" id="wows1_0"/>
        </li>
        <li><!-- <a href="http://wowslider.net"> -->
          <img src="<?php echo base_url() ?>/assets/data1/images/2.jpg" alt="bootstrap carousel example" title="bg" id="wows1_1"/>
          <!-- </a> -->
        </li>
        <li>
          <img src="<?php echo base_url() ?>/assets/data1/images/3.jpg" alt="c9hk3-main" title="c9hk3-main" id="wows1_2"/>
        </li>
      </ul>
    </div>
    <div class="ws_bullets"><div>
      <a href="#" title="4153760837">
        
      </a>
      <a href="#" title="bg">
        
      </a>
      <a href="#" title="c9hk3-main">
        
      </a>
    </div>
  </div>
  <div class="ws_shadow"></div>
</div>  
<script type="text/javascript" src="<?php echo base_url() ?>/assets/engine1/wowslider.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/assets/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->

</div>


<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 1em;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4 hm-berita">
          <div class="col-md-12 hm-berita1">
            Berita
          </div>
          <?php foreach ($berita as $b): ?>
          <a href="<?php echo site_url() ?>/Info/detailBerita/<?php echo $b->id_berita ?>">
            <div class="col-md-12 berita-wraper">
              <div class="col-md-5 hm-berita2">
                <img src="<?php echo $b->foto ?>" style="width: 100%; height: 100px;">
              </div>
              <div class="col-md-7 hm-berita3">
                <?php echo substr($b->isi_berita,0,100); ?>
              </div>
              <div class="col-md-7 hm-berita4">
                <?php echo date("d F Y", strtotime($b->tanggal_input)); ?>
              </div>
            </div>
          </a>
          <?php endforeach ?>
          <!-- <a href="">
            <div class="col-md-12 berita-wraper">
              <div class="col-md-5 hm-berita2">
                <img src="<?php echo base_url() ?>/assets/images/paket1.jpg" style="width: 100%; height: 100px;">
              </div>
              <div class="col-md-7 hm-berita3">
                Sebuah restoran di Sydney yang menawarkan makanan enak dan lezat paling murah di Australia.
              </div>
              <div class="col-md-7 hm-berita4">
                12-Agustus-2018
              </div>
            </div>
          </a>
          <a href="">
            <div class="col-md-12 berita-wraper">
              <div class="col-md-5 hm-berita2">
                <img src="<?php echo base_url() ?>/assets/images/paket2.jpg" style="width: 100%; height: 100px;">
              </div>
              <div class="col-md-7 hm-berita3">
                Sebuah restoran di Sydney yang menawarkan makanan enak dan lezat paling murah di Australia.
              </div>
              <div class="col-md-7 hm-berita4">
                12-Agustus-2018
              </div>
            </div>
          </a>
          <a href="">
            <div class="col-md-12 berita-wraper">
              <div class="col-md-5 hm-berita2">
                <img src="<?php echo base_url() ?>/assets/images/paket1.jpg" style="width: 100%; height: 100px;">
              </div>
              <div class="col-md-7 hm-berita3">
                Sebuah restoran di Sydney yang menawarkan makanan enak dan lezat paling murah di Australia.
              </div>
              <div class="col-md-7 hm-berita4">
                12-Agustus-2018
              </div>
            </div>
          </a>
          <a href="">
            <div class="col-md-12 berita-wraper">
              <div class="col-md-5 hm-berita2">
                <img src="<?php echo base_url() ?>/assets/images/paket4.jpg" style="width: 100%; height: 100px;">
              </div>
              <div class="col-md-7 hm-berita3">
                Sebuah restoran di Sydney yang menawarkan makanan enak dan lezat paling murah di Australia.
              </div>
              <div class="col-md-7 hm-berita4">
                12-Agustus-2018
              </div>
            </div>
          </a> -->
        </div>
        <div class="col-md-4 hm-promo">
          <div class="col-md-12 hm-promo1">
            Promo
          </div>
          <?php foreach ($promo as $p): ?>
          <a href="<?php echo site_url() ?>/Info/detailPromo/<?php echo $p->id_slider ?>">
            <div class="col-md-12 promo-wraper">
              <div class="col-md-12 hm-promo2">
                <img src="<?php echo $p->slide ?>" style="width: 100%; height: 200px;">
              </div>
              <div class="col-md-12 hm-promo3">
                <?php echo $p->nama_promo; ?>
                <div class="col-md-12 hm-promo4">
                  <!-- 12-Agustus-2018 -->
                </div>
              </div>                  
            </div>
          </a>
          <?php endforeach ?>
          <!-- <a href="">
            <div class="col-md-12 promo-wraper">
              <div class="col-md-12 hm-promo2">
                <img src="<?php echo base_url() ?>/assets/images/paket1.jpg" style="width: 100%; height: 200px;">
              </div>
              <div class="col-md-12 hm-promo3">
                Sebuah restoran di Sydney yang menawarkan makanan enak dan.
                <div class="col-md-12 hm-promo4">
                  12-Agustus-2018
                </div>
              </div>                  
            </div>
          </a>
          <a href="">
            <div class="col-md-12 promo-wraper">
              <div class="col-md-12 hm-promo2">
                <img src="<?php echo base_url() ?>/assets/images/paket2.jpg" style="width: 100%; height: 200px;">
              </div>
              <div class="col-md-12 hm-promo3">
                Sebuah restoran di Sydney yang menawarkan makanan enak dan lezat paling murah di Australia.
                <div class="col-md-12 hm-promo4">
                  12-Agustus-2018
                </div>
              </div>                  
            </div>
          </a> -->
        </div>
        <div class="col-md-4 hm-event">
          <div class="col-md-12 hm-event1">
            Event
          </div>
          <?php foreach ($event as $e): ?>
          <a href="<?php echo site_url() ?>/Info/detailEvent/<?php echo $e->id_event ?>">
            <div class="col-md-12 event-wraper">
              <div class="col-md-4 hm-event2">
                <?php echo date("d", strtotime($e->tanggal_event)); ?><br>
                <?php echo date("F", strtotime($e->tanggal_event)); ?><br>
                <?php echo date("Y", strtotime($e->tanggal_event)); ?>
                <!-- 12<br>April<br>2019 -->
              </div>
              <div class="col-md-8 hm-event3">
                <?php echo $e->judul; ?>
              </div>
            </div>
          </a>
          <?php endforeach ?>
          <!-- <a href="">
            <div class="col-md-12 event-wraper">
              <div class="col-md-4 hm-event2">
                12<br>Juni<br>2019
              </div>
              <div class="col-md-8 hm-event3">
                Garuda indonesia Travel Fair III in Surabaya Indonesia
              </div>
            </div>
          </a>
          <a href="">
            <div class="col-md-12 event-wraper">
              <div class="col-md-4 hm-event2">
                12<br>Januari<br>2019
              </div>
              <div class="col-md-8 hm-event3">
                Garuda indonesia Travel Fair III in Surabaya Indonesia
              </div>
            </div>
          </a>
          <a href="">
            <div class="col-md-12 event-wraper">
              <div class="col-md-4 hm-event2">
                10<br>Mei<br>2019
              </div>
              <div class="col-md-8 hm-event3">
                Garuda indonesia Travel Fair III in Surabaya Indonesia
              </div>
            </div>
          </a> -->
        </div>
      </div>
    </div>
  </div>
</div>
<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 1em;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 dtl-berita1">
          <!-- Senin 12 Juni 2018 17:00 WIB -->
          <?php echo date("D d F Y H:i", strtotime($event->tanggal_input)); ?> WIB
        </div>
        <div class="col-md-12 dtl-berita2">
          Berita/Event/Promo
        </div>
        <div class="col-md-10 dtl-berita3">
          <?php echo $event->judul; ?> (<?php echo date("D d F Y", strtotime($event->tanggal_event)); ?>)
        </div>
        <div class="col-md-12 dtl-berita4">
          <a href=""><i class="icon-facebook3"></i></a>
          <a href=""><i class="icon-twitter2"></i></a>
        </div>
        <div class="col-md-12">
          <img src="<?php echo $event->foto ?>" style="width: 100%;">
        </div>
        <div class="col-md-12 dtl-berita5">
          <!-- Berbuka puasa tak akan lengkap dengan aneka takjil manis yang unik. Tahun ini, untuk menu ramadan, Satoo mengandalkan aneka makanan khas Indonesia yang unik dan beragam. Sebut saja bubur tampah, es selendang mayang, es krim vanila dan baklava disajikan di Satoo restoran, Shangrila Hotel Jakarta. 

          Selain itu, beragam makanan utama seperti kambing guling, nasi kebuli rempah, sop buntut, martabak telur.

          Tak cuma itu, sajian ayam penyet sambal, ayam bakar pecak, aneka sate, dan bakso akan memuaskan rasa lapar Anda setelah seharian berpuasa
          <br><br>
          Berbuka puasa tak akan lengkap dengan aneka takjil manis yang unik. Tahun ini, untuk menu ramadan, Satoo mengandalkan aneka makanan khas Indonesia yang unik dan beragam. Sebut saja bubur tampah, es selendang mayang, es krim vanila dan baklava disajikan di Satoo restoran, Shangrila Hotel Jakarta. 

          Selain itu, beragam makanan utama seperti kambing guling, nasi kebuli rempah, sop buntut, martabak telur.

          Tak cuma itu, sajian ayam penyet sambal, ayam bakar pecak, aneka sate, dan bakso akan memuaskan rasa lapar Anda setelah seharian berpuasa -->
          <p style="text-align: justify;"><?php echo $event->deskripsi; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>

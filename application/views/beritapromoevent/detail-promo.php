<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 1em;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 dtl-berita1">
          <!-- Senin 12 Juni 2018 17:00 WIB -->
          <?php echo date("D d F Y H:i", strtotime($promo->tanggal_input)); ?> WIB
        </div>
        <div class="col-md-12 dtl-berita2">
          Berita/Event/Promo
        </div>
        <div class="col-md-10 dtl-berita3">
          <!-- Restoran Yang Berada Di ketinggiandan lantai berputar 360 derajat menyajikan pemandangan yang indah -->
          <?php echo $promo->nama_promo; ?> (<?php echo date("D d F Y", strtotime($promo->tanggal_promo)); ?>)
        </div>
        <div class="col-md-9" style="padding-left: unset;">
          <div class="col-md-12 dtl-berita4" style="text-align: left;">
            <a href=""><i class="icon-facebook3"></i></a>
            <a href=""><i class="icon-twitter2"></i></a>
          </div>
          <div class="col-md-12">
            <img src="<?php echo $promo->slide ?>" style="width: 100%;">
          </div>
          <div class="col-md-12 dtl-berita5">
            <!-- Berbuka puasa tak akan lengkap dengan aneka takjil manis yang unik. Tahun ini, untuk menu ramadan, Satoo mengandalkan aneka makanan khas Indonesia yang unik dan beragam. Sebut saja bubur tampah, es selendang mayang, es krim vanila dan baklava disajikan di Satoo restoran, Shangrila Hotel Jakarta. 

            Selain itu, beragam makanan utama seperti kambing guling, nasi kebuli rempah, sop buntut, martabak telur.

            Tak cuma itu, sajian ayam penyet sambal, ayam bakar pecak, aneka sate, dan bakso akan memuaskan rasa lapar Anda setelah seharian berpuasa
            <br><br>
            Berbuka puasa tak akan lengkap dengan aneka takjil manis yang unik. Tahun ini, untuk menu ramadan, Satoo mengandalkan aneka makanan khas Indonesia yang unik dan beragam. Sebut saja bubur tampah, es selendang mayang, es krim vanila dan baklava disajikan di Satoo restoran, Shangrila Hotel Jakarta. 

            Selain itu, beragam makanan utama seperti kambing guling, nasi kebuli rempah, sop buntut, martabak telur.

            Tak cuma itu, sajian ayam penyet sambal, ayam bakar pecak, aneka sate, dan bakso akan memuaskan rasa lapar Anda setelah seharian berpuasa -->

            <p style="text-align: justify;"><?php echo $promo->deskripsi; ?></p>
          </div>  
        </div>
        <div class="col-md-3">
          <div class="col-md-12 tour-populer1">
            Tour Populer
          </div>
          <?php foreach ($tour as $t): ?>
          <div class="col-md-12 tour-populer2">
            <a href="">
              <img src="<?php echo $t->gambar_paket ?>" class="img-responsive">
              <div class="col-md-10 col-md-offset-1 tour-populer3">
                <?php echo $t->nama_paket_tour; ?>
                <br>
                <?php echo $t->hari; ?> Hari <?php echo $t->hari; ?> Malam
              </div>
            </a>
          </div>
          <?php endforeach ?>
          <!-- <div class="col-md-12 tour-populer2">
            <a href="">
              <img src="<?php echo base_url() ?>/assets/images/paket1.jpg" class="img-responsive">
              <div class="col-md-10 col-md-offset-1 tour-populer3">
                Paket Tour Pulau Sempu
                <br>
                5 Hari 4 Malam
              </div>
            </a>
          </div>
          <div class="col-md-12 tour-populer2">
            <a href="">
              <img src="<?php echo base_url() ?>/assets/images/paket1.jpg" class="img-responsive">
              <div class="col-md-10 col-md-offset-1 tour-populer3">
                Paket Tour Pulau Sempu
                <br>
                5 Hari 4 Malam
              </div>
            </a>
          </div>
          <div class="col-md-12 tour-populer2">
            <a href="">
              <img src="<?php echo base_url() ?>/assets/images/paket1.jpg" class="img-responsive">
              <div class="col-md-10 col-md-offset-1 tour-populer3">
                Paket Tour Pulau Sempu
                <br>
                5 Hari 4 Malam
              </div>
            </a>
          </div> -->
        </div>              
      </div>
    </div>
  </div>
</div>
<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 2em;">
	<div class="container">
		<div class="row">
			<div class="col-md-6 faq-title">
				Kantor Kami
			</div>
			<div class="col-md-16">
				
			</div>
		</div>
	</div>
</div>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: #0f4471; padding: 0em;">
	<section id="intro" style="margin-top: 0px; padding-top: 0px; background-color: grey;">    
		<div class="row"> 
			<div class="col-md-12">
				<select id="location" style="z-index: 99; position: absolute;">
					<!-- <option value="-7.344495,112.732823">Pelita Malang</option>
					<option value="Kediri">Pelita Kediri</option>
					<option value="lampung">Pelita Sumatra</option>
					<option value="denpasar">Pelita Bali</option> -->
					<option value="">Pilih Cabang</option>
					<?php foreach ($cabang as $c): ?>
						<option value="<?php echo $c->lat_map.",".$c->lang_map ?>"><?php echo $c->nama_cabang; ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div id="map" style="height: 400px;">

			</div>
			<script src="https://maps.googleapis.com/maps/api/js"></script>
			<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRwLue3n8fNEVyc9NE-QcEO9BvJ2NTVbs&callback=initMap"></script>
		</div>  
	</section>
</div>

<script type="text/javascript">
	// var geocoder;
	// var map;
	// var geoMarker;
	// var bounds = new google.maps.LatLngBounds();

	// function initialize() {
	// 	map = new google.maps.Map(
	// 		document.getElementById("map"), {
	// 			center: new google.maps.LatLng(37.4419, -122.2219),
	// 			zoom: 13,
	// 			mapTypeId: google.maps.MapTypeId.ROADMAP
	// 		});
	// 	geoMarker = new google.maps.Marker();
	// 	geoMarker.setPosition(map.getCenter());
	// 	geoMarker.setMap(map);

	// 	$("#location option").each(function() {
	// 		console.log($(this).val());
	// 		geocodeAddress($(this).val());
	// 	});

	// 	$("#location").change(function() {
	// 		var addr = ($('#location :selected').val());
	// 		console.log(addr);
	// 		var geocoder = new google.maps.Geocoder();

	// 		geocoder.geocode({
	// 			'address': addr
	// 		}, function(results, status) {
	// 			if (status == google.maps.GeocoderStatus.OK) {
	// 				if (results[0].geometry.viewport) {
	// 					map.fitBounds(results[0].geometry.viewport);
	// 				} else if (results[0].geometry.bounds) {
	// 					map.fitBounds(results[0].geometry.bounds);
	// 				} else {
	// 					map.setCenter(results[0].geometry.location);
	// 				}
	// 			} else {
	// 				alert("Error geocoding address: " + address + "status=" + status);
	// 			}
	// 		});
	// 	});

	// }

	// google.maps.event.addDomListener(window, "load", initialize);

	// function geocodeAddress(address) {
	// 	var geocoder = new google.maps.Geocoder();
	// 	geocoder.geocode({
	// 		'address': address
	// 	}, function(results, status) {
	// 		if (status == google.maps.GeocoderStatus.OK) {
	// 			var marker = new google.maps.Marker({
	// 				position: results[0].geometry.location,
	// 				map: map,
	// 				title: address
	// 			});
	// 			bounds.extend(results[0].geometry.location);
	// 			map.fitBounds(bounds);
	// 		} else {
	// 			alert("Error geocoding address: " + address + "status=" + status);
	// 		}
	// 	});
	// }



	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 13,
			// center: {lat: -6.8968448, lng: 107.5909641}
			center: {lat: <?php echo $cabang[0]->lat_map; ?>, lng: <?php echo $cabang[0]->lang_map; ?>}
		});
		var geocoder = new google.maps.Geocoder;
		var infowindow = new google.maps.InfoWindow;

		$("#location").change(function() {
			var addr = ($('#location :selected').val());
			if (addr != "") {
				geocodeLatLng(geocoder, map, infowindow, addr);
				// alert(addr);	
			}
		});

	}

	function geocodeLatLng(geocoder, map, infowindow, addr) {
		var input = addr;
		var latlngStr = input.split(',', 2);
		var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
		geocoder.geocode({'location': latlng}, function(results, status) {
			if (status === 'OK') {
				if (results[0]) {
					// map.setZoom(11);
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: 13,
						center: {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])}
					});
					var marker = new google.maps.Marker({
						position: latlng,
						map: map
						// center: {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])}
					});
					infowindow.setContent(results[0].formatted_address);
					infowindow.open(map, marker);
				} else {
					window.alert('No results found');
				}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
	}

	google.maps.event.addDomListener(window, "load", initMap);
</script>

<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding: 2em;">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 box-alamat" style="margin-top: -160px;">
				<div class="col-md-2 col-md-offset-9 btn-go">
					<a href="">
						<div class="circle-btn-go">
							Go
						</div>
					</a>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="col-md-12 box-alamat1" style="color: #0f4471">
							Office Head
						</div>
						<div class="col-md-12 box-alamat2">
							Pasar Autum Mall Lt1
							A38 Surabaya
							081-343-532-433 Pin BB OH7898G
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-md-12 box-alamat1">
							Office Pelita Ltcc
						</div>
						<div class="col-md-12 box-alamat2">
							Pasar Autum Mall Lt1
							A38 Surabaya
							081-343-532-433 Pin BB OH7898G
						</div>
					</div>
					<div class="col-md-4">

					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<div class="col-md-12 box-alamat1">
							Pelita Kelampis
						</div>
						<div class="col-md-12 box-alamat2">
							Pasar Autum Mall Lt1
							A38 Surabaya
							081-343-532-433 Pin BB OH7898G
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-md-12 box-alamat1">

						</div>
					</div>
					<div class="col-md-4">

					</div>
				</div>
			</div>

			<div class="col-md-10 col-md-offset-1 box-hubungi" style="margin-top: px;">
				<div class="col-md-12">
					<div class="col-md-3 box-hubungi1">
						Hubungi Kami
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-3">
						<img src="<?php echo base_url() ?>/assets/images/chat.PNG" style="width: 90%">
					</div>
					<div class="col-md-9">
						<div class="col-md-12 box-hubungi2">
							Inggin mencari jawaban,ingin mendapatkan solusi, ataun sekedar memberikan masukan atas kinerja kami. silahkan hubungi kami melalui live Chat kami. Kami akan segera membantu menyelesaikan masalah anda.
						</div>
					</div>
				</div>
				<div class="col-md-12">							
					<div class="col-md-9 col-md-offset-3">
						<div class="col-md-12 box-hubungi2">
							Layanan Online Chat 24 Jam!
						</div>
						<div class="col-md-12">
							<a href="">
								<div class="col-md-5 btn-chat1">											
									<i class="fa fa-wechat"></i>
									Chat With Customer Care
								</div>
							</a>
						</div>
						<div class="col-md-12">
							<a href="">
								<div class="col-md-5 btn-chat2">										
									<i class="fa fa-wechat"></i>
									Facebook Massanger										
								</div>
							</a>
						</div>
						<div class="col-md-12">
							<a href="" class="box-hubungi3">
								Masih buntuh Bantuan?
							</a>
						</div>								
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="popup">
	<div class="window">
		<a href="#" class="close-button" title="Close">
			<i class="fa fa-close"></i>
		</a>
		<div class="row">
			<div class="col-md-12" style="background-color: ;">
				<div class="col-md-12 popup-1">
					On load modal
				</div>
				
				<div class="col-md-12" style="text-align: center;">
					<button class="btn-simpan">Selanjutnya</button>
				</div>
			</div>
		</div>
	</div>
</div>

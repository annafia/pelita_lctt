<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 2em">
  <div class="container">

    <div class="row">
      
      <div class="col-md-12">
        <div class="col-md-12 lokasi" style="background-color: ">
          Malang - Indonesia
        </div>
        <div class="col-md-12 nama-paket" style="background-color: ">
          Kota Batu dan sekitarnya
        </div>
        <div class="col-md-12">               
          <div class="col-md-12 line" style="padding: 0px;"></div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-calendar-o"></i> 09 April 2018
          </div>
          <div class="col-md-12 detail-pkt">
            <i class="fa fa-bell-o"></i> 5 Hari 5 Malam
          </div>
        </div>
        <div class="col-md-4" style="background-color: ">
          <div class="col-md-12 detail-pkt">
            Dewasa (Twin/Triple) Anak
          </div>
          <div class="col-md-12 detail-pkt">
            Extra Bad
          </div>
        </div>
        <div class="col-md-4" style="background-color:">
          <div class="col-md-12 detail-pkt" style="color: #9b9b9b">
            Harga
          </div>
          <div class="col-md-12 detail-pkt" style="color: #0f4471">
            Rp. 10.000.000
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-md-12 prog-bar">
            <div class="col-md-2 col-md-offset-3 bar-text">
              Isi data
            </div>
            <div class="col-md-2 bar-text" style="color: #ff6107;">
              Pembayaran
            </div>
            <div class="col-md-2 bar-text">
              Selesai
            </div>
          </div>
        </div>
        <div class="col-md-10 judul-isi" style="padding-left: 15px;">
          Pilih Metode Pembayaran
        </div>              
        <div class="col-md-8">
          <div class="col-md-12 box-data-pemesan">
            <div class="col-md-12 judul-isi">
              Data Pemesan
            </div>
            <div class="col-md-12 dtl-pemesan">
              Nama : Foster People
            </div>
            <div class="col-md-12 dtl-pemesan">
              Email : Fosterpeople.office@gmail.com
            </div>
            <div class="col-md-12 dtl-pemesan">
              No. Telepon : 081-234-564-422
            </div>
          </div>
          <div class="col-md-12 box-atm">
            <div class="col-md-12 box-akordion">
              <div class="col-md-12" style="padding: unset;">
                <div class="col-md-12 cara-bayar" style="background-color:  ;">
                  <input type="radio" name=""> ATM Bersama
                </div>
                <div class="col-md-12 cara-bayar2" style="background-color:;">
                  Ketentuan Pembayaran
                </div>
                <div class="col-md-12 cara-bayar3" style="background-color:;">
                  1. Biaya Transaksi Gratis<br>
                  2. Konfirmasi Pembyaran Tidak Perlu<br>
                  3. Verifikasi Pembayaran Otomatis
                </div>
                <div class="col-md-12 cara-bayar2" style="background-color:;">
                  Pilih Type Pembayaran
                </div>
                <div class="col-md-12">
                  <input class="cara-bayar4" type="text" name="">
                </div>
              </div>
              <div class="col-md-12" style="padding: unset;">
                <div class="col-md-12 cara-bayar" style="background-color:  ;">
                  <input type="radio" name=""> Kartu Kredit
                      </div><!-- 
                      <div class="col-md-12 cara-bayar2" style="background-color:;">
                        Ketentuan Pembayaran
                      </div>
                      <div class="col-md-12 cara-bayar3" style="background-color:;">
                        1. Biaya Transaksi Gratis<br>
                        2. Konfirmasi Pembyaran Tidak Perlu<br>
                        3. Verifikasi Pembayaran Otomatis
                      </div>
                      <div class="col-md-12 cara-bayar2" style="background-color:;">
                        Pilih Type Pembayaran
                      </div>
                      <div class="col-md-12">
                        <input class="cara-bayar4" type="text" name="">
                      </div> -->
                    </div>
                    <div class="col-md-12" style="padding: unset;">
                      <div class="col-md-12 cara-bayar" style="background-color:  ;">
                        <input type="radio" name=""> Kartu Kredit Standart
                      </div><!-- 
                      <div class="col-md-12 cara-bayar2" style="background-color:;">
                        Ketentuan Pembayaran
                      </div>
                      <div class="col-md-12 cara-bayar3" style="background-color:;">
                        1. Biaya Transaksi Gratis<br>
                        2. Konfirmasi Pembyaran Tidak Perlu<br>
                        3. Verifikasi Pembayaran Otomatis
                      </div>
                      <div class="col-md-12 cara-bayar2" style="background-color:;">
                        Pilih Type Pembayaran
                      </div>
                      <div class="col-md-12">
                        <input class="cara-bayar4" type="text" name="">
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="col-md-12 col-xs-12 box-transaksi">
                  <div class="col-md-12 kode-transaksi">
                    No. Transaksi : 75085
                  </div>
                  <div class="col-md-12 rincian-hrg">
                    Rincian Harga
                  </div>
                  <div class="col-md-12 rincian-hrg1">
                    Dewasa (Twin/Tripple) Anak
                    Extra Bed
                  </div>
                  <div class="col-md-12 col-xs-12 rincian-hrg2">                    
                    <div class="col-md-6 col-xs-6">
                      Uang Muka
                    </div>
                    <div class="col-md-6 col-xs-6" style="text-align: right;">
                      Rp. 1000.000
                    </div>
                    <div class="col-md-6 col-xs-6">
                      Diskon
                    </div>
                    <div class="col-md-6 col-xs-6" style="text-align: right;">
                      Rp. 500.000
                    </div>
                  </div>
                  <div class="col-md-12 col-xs-12">
                    <div class="col-md-12 rincian-hrg-line"></div>
                  </div>
                  <div class="col-md-12 col-xs-12 total-hrg">
                    Total Harga
                  </div>
                  <div class="col-md-12 col-xs-12 total-hrg1">
                    Rp. 10.000.000
                  </div>
                  <div class="col-md-12 col-xs-12">
                    <div class="col-md-12 rincian-hrg-line"></div>
                  </div>
                  <div class="col-md-12 col-xs-12 pakai-promo">
                    Pakai Promo
                  </div>
                  <div class="col-md-12 col-xs-12">
                    <input class="pakai-promo1" type="text" name="">
                  </div>
                  <div class="col-md-12 col-xs-12">                   
                    <div class="col-md-12">
                      <button class="btn-pakai-promo">Bayar Sekarang</button>
                    </div>
                  </div>

                </div>
              </div>

            </div>
            
          </div>
        </div>
      </div>
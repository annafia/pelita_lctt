      <div id="fh5co-contact" class="fh5co-section-gray" style="padding-top: 2em;">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="kiri">
                <div class="foto1">
                  <div class="lingkaran">
                    <?php if ($profile->foto != ""): ?>
                      <img class="lingkaran" src="<?php echo $profile->foto ?>">
                    <?php else: ?>
                      <img class="lingkaran" src="<?php echo base_url() ?>/assets/images/avatar3.png">
                    <?php endif ?>
                  </div>
                </div>              
                <div class="nama">
                  <?php echo $profile->nama; ?>
                </div>
                <div class="point">
                  Point : 300
                </div>
                <a href="<?php echo base_url() ?>/index.php/UserPage/editprofile">
                  <div class="menu mpan">
                    <div class="text">Edit Profile</div>
                  </div>
                </a>
                <a href="<?php echo base_url() ?>/index.php/UserPage/pointsaya">
                  <div class="menu">
                    <div class="text">Point Saya</div>
                  </div>
                </a>
                <a href="<?php echo base_url() ?>/index.php/UserPage/pesanansaya">
                  <div class="menu">
                    <div class="text">Pesanan Saya</div>
                  </div>
                </a>
                <a href="<?php echo base_url() ?>/index.php/UserPage/pengaturanakun">
                  <div class="menu">
                    <div class="text">Pengaturan akun</div>
                  </div>
                </a>
                <a href="">
                  <div class="menu">
                    <div class="text">Log Out</div>
                  </div>
                </a>
              </div>  
            </div>

            <div class="col-md-8">
              <div class="kanan col-md-12">
                <div class="judul col-md-12">
                  Edit Profile
                </div>
                <form method="POST" action="<?php echo site_url() ?>/UserPage/updateProfile" enctype="multipart/form-data">
                  <div class="Upfoto col-md-12">
                    <div class="col-md-3 col-md-offset-3" style="background-color: ">
                      
                      <?php if ($profile->foto != ""): ?>
                        <img class="bunder" src="<?php echo $profile->foto ?>">
                      <?php else: ?>
                        <img class="bunder" src="<?php echo base_url() ?>/assets/images/avatar3.png">
                      <?php endif ?>
                    </div>
                    <div class="col-md-4" style="background-color: ;">
                      <input type="file" name="gambar">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <!-- <div class="col-md-6">
                      <label>Nama Depan</label>
                      <input type="text" class="inText" name="" style="height: 36px;" placeholder="Nama Depan" value="<?php echo $profile->nama ?>">
                    </div>
                    <div class="col-md-6">
                      <label>Nama Belkang</label>
                      <input type="text" class="inText" name="" style="height: 36px;" placeholder="Nama Belakang">
                    </div> -->
                    <div class="col-md-12">
                      <label>Nama</label>
                      <input type="text" class="inText" name="nama" style="height: 36px;" placeholder="Nama Depan" value="<?php echo $profile->nama ?>">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-12">
                      <label>Alamat</label>
                      <input type="text" class="inText" name="alamat" style="height: 36px;" value="<?php echo $profile->alamat ?>">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-5">
                      <label>Negara</label>
                      <input type="text" class="inText" name="" style="height: 36px;" placeholder="">
                    </div>
                    <div class="col-md-4">
                      <label>Kota</label>
                      <input type="text" class="inText" name="kota" style="height: 36px;" placeholder="" value="<?php echo $profile->kota ?>">
                    </div>
                    <div class="col-md-3">
                      <label>Kode Pos</label>
                      <input type="text" class="inText" name="" style="height: 36px;" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="background-color: ">
                    <div class="col-md-6">
                      <label>Tanggal Lahir</label>
                      <input type="date" class="inText" name="tanggal_lahir" style="height: 36px;" placeholder="" value="<?php echo $profile->tanggal_lahir ?>">
                    </div>
                    <div class="col-md-6">
                      <label>Nomor Telp</label>
                      <input type="text" class="inText" name="no_hp" style="height: 36px;" placeholder="" value="<?php echo $profile->no_hp ?>">
                    </div>
                  </div>
                  <div class="col-md-12 row-edit" style="padding-left: unset;">
                    <div class="col-md-6" style="text-align: right;">
                      <button class="btn-batal">Batal</button>
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                      <button class="btn-simpan">Simpan Perubahan</button>
                    </div>
                  </div>
                </form>
              </div>              
            </div>
            
          </div>
        </div>
      </div>

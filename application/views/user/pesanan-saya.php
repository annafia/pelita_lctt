<div id="fh5co-contact" class="fh5co-section-gray" style="padding-top: 2em;">
  <div class="container">

    <div class="row">
      <div class="col-md-4">
        <div class="kiri">
          <div class="foto1">
            <div class="lingkaran">
              <?php if ($profile->foto != ""): ?>
                <img class="lingkaran" src="<?php echo $profile->foto ?>">
              <?php else: ?>
                <img class="lingkaran" src="<?php echo base_url() ?>/assets/images/avatar3.png">
              <?php endif ?>
            </div>
          </div>              
          <div class="nama">
            <?php echo $profile->nama; ?>
          </div>
          <div class="point">
            Point : 300
          </div>
          <a href="<?php echo base_url() ?>/index.php/UserPage/editprofile">
            <div class="menu">
              <div class="text">Edit Profile</div>
            </div>
          </a>
          <a href="<?php echo base_url() ?>/index.php/UserPage/pointsaya">
            <div class="menu">
              <div class="text">Point Saya</div>
            </div>
          </a>
          <a href="<?php echo base_url() ?>/index.php/UserPage/pesanansaya">
            <div class="menu mpan">
              <div class="text">Pesanan Saya</div>
            </div>
          </a>
          <a href="<?php echo base_url() ?>/index.php/UserPage/pengaturanakun">
            <div class="menu">
              <div class="text">Pengaturan akun</div>
            </div>
          </a>
          <a href="">
            <div class="menu">
              <div class="text">Log Out</div>
            </div>
          </a>
        </div>  
      </div>

      <div class="col-md-8">              
        <?php if (!empty($tagihan)): ?>
        <div class="col-md-12 judul-pesanan">
          Sedang Berjalan
        </div>
        <?php endif ?>
        <?php foreach ($tagihan as $t): ?>
        <div class="col-md-12 paket">
          <div class="col-md-5 gambar-pkt">
            <!-- <img src="<?php echo base_url() ?>/assets/images/paket1.jpg"> -->
            <img src="<?php echo $t->gambar_paket ?>">
          </div>
          <div class="col-md-4 detail-paket1">
            <?php echo $t->negara; ?>
          </div>
          <div class="col-md-3 detail-paket2">
            <?php echo $t->jml_keberangkatan; ?> Keberangkatan
          </div>
          <div class="col-md-7 detail-paket3">
            <a href="<?php echo site_url() ?>/KonfirmasiPembayaran/pembayaran/<?php echo $t->id_list_hold_seat ?>"><?php echo $t->nama_paket_tour; ?></a>
          </div>
          <div class="col-md-7 detail-paket4">
            Durasi : <?php echo $t->durasi; ?> Hari <?php echo $t->durasi; ?> Malam
          </div>
          <div class="col-md-7 detail-paket5">
            Tanggal Keberangkatan : <?php echo date('d F Y', strtotime($t->tanggal_keberangkatan)); ?>
          </div>
          <div class="col-md-7 detail-paket6">
            Status
          </div>
          <div class="col-md-7 detail-paket7">
            <!-- Pembayaran DP 30% -->
            <?php echo $t->status; ?>
          </div>
        </div>
        <?php endforeach ?>
        <?php if (!empty($log)): ?>
        <div class="col-md-12 judul-pesanan" style="margin-top: 56px;">
          Daftar Transaksi
        </div>
        <?php endif ?>
        <?php foreach ($log as $l): ?>
        <div class="col-md-12 paket">
          <div class="col-md-5 gambar-pkt">
            <!-- <img src="<?php echo base_url() ?>/assets/images/paket1.jpg"> -->
            <img src="<?php echo $l->gambar_paket ?>">
          </div>
          <div class="col-md-4 detail-paket1">
            <?php echo $l->negara; ?>
          </div>
          <div class="col-md-3 detail-paket2">
            <?php echo $l->jml_keberangkatan; ?> Keberangkatan
          </div>
          <div class="col-md-7 detail-paket3">
            <?php echo $l->nama_paket_tour; ?>
          </div>
          <div class="col-md-7 detail-paket4">
            Durasi : <?php echo $l->durasi; ?> Hari <?php echo $l->durasi; ?> Malam
          </div>
          <div class="col-md-7 detail-paket5">
            Tanggal Keberangkatan : <?php echo date('d F Y', strtotime($l->tanggal_keberangkatan)); ?>
          </div>
          <div class="col-md-7 detail-paket6">
            Status
          </div>
          <div class="col-md-7 detail-paket7">
            <!-- Pembayaran Lunas -->
            <?php echo $l->status; ?>
          </div>
        </div>
        <?php endforeach ?>

        
        <?php if (!empty($visa)): ?>
        <div class="col-md-12 judul-pesanan" style="margin-top: 56px;">
          Pemesanan Visa
        </div>
        <?php endif ?>
        <?php foreach ($visa as $v): ?>
        <div class="col-md-12 paket">
          <div class="col-md-6 detail-paket1">
            Negara Tujuan <b><?php echo $v->negara; ?></b>
          </div>
          <div class="col-md-6 detail-paket2">
            Berangkat Pada Tanggal <b><?php echo date('d F Y', strtotime($v->tgl_keberangkatan)); ?></b>
          </div>
          <div class="col-md-12 detail-paket3">
            <?php echo $v->id_pembayaran_visa; ?>
          </div>
          <div class="col-md-12 detail-paket6">
            Status
          </div>
          <div class="col-md-12 detail-paket7">
            <!-- Pembayaran Lunas -->
            <?php if ($v->bukti == null || $v->bukti == ''): ?>
              <?php echo $v->status; ?> - upload struk pembayaran visa anda
            <?php elseif ($v->bukti != null || $v->bukti != ''): ?>
              <?php echo $v->status; ?>
            <?php endif ?>
          </div>
          <div class="col-md-12 table-responsive">
            <table class="table table-striped">
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Tipe</th>
              </tr>
              <?php $total = 0; ?>
              <?php foreach ($detailVisa as $dv): ?>
              <tr>
                <td><?php echo $dv->nama_lengkap; ?></td>
                <td>Rp. <?php echo number_format($dv->harga_visa, 2, ",", "."); ?></td>
                <td><?php echo $dv->visa_umur; ?></td>
                <?php $total += $dv->harga_visa; ?>
              </tr>
              <?php endforeach ?>
            </table>
          </div>
          <div class="col-md-6 detail-paket7">
            Total: Rp. <?php echo number_format($total, 2, ",", "."); ?>
          </div>
          <?php if ($v->bukti == null || $v->bukti == ''): ?>
          <form action="<?php echo site_url() ?>/UserPage/konfVisa" method="POST" enctype="multipart/form-data">
          <div class="col-md-12">
            <label>Bukti Transfer</label>                       
            <div class="upload-wraper">
              <input id="uploadFile" placeholder="Pilih file..." disabled="disabled">
              <div class="fileUpload btn-upload">
                <span>Upload</span>
                <input id="uploadBtn" name="gambar" type="file" class="upload">
              </div>
            </div>
            <!-- <input type="file" name="gambar"> -->
          </div>
          
          <input type="hidden" value="<?php echo $v->id_pembayaran_visa; ?>" name="id_pembayaran_visa">
          <div class="col-md-5 col-md-offset-7" style="margin-top: 15px;">
            <button type="submit" class="btn-simpan">Konfirmasi Pembayaran</button>
          </div>
          </form>
          <?php endif ?>
        </div>
        <?php endforeach ?>


        <?php if (!empty($logVisa)): ?>
        <div class="col-md-12 judul-pesanan" style="margin-top: 56px;">
          Log Pemesanan Visa
        </div>
        <?php endif ?>
        <?php foreach ($logVisa as $lv): ?>
        <div class="col-md-12 paket">
          <div class="col-md-6 detail-paket1">
            Negara Tujuan <b><?php echo $lv->negara; ?></b>
          </div>
          <div class="col-md-6 detail-paket2">
            Berangkat Pada Tanggal <b><?php echo date('d F Y', strtotime($lv->tgl_keberangkatan)); ?></b>
          </div>
          <div class="col-md-12 detail-paket3">
            <?php echo $lv->id_pembayaran_visa; ?>
          </div>
          <div class="col-md-12 detail-paket6">
            Status
          </div>
          <div class="col-md-12 detail-paket7">
            <!-- Pembayaran Lunas -->
            <?php echo $lv->status; ?> - <?php echo $lv->status_visa; ?>
          </div>
          <div class="col-md-12 table-responsive">
            <table class="table table-striped">
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Tipe</th>
              </tr>
              <?php $total = 0; ?>
              <?php foreach ($detailVisa as $dv): ?>
              <?php if ($dv->id_pembayaran_visa == $lv->id_pembayaran_visa): ?>
              <tr>
                <td><?php echo $dv->nama_lengkap; ?></td>
                <td>Rp. <?php echo number_format($dv->harga_visa, 2, ",", "."); ?></td>
                <td><?php echo $dv->visa_umur; ?></td>
                <?php $total += $dv->harga_visa; ?>
              </tr>
              <?php endif ?>
              <?php endforeach ?>
            </table>
          </div>
          <div class="col-md-6 detail-paket7">
            Total: Rp. <?php echo number_format($total, 2, ",", "."); ?>
          </div>
        </div>
        <?php endforeach ?>

        <?php if (!empty($paspor)): ?>
        <div class="col-md-12 judul-pesanan" style="margin-top: 56px;">
          Pembuatan Paspor
        </div>
        <?php endif ?>
        <?php foreach ($paspor as $p): ?>
        <div class="col-md-12 paket">
          <div class="col-md-6 detail-paket1">
            Kantor Imigrasi <b><?php echo $p->kantor_imigrasi; ?></b> (<?php echo $p->durasi_proses; ?> Hari)
          </div>
          <div class="col-md-6 detail-paket2">
            <?php echo $p->tipe_paspor ?>
          </div>
          <div class="col-md-12 detail-paket3">
            <?php echo $p->id_inv_paspor; ?>
          </div>
          <div class="col-md-12 detail-paket6">
            Status
          </div>
          <div class="col-md-12 detail-paket7">
            <!-- Pembayaran Lunas -->
            <?php echo $p->status; ?>
          </div>
          <div class="col-md-12 table-responsive">
            <table class="table table-striped">
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Tipe</th>
              </tr>
              <?php $total = 0; ?>
              <?php foreach ($detailPaspor as $dp): ?>
              <?php if ($dp->id_inv_paspor == $p->id_inv_paspor): ?>
              <tr>
                <td><?php echo $dp->nama; ?></td>
                <td>Rp. <?php echo number_format($dp->harga, 2, ",", "."); ?></td>
                <td><?php echo $dp->umur_paspor; ?></td>
                <?php $total += $dp->harga; ?>
              </tr>
              <?php endif ?>
              <?php endforeach ?>
            </table>
          </div>
          <div class="col-md-6 detail-paket7">
            Total: Rp. <?php echo number_format($total, 2, ",", "."); ?>
          </div>
          <form action="<?php echo site_url() ?>/UserPage/konfPaspor" method="POST" enctype="multipart/form-data">
          <div class="col-md-12">
            <label>Bukti Transfer</label>                       
            <div class="upload-wraper">
              <input id="uploadFile" placeholder="Pilih file..." disabled="disabled">
              <div class="fileUpload btn-upload">
                <span>Upload</span>
                <input id="uploadBtn" name="gambar1" type="file" class="upload">
              </div>
            </div>
            <!-- <input type="file" name="gambar"> -->
          </div>
          
          <input type="hidden" value="<?php echo $p->id_inv_paspor; ?>" name="id_inv_paspor">
          <div class="col-md-5 col-md-offset-7" style="margin-top: 15px;">
            <button type="submit" class="btn-simpan">Konfirmasi Pembayaran</button>
          </div>
          </form>
        </div>
        <?php endforeach ?>


        <?php if (!empty($logPaspor)): ?>
        <div class="col-md-12 judul-pesanan" style="margin-top: 56px;">
          Log Pembuatan Paspor
        </div>
        <?php endif ?>
        <?php foreach ($logPaspor as $lp): ?>
        <div class="col-md-12 paket">
          <div class="col-md-6 detail-paket1">
            Kantor Imigrasi <b><?php echo $lp->kantor_imigrasi; ?></b> (<?php echo $lp->durasi_proses; ?> Hari)
          </div>
          <div class="col-md-6 detail-paket2">
            <?php echo $lp->tipe_paspor ?>
          </div>
          <div class="col-md-12 detail-paket3">
            <?php echo $lp->id_inv_paspor; ?>
          </div>
          <div class="col-md-12 detail-paket6">
            Status
          </div>
          <div class="col-md-12 detail-paket7">
            <!-- Pembayaran Lunas -->
            <?php echo $lp->status; ?>
          </div>
          <div class="col-md-12 table-responsive">
            <table class="table table-striped">
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Tipe</th>
              </tr>
              <?php $total = 0; ?>
              <?php foreach ($detailPaspor as $dp): ?>
              <?php if ($dp->id_inv_paspor == $lp->id_inv_paspor): ?>
              <tr>
                <td><?php echo $dp->nama; ?></td>
                <td>Rp. <?php echo number_format($dp->harga, 2, ",", "."); ?></td>
                <td><?php echo $dp->umur_paspor; ?></td>
                <?php $total += $dp->harga; ?>
              </tr>
              <?php endif ?>
              <?php endforeach ?>
            </table>
          </div>
          <div class="col-md-6 detail-paket7">
            Total: Rp. <?php echo number_format($total, 2, ",", "."); ?>
          </div>
        </div>
        <?php endforeach ?>


        <!-- <div class="col-md-12 paket">
          <div class="col-md-5 gambar-pkt">
            <img src="<?php echo base_url() ?>/assets/images/paket2.jpg">
          </div>
          <div class="col-md-4 detail-paket1">
            Indonesia - Malang
          </div>
          <div class="col-md-3 detail-paket2">
            10 Keberangkatan
          </div>
          <div class="col-md-7 detail-paket3">
            Hongkong Taiwan + Sng
          </div>
          <div class="col-md-7 detail-paket4">
            Durasi : 3 Hari 3 Malam
          </div>
          <div class="col-md-7 detail-paket5">
            Tanggal Keberangkatan : 15 April 2018
          </div>
          <div class="col-md-7 detail-paket6">
            Status :
          </div>
          <div class="col-md-7 detail-paket7">
            Pembayaran Lunas
          </div>
        </div> -->

      </div>
      
    </div>
  </div>
</div>
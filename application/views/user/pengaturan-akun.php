<div id="fh5co-contact" class="fh5co-section-gray" style="padding-top: 2em;">
  <div class="container">

    <div class="row">
      <div class="col-md-4">
        <div class="kiri">
          <div class="foto1">
            <div class="lingkaran">
              <?php if ($profile->foto != ""): ?>
                <img class="lingkaran" src="<?php echo $profile->foto ?>">
              <?php else: ?>
                <img class="lingkaran" src="<?php echo base_url() ?>/assets/images/avatar3.png">
              <?php endif ?>
            </div>
          </div>              
          <div class="nama">
            <?php echo $profile->nama; ?>
          </div>
          <div class="point">
            Point : 300
          </div>
          <a href="<?php echo base_url() ?>/index.php/UserPage/editprofile">
            <div class="menu">
              <div class="text">Edit Profile</div>
            </div>
          </a>
          <a href="<?php echo base_url() ?>/index.php/UserPage/pointsaya">
            <div class="menu">
              <div class="text">Point Saya</div>
            </div>
          </a>
          <a href="<?php echo base_url() ?>/index.php/UserPage/pesanansaya">
            <div class="menu">
              <div class="text">Pesanan Saya</div>
            </div>
          </a>
          <a href="<?php echo base_url() ?>/index.php/UserPage/pengaturanakun">
            <div class="menu mpan">
              <div class="text">Pengaturan akun</div>
            </div>
          </a>
          <a href="">
            <div class="menu">
              <div class="text">Log Out</div>
            </div>
          </a>
        </div>  
      </div>

      <div class="col-md-8">
        <div class="kanan col-md-12" style="padding-left: 10%; padding-right: 10%;">
          <form method="POST" action="<?php echo site_url() ?>/UserPage/updatePassword">
            <div class="col-md-12 judul-pengaturan">
              Pengaturan Akun
            </div>                  
            <div class="col-md-12 row-edit">
              <div class="col-md-12" style="padding-left: unset;">
                <label>Email</label>
                <input type="text" class="inText" name="email" style="height: 36px;" placeholder="Tesjncs@Gmail.co.id" value="<?php echo $profile->id_member ?>">
                <div class="info">Informasi dan promo akan di kirim ke email</div>
              </div>
            </div>
            <div class="col-md-12 judul-pengaturan" style="margin-top: 56px;">
              Pengaturan Kata Sandi
            </div>
            <?php if ($this->session->flashdata('err')): ?>
              <p class="detail-paket7"><?php echo $this->session->flashdata('err'); ?></p>
            <?php endif ?>
            <div class="col-md-12 row-edit" style="padding-left: unset;">
              <div class="col-md-12">
                <label>Kata Sandi Lama</label>
                <input type="password" class="inText" name="sandiLama" style="height: 36px;">
              </div>
            </div>
            <div class="col-md-12 row-edit" style="padding-left: unset;">
              <div class="col-md-12">
                <label>Kata Sandi Baru</label>
                <input type="text" class="inText" name="sandiBaru" style="height: 36px;">
              </div>
            </div>
            <div class="col-md-12 row-edit" style="padding-left: unset;">
              <div class="col-md-12">
                <label>Konfirmasi Kata Sandi Baru</label>
                <input type="text" class="inText" name="konfSandi" style="height: 36px;">
              </div>
            </div>
            <div class="col-md-12 row-edit" style="padding-left: unset; ">
              <div class="col-md-3 col-md-offset-3">
                <button class="btn-batal">Batal</button>
              </div>
              <div class="col-md-6" style="text-align: right;">
                <button class="btn-simpan">Simpan Perubahan</button>
              </div>
            </div>
          </form>
        </div>              
      </div>

    </div>
  </div>
</div>
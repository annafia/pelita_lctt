<div id="fh5co-contact" class="fh5co-section-gray" style="background-color: white; padding-top: 2em">
  <div class="container">

    <div class="row">

      <div class="col-md-12">
        <div class="col-md-12 cek-ok">
          ✔
        </div>
        <div class="col-md-12 cek-ok1">
          Terimakasih - Pembayaran Berhasil
        </div>
        <div class="col-md-12 cek-ok2">
          Malang - Indonesia
        </div>
        <div class="col-md-12 cek-ok1">
          Kota Batu dan sekitarnya
        </div>
        <div class="col-md-12">               
          <div class="col-md-12 box-data-pemesan">
            <div class="col-md-12 judul-isi">
              Data Pemesan
            </div>
            <div class="col-md-12 dtl-pemesan">
              Nama : Foster People
            </div>
            <div class="col-md-12 dtl-pemesan">
              Email : Fosterpeople.office@gmail.com
            </div>
            <div class="col-md-12 dtl-pemesan">
              No. Telepon : 081-234-564-422
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="col-md-12 box-atm" style="padding-top: unset;">
            <div class="col-md-12 judul-isi">
              Rincian Pemesanan
            </div>
            <div class="col-md-12 rincian-pemesanan">
              Kota Batu dan sekitarnya
            </div>
            <div class="col-md-12">                   
              <div class="col-md-12 rincian-pemesanan1">
                Tanggal Keberangkatan : 9 April 2018 - 12 April 2018
              </div>
              <div class="col-md-12 rincian-pemesanan1">
                Durasi : 3 Hari 3 Malam
              </div>
              <div class="col-md-12 data-tamu">
                Data Tamu
              </div>
              <div class="col-md-12 data-tamu1">
                <div class="col-md-6">
                  1. Ravil Ahmad
                </div>
                <div class="col-md-6">
                  21 Maret 1997
                </div>
              </div>
              <div class="col-md-12 data-tamu1">
                <div class="col-md-6">
                  2. Nanda Budiman
                </div>
                <div class="col-md-6">
                  02 Agustus 1962
                </div>
              </div>              
            </div>
            <div class="col-md-12 rincian-pemesanan2">
              Catatan : 
            </div>
            <div class="col-md-12 rincian-pemesanan3">
              If you’re planning a stag do in Birmingham, you’d almost have to include some sporting activities. After all, Sport England has awarded Birmingham the title National City of Sport. 
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="col-md-12 col-xs-12 box-transaksi">
            <div class="col-md-12 kode-transaksi">
              No. Transaksi : 75085
            </div>
            <div class="col-md-12 rincian-hrg">
              Rincian Harga
            </div>
            <div class="col-md-12 rincian-hrg1">
              Dewasa (Twin/Tripple) Anak
              Extra Bed
            </div>
            <div class="col-md-12 col-xs-12 rincian-hrg2">                    
              <div class="col-md-6 col-xs-6">
                Uang Muka
              </div>
              <div class="col-md-6 col-xs-6" style="text-align: right;">
                Rp. 1000.000
              </div>
              <div class="col-md-6 col-xs-6">
                Diskon
              </div>
              <div class="col-md-6 col-xs-6" style="text-align: right;">
                Rp. 500.000
              </div>
            </div>
            <div class="col-md-12 col-xs-12">
              <div class="col-md-12 rincian-hrg-line"></div>
            </div>
            <div class="col-md-12 col-xs-12 total-hrg">
              Total Harga
            </div>
            <div class="col-md-12 col-xs-12 total-hrg1">
              Rp. 10.000.000
            </div>
            <div class="col-md-12 col-xs-12 pakai-promo">
              Promo Code : 
              <font style="color: #417505;">                
                0g484hf
              </font>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
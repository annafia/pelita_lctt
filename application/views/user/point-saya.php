      <div id="fh5co-contact" class="fh5co-section-gray" style="padding-top: 2em;">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="kiri">
                <div class="foto1">
                  <div class="lingkaran">
                    <?php if ($profile->foto != ""): ?>
                      <img class="lingkaran" src="<?php echo $profile->foto ?>">
                    <?php else: ?>
                      <img class="lingkaran" src="<?php echo base_url() ?>/assets/images/avatar3.png">
                    <?php endif ?>
                  </div>
                </div>              
                <div class="nama">
                  <?php echo $profile->nama; ?>
                </div>
                <div class="point">
                  Point : 300
                </div>
                <a href="<?php echo base_url() ?>/index.php/UserPage/editprofile">
                  <div class="menu">
                    <div class="text">Edit Profile</div>
                  </div>
                </a>
                <a href="<?php echo base_url() ?>/index.php/UserPage/pointsaya">
                  <div class="menu mpan">
                    <div class="text">Point Saya</div>
                  </div>
                </a>
                <a href="<?php echo base_url() ?>/index.php/UserPage/pesanansaya">
                  <div class="menu">
                    <div class="text">Pesanan Saya</div>
                  </div>
                </a>
                <a href="<?php echo base_url() ?>/index.php/UserPage/pengaturanakun">
                  <div class="menu">
                    <div class="text">Pengaturan akun</div>
                  </div>
                </a>
                <a href="">
                  <div class="menu">
                    <div class="text">Log Out</div>
                  </div>
                </a>
              </div>  
            </div>

            <div class="col-md-8">
              <div class="kanan col-md-12" style="padding-left: 10vh; padding-right: 10vh;">
                <div class="judul col-md-12" style="margin-bottom: 56px;">
                  Point Saya
                </div>
                <div class="col-md-12 poin">
                  <div class="col-md-3 total-point">
                    Total Point
                    <h3>
                      20
                    </h3>
                  </div>
                  <div class="col-md-9 total-transaksi">
                    Total Transaksi
                    <h3>
                      Rp. 20.000.000
                    </h3>
                  </div>
                </div>
              </div>
              <div class="col-md-12 tukar-poin">
                Tukarkan Poin
              </div>
              <div class="col-md-12 paket">
                <div class="col-md-5 gambar-pkt">
                  <img src="<?php echo base_url() ?>/assets/images/paket1.jpg">
                </div>
                <div class="col-md-4 detail-paket1">
                  Indonesia - Malang
                </div>
                <div class="col-md-3 detail-paket2">
                  10 Keberangkatan
                </div>
                <div class="col-md-7 detail-paket3">
                  Malang Kota Batu + Jatim Park
                </div>
                <div class="col-md-7 detail-paket4">
                  Durasi : 3 Hari 3 Malam
                </div>
                <div class="col-md-7 detail-paket5">
                  Tanggal Keberangkatan : 15 April 2018
                </div>
                <div class="col-md-7 detail-paket6">
                  Mulai :
                </div>
                <div class="col-md-7 detail-paket7">
                  10 Poin + Rp. 20.000.000
                </div>
              </div>
              <div class="col-md-12 paket">
                <div class="col-md-5 gambar-pkt">
                  <img src="<?php echo base_url() ?>/assets/images/paket2.jpg">
                </div>
                <div class="col-md-4 detail-paket1">
                  Indonesia - Malang
                </div>
                <div class="col-md-3 detail-paket2">
                  19 Keberangkatan
                </div>
                <div class="col-md-7 detail-paket3">
                  Hongkong Taiwan + Sng
                </div>
                <div class="col-md-7 detail-paket4">
                  Durasi : 3 Hari 3 Malam
                </div>
                <div class="col-md-7 detail-paket5">
                  Tanggal Keberangkatan : 15 April 2018
                </div>
                <div class="col-md-7 detail-paket6">
                  Mulai :
                </div>
                <div class="col-md-7 detail-paket7">
                  10 Poin + Rp. 20.000.000
                </div>
              </div>
              <div class="col-md-12 paket">
                <div class="col-md-5 gambar-pkt">
                  <img src="<?php echo base_url() ?>/assets/images/paket1.jpg">
                </div>
                <div class="col-md-4 detail-paket1">
                  Indonesia - Malang
                </div>
                <div class="col-md-3 detail-paket2">
                  10 Keberangkatan
                </div>
                <div class="col-md-7 detail-paket3">
                  Malang Kota Batu + Jatim Park
                </div>
                <div class="col-md-7 detail-paket4">
                  Durasi : 3 Hari 3 Malam
                </div>
                <div class="col-md-7 detail-paket5">
                  Tanggal Keberangkatan : 15 April 2018
                </div>
                <div class="col-md-7 detail-paket6">
                  Mulai :
                </div>
                <div class="col-md-7 detail-paket7">
                  10 Poin + Rp. 20.000.000
                </div>
              </div>

            </div>
            
          </div>
        </div>
      </div>
<div id="fh5co-contact" class="fh5co-section-gray">
  <div class="container">

    <div class="row">
      <div class="col-md-4">
        <div class="kiri">
          <div class="foto1">
            <div class="lingkaran">
              <img class="lingkaran" src="<?php echo $profile->foto ?>">
            </div>
          </div>              
          <div class="nama">
            <?php echo $profile->nama; ?>
          </div>
          <div class="point">
            Point : 300
          </div>
          <a href="">
            <div class="menu">
              <div class="text">Edit Profile</div>
            </div>
          </a>
          <a href="">
            <div class="menu">
              <div class="text">Point Saya</div>
            </div>
          </a>
          <a href="">
            <div class="menu mpan">
              <div class="text">Pesanan Saya</div>
            </div>
          </a>
          <a href="">
            <div class="menu">
              <div class="text">Pengaturan akun</div>
            </div>
          </a>
          <a href="">
            <div class="menu">
              <div class="text">Log Out</div>
            </div>
          </a>
        </div>  
      </div>

      <div class="col-md-8">
        <div class="col-md-12 judul-pesanan" style="padding: unset;">
          Konfirmasi Pembayaran
        </div>
        <div class="col-md-12 paket" style="margin-bottom: 15px;">
          <div class="col-md-5 gambar-pkt">
            <img src="<?php echo $pembayaran->gambar_paket; ?>">
          </div>
          <div class="col-md-7 judul-pesanan" style="font-size: 20px;">
            <!-- LHS1534204335 -->
            <?php echo $pembayaran->id_list_hold_seat; ?>
          </div>
          <div class="col-md-7 detail-paket3">
            <?php echo $pembayaran->nama_paket_tour; ?>
          </div>
          <div class="col-md-7 detail-paket4">
            Durasi : <?php echo $pembayaran->jml_keberangkatan; ?> Hari <?php echo $pembayaran->jml_keberangkatan; ?> Malam
          </div>
          <div class="col-md-7 detail-paket5">
            Tanggal Keberangkatan : <?php date('d F Y', strtotime($pembayaran->tanggal_keberangkatan)); ?>
          </div>
          <div class="col-md-7 detail-paket6">
            Status
          </div>
          <div class="col-md-7 detail-paket7">
            <!-- Pembayaran DP 30% -->
            <?php echo $pembayaran->status; ?>
          </div>
          <div class="col-md-12 line" style="margin-top: 15px; margin-bottom: 10px;">
            
          </div>
          <div class="col-md-8 judul-pesanan" style="font-size: 20px;">
            Riwayat Pembayaran
          </div>
          <div class="col-md-12 table-responsive">
            <table class="table table-striped">
              <tr>
                <th>
                  ID Pembayaran
                </th>
                <th>
                  Tgl Pembayaran
                </th>
                <th>
                  Jumlah
                </th>
                <th>
                  Status
                </th>
              </tr>
              <?php foreach ($log as $l): ?>
              <tr>
                <td>
                  <?php echo $l->id_pembayaran; ?>
                </td>
                <td>
                  <?php echo date('d F Y', strtotime($l->tanggal)); ?>
                </td>
                <td>
                  Rp. <?php echo number_format($l->jumlah_tf, 2, ",", ".") ?>
                </td>
                <td>
                  <?php echo $l->status_valid; ?>
                </td>
              </tr>
              <?php endforeach ?>
              <!-- <tr>
                <td>
                  3448946
                </td>
                <td>
                  23-Agustus-2018
                </td>
                <td>
                  Rp. 5000.0000
                </td>
                <td>
                  Valid
                </td>
              </tr>
              <tr>
                <td>
                  3448946
                </td>
                <td>
                  02-Septmber-2018
                </td>
                <td>
                  Rp. 2300.0000
                </td>
                <td>
                  Valid
                </td>
              </tr>
              <tr>
                <td>
                  3448946
                </td>
                <td>
                  23-Agustus-2018
                </td>
                <td>
                  Rp. 400.0000
                </td>
                <td>
                  Valid
                </td>
              </tr> -->
            </table>
            <div class="col-md-8 judul-pesanan" style="font-size: 20px;">
              <?php if (empty($total->jumlah_tf)): ?>
                <?php $jumlah_tf = 0; ?>
              <?php else: ?>
                <?php $jumlah_tf = $total->jumlah_tf; ?>
              <?php endif ?>
              <?php $tagihan = $pembayaran->harga - $jumlah_tf; ?>
              <?php if ($tagihan != 0): ?>
                Tagihan Anda: Rp. <?php echo number_format($tagihan, 2, ",", "."); ?>
              <?php else: ?>
                Pembayaran Lunas
              <?php endif ?>
            </div>
          </div>                                
          <div class="col-md-12 line" style="margin-top: 15px; margin-bottom: 10px;">
            
          </div>
          <div class="col-md-8 judul-pesanan" style="font-size: 20px;">
            Konfirmasi Pembayaran
          </div>
          <div class="col-md-12" style="padding: unset;">
            <form method="POST" action="<?php echo site_url() ?>/KonfirmasiPembayaran/bayar" enctype="multipart/form-data">
              <input type="hidden" name="lhs" value="<?php echo $pembayaran->id_list_hold_seat; ?>">
              <div class="col-md-6 row-edit">
                <div class="col-md-12">
                  <label>Metode pembayaran</label>
                  <select style="width: 100%" name="tipe">
                    <!-- <option value="1">Transfer</option>
                    <option value="1">DOKU</option> -->
                    <?php foreach ($tipe_pmb as $tp): ?>
                    <option value="<?php echo $tp->id_tipe_pembayaran ?>"><?php echo $tp->tipe_pembayaran; ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6 row-edit">
                <div class="col-md-12">
                  <label>Pilih Bank</label>
                  <select style="width: 100%" name="norek">
                    <!-- <option value="1">Mandiri (0808)</option>
                    <option value="2">BNI (0909)</option>
                    <option value="3">BCA (0101)</option> -->
                    <?php foreach ($bank as $b): ?>
                    <option value="<?php echo $b->id_rekening_bank ?>"><?php echo $b->nama_bank; ?> (<?php echo $b->norek; ?>)</option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6 row-edit">
                <div class="col-md-12">
                  <label>Rekening Transfer</label>
                  <input type="text" class="inText" name="rektf" style="height: 36px;">
                </div>
              </div>
              <div class="col-md-6 row-edit">
                <div class="col-md-12">
                  <label>Jumlah Transfer</label>
                  <input type="text" class="inText mask_money" name="jumlahtf" style="height: 36px;">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-12">
                  <label>Nomor Struk Transfer</label>
                  <input type="text" class="inText" name="nobuktitf" style="height: 36px;">
                </div>
              </div>
              <div class="col-md-12 row-edit">
                <div class="col-md-12">
                  <label>Bukti Transfer</label>                       
                  <div class="upload-wraper">
                    <input id="uploadFile" placeholder="Pilih file..." disabled="disabled" />
                    <div class="fileUpload btn-upload">
                      <span>Upload</span>
                      <input id="uploadBtn" name="gambar" type="file" class="upload" />
                    </div>
                  </div>
                  <!-- <input type="file" name="gambar"> -->
                </div>
              </div>
              <div class="col-md-5 col-md-offset-7" style="margin-top: 15px;">
                <button type="submit" class="btn-simpan">Konfirmasi Pembayaran</button>
              </div>
            </form>
          </div>                
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
.row-edit{
  padding: unset;
}
.line{
  border-color: #D2D2D2;
  padding: unset;
}
</style>
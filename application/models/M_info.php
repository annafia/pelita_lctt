<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_info extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function insertData($tableName, $data)
    {
        $res = $this->db->insert($tableName, $data);
        return $res;
    }

    public function updateData($tableName, $data, $where)
    {
        $res = $this->db->update($tableName, $data, $where);
        return $res;
    }

    public function deleteData($tableName, $where)
    {
        $res = $this->db->delete($tableName, $where);
        return $res;
    }

    public function getEvent($where = "") {
        $data = $this->db->query('select * from event ' . $where);
        return $data;
    }

    public function getBerita($where = "") {
        $data = $this->db->query('select * from berita ' . $where);
        return $data;
    }

    public function getPromo($where = "") {
        $data = $this->db->query('select * from slider ' . $where);
        return $data;
    }

    public function getTour($where = "") {
        $data = $this->db->query('select a.id_paket_tour, b.id_negara, c.id_benua, d.id_jadwal_tour, e.id_harga_tour,
                a.nama_paket_tour, b.negara, c.benua, d.max_kuota, d.tanggal_keberangkatan, min(e.harga) as harga, coalesce(sum(f.kuota), 0) as kuota, g.gambar_paket,
                max(h.hari_ke) as hari
                from paket_tour a
                join negara b
                on a.id_negara=b.id_negara
                join benua c
                on b.id_benua = c.id_benua
                join jadwal_tour d
                on a.id_paket_tour=d.id_paket_tour
                left join harga_tour e
                on d.id_jadwal_tour=e.id_jadwal_tour
                left join list_hold_seat f
                on e.id_harga_tour=f.id_harga_tour
                left join
                (
                    select a1.id_paket_tour, a1.gambar_paket, a1.hapus
                    from gambar_paket a1
                    where a1.hapus = 0
                    group by a1.id_paket_tour
                ) g
                on a.id_paket_tour=g.id_paket_tour
                join itinerary h
                on a.id_paket_tour=h.id_paket_tour ' . $where);
        return $data;
    }
}


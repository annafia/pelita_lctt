<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_konf_pem extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function insertData($tableName, $data)
    {
        $res = $this->db->insert($tableName, $data);
        return $res;
    }

    public function updateData($tableName, $data, $where)
    {
        $res = $this->db->update($tableName, $data, $where);
        return $res;
    }

    public function deleteData($tableName, $where)
    {
        $res = $this->db->delete($tableName, $where);
        return $res;
    }

    public function getAllPembayaran($where = "")
    {
        // $data = $this->db->query('select a.id_paket_tour, b.id_jadwal_tour, c.id_harga_tour, d.id_list_hold_seat, a.nama_paket_tour, b.tanggal_keberangkatan, b.max_kuota, c.keterangan, c.harga, d.time_expired, d.id_member, coalesce(c.harga - sum(f.jumlah_tf)) as tagihan, d.status, max(h.hari_ke) as durasi, g.negara, i.jml_keberangkatan, j.gambar_paket
        //         from paket_tour a
        //         join jadwal_tour b
        //         on a.id_paket_tour=b.id_paket_tour
        //         join harga_tour c
        //         on b.id_jadwal_tour=c.id_jadwal_tour
        //         join list_hold_seat d
        //         on c.id_harga_tour=d.id_harga_tour
        //         left join
        //         (
        //             select a1.id_list_hold_seat, b1.jumlah_tf
        //             from pembayaran a1
        //             left join p_transfer b1
        //             on a1.id_pembayaran=b1.id_pembayaran
        //             where b1.status_valid != "tidak valid"
        //         ) f
        //         on d.id_list_hold_seat=f.id_list_hold_seat
        //         left join negara g
        //         on g.id_negara=a.id_negara
        //         left join itinerary h
        //         on h.id_paket_tour=a.id_paket_tour
        //         left join
        //         (
        //             select a.id_paket_tour, count(b.id_jadwal_tour) as jml_keberangkatan
        //             from paket_tour a
        //             join jadwal_tour b
        //             on a.id_paket_tour=b.id_paket_tour
        //             where b.hapus=0
        //             group by a.id_paket_tour
        //             order by a.id_paket_tour asc
        //         ) i
        //         on a.id_paket_tour = i.id_paket_tour
        //         left join
        //         (
        //             select a1.id_paket_tour, a1.gambar_paket
        //             from gambar_paket a1
        //             where a1.hapus = 0
        //             group by a1.id_paket_tour
        //         ) j
        //         on a.id_paket_tour=j.id_paket_tour '
        //         . $where .
        //         ' group by d.id_list_hold_seat');

        $data = $this->db->query('select a.id_paket_tour, b.id_jadwal_tour, c.id_harga_tour, d.id_list_hold_seat, a.nama_paket_tour, b.tanggal_keberangkatan, b.max_kuota, c.keterangan, c.harga, d.time_expired, d.id_member, d.status, max(h.hari_ke) as durasi, g.negara, i.jml_keberangkatan, j.gambar_paket
                from paket_tour a
                join jadwal_tour b
                on a.id_paket_tour=b.id_paket_tour
                join harga_tour c
                on b.id_jadwal_tour=c.id_jadwal_tour
                join list_hold_seat d
                on c.id_harga_tour=d.id_harga_tour
                left join negara g
                on g.id_negara=a.id_negara
                left join itinerary h
                on h.id_paket_tour=a.id_paket_tour
                left join
                (
                    select a.id_paket_tour, count(b.id_jadwal_tour) as jml_keberangkatan
                    from paket_tour a
                    join jadwal_tour b
                    on a.id_paket_tour=b.id_paket_tour
                    where b.hapus=0
                    group by a.id_paket_tour
                    order by a.id_paket_tour asc
                ) i
                on a.id_paket_tour = i.id_paket_tour
                left join
                (
                    select a1.id_paket_tour, a1.gambar_paket
                    from gambar_paket a1
                    where a1.hapus = 0
                    group by a1.id_paket_tour
                ) j
                on a.id_paket_tour=j.id_paket_tour '
            . $where .
            ' group by d.id_list_hold_seat');

        return $data;
    }

    public function getNorek($where = "")
    {
        $data = $this->db->query('select * from rekening_bank ' . $where);
        return $data;
    }

    public function getTipeTransfer($where = "")
    {
        $data = $this->db->query('select * from tipe_pembayaran ' . $where);
        return $data;
    }

    public function getLogPembayaran($where = "")
    {
        $data = $this->db->query('select * from
                pembayaran a
                left join p_transfer b
                on a.id_pembayaran=b.id_pembayaran ' . $where);
        return $data;
    }

    public function getCheckPembayaran($where = "")
    {
        $data = $this->db->query('select a.id_list_hold_seat, count(id_list_hold_seat) as jml_tdk_valid from
                pembayaran a
                left join p_transfer b
                on a.id_pembayaran=b.id_pembayaran '
            . $where .
            ' group by a.id_list_hold_seat');
        return $data;
    }

    public function getTotalPembayaran($where = "")
    {
        $data = $this->db->query('select a1.id_list_hold_seat, coalesce(sum(c1.jumlah_tf),0) as jumlah_tf
                from list_hold_seat a1
                left join pembayaran b1
                on a1.id_list_hold_seat=b1.id_list_hold_seat
                left join p_transfer c1
                on b1.id_pembayaran=c1.id_pembayaran '
            . $where .
            ' group by a1.id_list_hold_seat');
        return $data;
    }

    public function getListPemVisa($where = "")
    {
        $data = $this->db->query('select *
                from pembayaran_visa a
                join pengajuan_visa b
                on a.id_pembayaran_visa=b.id_pembayaran_visa
                join harga_visa c
                on b.id_harga_visa=c.id_harga_visa
                join negara d
                on c.id_negara=d.id_negara
                join visa_umur e
                on c.id_visa_umur=e.id_visa_umur
                join status_visa f
                on b.id_status_visa=f.id_status_visa ' . $where);
        return $data;
    }

    public function getPaspor($where = "")
    {
        $data = $this->db->query('select *
                from inv_paspor a
                join paspor b
                on a.id_inv_paspor=b.id_inv_paspor
                join harga_paspor c
                on b.id_harga_paspor=c.id_harga_paspor
                join kantor_imigrasi d
                on c.id_kantor_imigrasi=d.id_kantor_imigrasi
                join umur_paspor e
                on c.id_umur_paspor=e.id_umur_paspor
                join tipe_paspor f
                on a.id_tipe_paspor=f.id_tipe_paspor
                join status_paspor g
                on a.id_status_paspor=g.id_status_paspor ' . $where);
        return $data;
    }
}

<?php
class M_tour extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function get_negara()
    {
        $query = "SELECT * FROM negara n JOIN benua b ON b.id_benua= n.id_benua where n.hapus= 0 and b.hapus= 0";
        return $this->db->query($query);
    }
    public function get_benua(){
        $res = $this->db->where("hapus",0)->get('benua');
        return $res->result();
    }
    public function find_negara($id_benua){
        $res = $this->db->where("hapus",0)->where('id_benua',$id_benua)->get('negara');
        return $res->result_array();
    }

    public function get_paket($start = null, $end = null, $benua = null, $negara = null, $filter = null, $limit = null)
    {
        // $where_negara  = null;
        // $where_tanggal = null;
        // $order_by      = null;

        // if ($filter == "asc") {
        //     $order_by = "order by ht.harga asc";
        // } elseif ($filter == "desc") {
        //     $order_by = "order by ht.harga desc";
        // }

        // if ($negara != null) {
        //     $where_negara = " AND pt.id_negara='$negara'";
        // }

        // if ($tanggal != null) {
        //     $where_tanggal = " AND jt.tanggal_keberangkatan <= date('$tanggal')";
        // }

        // $query = "
        // SELECT *, (select count(id_itinerary) from itinerary i where i.id_paket_tour= pt.id_paket_tour) as total_hari
        // FROM jadwal_tour jt
        //     JOIN
        //         paket_tour pt ON jt.id_paket_tour= pt.id_paket_tour
        //     JOIN
        //         negara n  ON n.id_negara= pt.id_negara
        //     JOIN
        //         benua b ON b.id_benua= n.id_benua
        //     JOIN
        //         gambar_paket gp ON gp.id_paket_tour= pt.id_paket_tour
        //     JOIN
        //         harga_tour ht ON ht.id_jadwal_tour= jt.id_jadwal_tour
        //     WHERE
        //         jt.tanggal_keberangkatan > CURRENT_DATE()
        //         AND
        //         jt.hapus = 0
        //         AND
        //         pt.hapus=0
        //     " . $where_tanggal . $where_negara . " GROUP BY jt.tanggal_keberangkatan " . $order_by  . $limit;
        if($benua==0)
            $benua=null;
        if($negara==0)
            $negara=null;
        
        $where = "where b.tanggal_keberangkatan > CURRENT_DATE() and a.hapus = 0 and b.hapus = 0";
        $order_by = "";
        if ($filter == "asc") {
            $order_by = "order by c.harga asc";
        } elseif ($filter == "desc") {
            $order_by = "order by c.harga desc";
        }
        if ($benua != null) {
            $where .= " and d.id_benua = $benua";
        }
        if ($negara != null) {
            $where .= " and g.id_negara = $negara";
        }

        if ($start != null) {
            $where .= " and b.tanggal_keberangkatan >= '$start'";
        }
        if ($end != null) {
            $where .= " and b.tanggal_keberangkatan <= '$end'";
        }

        $query = 'select *,
                max(h.hari_ke) as total_hari, i.jml_keberangkatan, j.gambar_paket
                from paket_tour a
                join jadwal_tour b
                on a.id_paket_tour=b.id_paket_tour
                join harga_tour c
                on b.id_jadwal_tour=c.id_jadwal_tour
                left join negara g
                on g.id_negara=a.id_negara
                left join benua d
                on g.id_benua=d.id_benua
                left join itinerary h
                on h.id_paket_tour=a.id_paket_tour
                left join
                (
                    select a.id_paket_tour, count(b.id_jadwal_tour) as jml_keberangkatan
                    from paket_tour a
                    join jadwal_tour b
                    on a.id_paket_tour=b.id_paket_tour
                    where b.hapus=0
                    group by a.id_paket_tour
                    order by a.id_paket_tour asc
                ) i
                on a.id_paket_tour = i.id_paket_tour
                left join
                (
                    select a1.id_paket_tour, a1.gambar_paket
                    from gambar_paket a1
                    where a1.hapus = 0
                    group by a1.id_paket_tour
                ) j
                on a.id_paket_tour=j.id_paket_tour '
            . $where .
            ' group by b.id_jadwal_tour ' . $order_by ." " . $limit;


        return $this->db->query($query);
    }

    public function harga_tour($id_paket_tour, $tanggal_keberangkatan = null)
    {
        return $this->db->query("SELECT * FROM harga_tour ht join jadwal_tour jt on jt.id_jadwal_tour= ht.id_jadwal_tour where id_paket_tour= '$id_paket_tour' AND tanggal_keberangkatan= '$tanggal_keberangkatan'");
    }

    public function itinerary_tour($id_paket_tour)
    {
        return $this->db->query("SELECT * FROM itinerary where id_paket_tour= '$id_paket_tour'");
    }

    public function gambar_tour($id_paket_tour)
    {
        return $this->db->query("SELECT * FROM gambar_paket where id_paket_tour= '$id_paket_tour' and hapus = 0");
    }

    public function data_tour($id_paket_tour)
    {
        $query = "
        SELECT *, (select count(id_itinerary) from itinerary i where i.id_paket_tour= pt.id_paket_tour) as total_hari
        FROM
            paket_tour pt
            JOIN
                negara n on pt.id_negara= n.id_negara
            JOIN benua b on b.id_benua= n.id_benua
             where pt.id_paket_tour= '$id_paket_tour'";
        return $this->db->query($query);
    }

    public function get_jenis_faq()
    {
        return $this->db->get('jenis_faq');
    }

    public function get_faq()
    {
        return $this->db->get('faq');
    }

    public function get_slider()
    {
        return $this->db->get('slider');
    }

    public function get_data_tour($id_jadwal_tour, $id_harga)
    {
        $query = "
        SELECT *, (SELECT COUNT(i.id_itinerary) FROM itinerary i WHERE i.id_paket_tour= pt.id_paket_tour) as total_hari
        FROM
        harga_tour ht
        LEFT JOIN jadwal_tour jt ON jt.id_jadwal_tour= ht.id_jadwal_tour
        LEFT JOIN paket_tour pt ON pt.id_paket_tour= jt.id_paket_tour
        LEFT JOIN negara n ON n.id_negara= pt.id_negara
        LEFT JOIN benua b ON b.id_benua= n.id_benua
        WHERE jt.id_jadwal_tour ='$id_jadwal_tour'
        AND ht.id_harga_tour='$id_harga'";
        return $this->db->query($query);
    }

    public function i_list_hold($id_list_hold_seat, $id_member, $id_harga, $jumlah)
    {
        $waktu       = date("Y-m-d H:i:s");
        $waktu_habis = date('Y-m-d H:i:s', strtotime('1 hour'));
        $data        = array('id_list_hold_seat' => $id_list_hold_seat, 'id_member' => $id_member, 'id_harga_tour' => $id_harga, 'kuota' => $jumlah, 'time_book' => $waktu, 'time_expired' => $waktu_habis, 'status' => 'booked');
        // $this->db->insert('list_hold_seat', $data);
        // return $this->db->query("SELECT * FROM list_hold_seat WHERE time_book='$waktu'");
        $res = $this->db->insert('list_hold_seat', $data);
        return $res;
    }

    public function i_keluarga($id_list_hold_seat = "", $nama_depan = "", $nama_belakang = "", $umur = "", $tanggal_lahir = "", $no_hp = "", $nomor_id = "", $kategori_usia = "")
    {
        $data = array(
            'id_list_hold_seat' => $id_list_hold_seat,
            'nama_depan'        => $nama_depan,
            'nama_belakang'     => $nama_belakang,
            'umur'              => $umur,
            'tanggal_lahir'     => $tanggal_lahir,
            'no_hp'             => $no_hp,
            'nomor_id'          => $nomor_id,
            'kategori_usia'     => $kategori_usia,
        );
        $res = $this->db->insert('anggota_keluarga', $data);
        return $res;
    }

}

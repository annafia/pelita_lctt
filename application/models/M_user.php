<?php
class M_user extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function login($email, $password)
    {
        $this->db->where('id_member', $email);
        $this->db->where('password', $password);
        return $this->db->get('member');
    }

    public function register($data)
    {
        $this->db->insert('member', $data);
    }

    public function get_user($email)
    {
        $this->db->where('id_member', $email);
        return $this->db->get('member');
    }

    public function insertData($tableName, $data)
    {
        $res = $this->db->insert($tableName, $data);
        return $res;
    }

    public function updateData($tableName, $data, $where)
    {
        $res = $this->db->update($tableName, $data, $where);
        return $res;
    }

    public function deleteData($tableName, $where)
    {
        $res = $this->db->delete($tableName, $where);
        return $res;
    }

}

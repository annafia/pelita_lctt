<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_visapaspor extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function insertData($tableName, $data)
    {
        $res = $this->db->insert($tableName, $data);
        return $res;
    }

    public function updateData($tableName, $data, $where)
    {
        $res = $this->db->update($tableName, $data, $where);
        return $res;
    }

    public function deleteData($tableName, $where)
    {
        $res = $this->db->delete($tableName, $where);
        return $res;
    }

    
    public function getNegara($where = "")
    {
        $data = $this->db->query('select * from negara ' . $where);
        return $data;
    }

    public function getJenisVisa($where = "")
    {
        $data = $this->db->query('select *
                from jenis_visa a
                join harga_visa b
                on a.id_jenis_visa=b.id_jenis_visa '
                . $where .
                ' group by a.id_jenis_visa');
        return $data;
    }

    public function getHargaVisa($where = "")
    {
        $data = $this->db->query('select *
                from jenis_visa a
                join harga_visa b
                on a.id_jenis_visa=b.id_jenis_visa ' . $where);
        return $data;
    }

    public function getTipePaspor($where = "")
    {
        $data = $this->db->query('select * from tipe_paspor ' . $where);
        return $data;
    }

    public function getKantorImigrasi($where = "")
    {
        $data = $this->db->query('select * from kantor_imigrasi ' . $where);
        return $data;
    }

    public function getHargaPaspor($where = "")
    {
        $data = $this->db->query('select * from harga_paspor ' . $where);
        return $data;
    }
}

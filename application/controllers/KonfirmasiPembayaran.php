<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KonfirmasiPembayaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->library('image_lib');
        date_default_timezone_set("Asia/Bangkok");

    }

    public function pembayaran($id)
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata']   = $userdata;
        $data['profile']    = $this->M_user->get_user($user)->row();
        $data['pembayaran'] = $this->M_konf_pem->getAllPembayaran("where d.id_list_hold_seat = '$id' and d.hapus = 0")->row();
        $data['total']      = $this->M_konf_pem->getTotalPembayaran("where c1 . status_valid != 'tidak valid' and a1 . id_list_hold_seat = '$id'")->row();
        $data['log']        = $this->M_konf_pem->getLogPembayaran("where a.id_list_hold_seat = '$id'")->result(); //LHS1525860193
        $data['bank']       = $this->M_konf_pem->getNorek()->result();
        $data['tipe_pmb']   = $this->M_konf_pem->getTipeTransfer()->result();

        // echo print_r($data['log']);

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('user/konfirmasi-pembayaran', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function bayar()
    {
        // echo print_r($this->input->post());

        $id_pembayaran      = "TF" . time();
        $id_tipe_pembayaran = $this->input->post('tipe');
        $id_list_hold_seat  = $this->input->post('lhs');

        $dataInsert = array(
            'id_pembayaran'      => $id_pembayaran,
            'id_tipe_pembayaran' => $id_tipe_pembayaran,
            'id_list_hold_seat'  => $id_list_hold_seat,
        );

        $execute = $this->M_konf_pem->insertData('pembayaran', $dataInsert);

        if ($execute >= 1) {
            if ($id_tipe_pembayaran == 1) {
                $id_rekening_bank = $this->input->post('norek');
                $rek_transfer     = $this->input->post('rektf');
                $nobuktitf        = $this->input->post('nobuktitf');
                $tanggal          = date('Y-m-d');
                $jumlah_tf        = $this->input->post('jumlahtf');

                $config['upload_path']   = './uploads/buktiTF/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = 2048;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('gambar')) {
                    echo "tidak ada gambar";
                    // $error = array('error' => $this->upload->display_errors());
                } else {
                    $data    = array('upload_data' => $this->upload->data());
                    $addData = array(
                        'id_pembayaran'    => $id_pembayaran,
                        'id_rekening_bank' => $id_rekening_bank,
                        'rek_transfer'     => $rek_transfer,
                        'tanggal'          => $tanggal,
                        'jumlah_tf'        => str_replace(str_split('_.'), "", $jumlah_tf),
                        'no_bukti_tf'      => $nobuktitf,
                        'time_konf'        => date('y-m-d H:i:s'),
                        'bukti_tf'         => base_url() . "uploads/buktiTF/" . $this->upload->data('file_name'),
                        'status_valid'     => 'menunggu validasi',
                    );
                    $execute = $this->M_konf_pem->insertData('p_transfer', $addData);
                    if ($execute >= 1) {
                        $this->session->set_flashdata('popUp', 'true');
                        redirect('KonfirmasiPembayaran/');
                    } else {

                    }
                }
            }
        } else {
            echo "error";
        }
    }

}

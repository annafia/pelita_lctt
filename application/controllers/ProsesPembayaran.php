<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProsesPembayaran extends CI_Controller {
  public function  __construct(){
    parent:: __construct();
    $this->load->helper('url');
    $this->load->library('session');
    $this->load->library('upload');
    $this->load->library('image_lib');
    date_default_timezone_set("Asia/Bangkok");
    
  }
  public function prosespembayaran()
  {
    $this->load->view('header');
    $this->load->view('head');
    $this->load->view('proses-pembayaran');
    $this->load->view('footer');
    $this->load->view('footer-js');
  }
    public function reportpembayaran()
  {
    $this->load->view('header');
    $this->load->view('head');
    $this->load->view('report-pembayaran');
    $this->load->view('footer');
    $this->load->view('footer-js');
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function  __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('image_lib');
		date_default_timezone_set("Asia/Bangkok");
		
	}
	public function index()
	{
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;
		$data['negara']= $this->M_tour->get_negara()->result();
		$data['paket_tour']= $this->M_tour->get_paket(null, null, null, ' order by rand() limit 5 ')->result();
		$data['rand_paket_tour']= $this->M_tour->get_paket(null, null, null, 'order by rand() limit 1')->result();
		$data['slider']= $this->M_tour->get_slider()->result();
		$this->load->view('template/header', $data);
		$this->load->view('template/head', $data);
		$this->load->view('template/home2', $data);
		$this->load->view('template/footer', $data);
		$this->load->view('template/footer-js', $data);
	}
	public function login(){
		if ($this->input->post()) {
			$email= $this->input->post('email');
			$password= $this->input->post('password');
			$password= md5($password);
			$user_account=  $this->M_user->login($email, $password);
			if ($user_account->row() != null) {
				$userdata = array('usermember' => $user_account->row()->id_member );
				$this->session->set_userdata('userdata_login', $userdata);
				$this->session->set_flashdata('info', "Login berhasil");
			}else{
				$this->session->set_flashdata('info', "Login gagal");
			}
		}
		redirect('Home');
	}
	public function register(){

		
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;
		if ($userdata == null) {
			$this->load->view('template/header', $data);
			$this->load->view('template/head', $data);
			$this->load->view('template/register', $data);
			$this->load->view('template/footer', $data);
			$this->load->view('template/footer-js', $data);
		}else{
			// redirect('Home');
		}
		
	}
	public function beritapromoevent(){
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;

		$this->load->view('template/header', $data);
		$this->load->view('template/head', $data);
		$this->load->view('beritapromoevent/berita-promo-event');
		$this->load->view('template/footer');
		$this->load->view('template/footer-js');		
	}
	public function detailberita(){
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;

		$this->load->view('template/header', $data);
		$this->load->view('template/head', $data);
		$this->load->view('beritapromoevent/detail-berita');
		$this->load->view('template/footer');
		$this->load->view('template/footer-js');		
	}
	public function detailpromo(){
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;

		$this->load->view('template/header', $data);
		$this->load->view('template/head', $data);
		$this->load->view('beritapromoevent/detail-promo');
		$this->load->view('template/footer');
		$this->load->view('template/footer-js');		
	}
	public function i_register(){
		if ($this->input->post()) {
			$cek_email= $this->M_user->get_user($this->input->post('email'));
			if ($cek_email->row() != null) {

				$this->session->set_flashdata('info', "Registrasi gagal, email sudah digunakan");
				redirect('Home');
			}
			$register=  $this->input->post();
			$config['upload_path'] = 'uploads/';
			$config['allowed_types']='gif|jpeg|png|jpg|pdf';
			$config['encrypt_name']= TRUE;
			$this->upload->initialize($config);
			if ($this->upload->do_upload('userfile')) {
				$dok =$this->upload->data();
				$file = $dok['file_name'];
				$register['foto']= $file;
				$register['id_member']= $register['email'];
				$register['nama']= $register['nama_depan'].' '.$register['nama_belakang'];
				$register['password']= md5($register['password']);
				$userdata = array('usermember' => $register['id_member'] );
				$this->session->set_userdata('userdata_login', $userdata);
				unset($register['nama_depan']);
				unset($register['nama_belakang']);
				unset($register['email']);
				// print_r($register);
				$this->M_user->register($register);	
				$this->session->set_flashdata('info', "Registrasi berhasil");
			}else{	
				$this->session->set_flashdata('info', "Registrasi gagal, foto tidak sesuai dengan format");
			}
			redirect('Home');
		}
	}
	// public function index_old()
	// {
	// 	$this->load->view('header');
	// 	$this->load->view('head');
	// 	$this->load->view('home');
	// 	$this->load->view('footer');
	// 	$this->load->view('footer-js');
	// }
}

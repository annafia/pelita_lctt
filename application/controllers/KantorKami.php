<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KantorKami extends CI_Controller {
	public function  __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('image_lib');
		date_default_timezone_set("Asia/Bangkok");
		
	}
	public function kantorkami()
	{	
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;	

		$data['cabang'] = $this->M_kantor->getCabang("where hapus = 0")->result();

		$this->load->view('template/header');
		$this->load->view('template/head',$data);
		$this->load->view('kantorkami/kantorkami');
		$this->load->view('template/footer');
		$this->load->view('template/footer-js');
	}
}

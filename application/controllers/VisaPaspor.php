<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VisaPaspor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->library('image_lib');
        date_default_timezone_set("Asia/Bangkok");

    }
    public function pencarianvisapaspor()
    {
        $userdata         = $this->session->userdata('userdata_login');
        $data['userdata'] = $userdata;

        $data['jenis_faq'] = $this->M_tour->get_jenis_faq()->result();
        $data['faq']       = $this->M_tour->get_faq()->result();

        $data['tipe_paspor'] = $this->M_visapaspor->getTipePaspor()->result();
        $data['imigrasi']    = $this->M_visapaspor->getKantorImigrasi()->result();

        $data['negara']     = $this->M_visapaspor->getNegara("where hapus = 0")->result();
        $data['jenis_visa'] = $this->M_visapaspor->getJenisVisa()->result();
        $data['durasi']     = $this->M_visapaspor->getHargaVisa("where hapus = 0")->result();
        $data['ajax']       = $this->load->view("visapaspor/ajaxVisa", null, true);

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('visapaspor/pencarianvisapaspor', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function jsonTipeVisa()
    {
        $id_negara         = $this->input->post('id_negara');
        $data['jenisVisa'] = $this->M_visapaspor->getJenisVisa("where b.id_negara = $id_negara and b.hapus = 0")->result();
        $data['durasi']    = $this->M_visapaspor->getHargaVisa("where id_negara = $id_negara and hapus = 0")->result();
        echo json_encode($data);
    }

    public function jsonDurasi()
    {
        $id_jenis_visa = $this->input->post('id_jenis_visa');
        $data          = $this->M_visapaspor->getHargaVisa("where id_jenis_visa = $id_jenis_visa and hapus = 0 group by id_jenis_visa")->result();
        echo json_encode($data);
    }

    public function jsonImigrasi()
    {
        $imigrasi = $this->input->post('imigrasi');
        $data     = $this->M_visapaspor->getKantorImigrasi("where id_kantor_imigrasi = $imigrasi")->result();
        echo json_encode($data);
    }

    public function isidatavisa()
    {
        $userdata         = $this->session->userdata('userdata_login');
        $data['userdata'] = $userdata;

        $negara     = $this->input->post('negara');
        $dewasa     = $this->input->post('dewasa');
        $anak       = $this->input->post('anak');
        $bayi       = $this->input->post('bayi');
        $tgl        = $this->input->post('tgl');
        $jenis_visa = $this->input->post('jenis_visa');
        $durasi     = $this->input->post('durasi');

        // echo print_r($this->input->post());

        $nama_negara  = $this->M_visapaspor->getNegara("where id_negara = $negara")->row();
        $harga_dewasa = $this->M_visapaspor->getHargaVisa("where b.id_negara = $negara and b.id_jenis_visa = $jenis_visa and b.id_visa_umur = 1")->row();
        $harga_anak   = $this->M_visapaspor->getHargaVisa("where b.id_negara = $negara and b.id_jenis_visa = $jenis_visa and b.id_visa_umur = 2")->row();
        $harga_bayi   = $this->M_visapaspor->getHargaVisa("where b.id_negara = $negara and b.id_jenis_visa = $jenis_visa and b.id_visa_umur = 3")->row();

        $harga = ($dewasa * $harga_dewasa->harga_visa) + ($anak * $harga_anak->harga_visa) + ($bayi * $harga_bayi->harga_visa);

        $data['nama_negara']  = $nama_negara->negara;
        $data['durasi']       = $harga_dewasa->lama_pembuatan;
        $data['dewasa']       = $dewasa;
        $data['anak']         = $anak;
        $data['bayi']         = $bayi;
        $data['harga_dewasa'] = $harga_dewasa;
        $data['harga_anak']   = $harga_anak;
        $data['harga_bayi']   = $harga_bayi;
        $data['tgl']          = $tgl;
        $data['jenisVisa']    = $harga_dewasa->jenis_visa;
        $data['harga']        = $harga;

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('visapaspor/isidatavisa', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function addVisa()
    {
        // echo "<pre>";
        // echo print_r($this->input->post());
        // echo "</pre>";

        $nd_pemesan    = $this->input->post('nd_pemesan');
        $nb_pemesan    = $this->input->post('nb_pemesan');
        $email_pemesan = $this->input->post('email_pemesan');
        $nohp_pemesan  = $this->input->post('nohp_pemesan');
        $tglbrk        = $this->input->post('tglbrk');

        $hargaVisa = $this->input->post('hargaVisa');
        $radio     = $this->input->post('radio');
        $nama      = $this->input->post('nama');
        $tgl       = $this->input->post('tgl');
        $kw        = $this->input->post('kw');
        $nopas     = $this->input->post('nopas');
        $npp       = $this->input->post('npp');
        $intgl     = $this->input->post('intgl');
        $exptgl    = $this->input->post('exptgl');
        $domisili  = $this->input->post('domisili');

        $id_pembayaran_visa = "TFVS" . time();

        $inv = array(
            'id_pembayaran_visa' => $id_pembayaran_visa,
            'id_member'          => $email_pemesan,
            'status'             => 'menunggu validasi',
        );
        $execute = $this->M_visapaspor->insertData('pembayaran_visa', $inv);
        if ($execute >= 1) {
            $res = "";
            foreach ($nama as $key => $value) {
                // if ($radio[$key] == 1) {
                //     $radio[$key] = "Mr .";
                // } elseif ($radio[$key] == 2)  {
                //     $radio[$key] = "Mrs .";
                // }
                $addData = array(
                    'id_harga_visa'         => $hargaVisa[$key],
                    'id_status_visa'        => 4,
                    'id_pembayaran_visa'    => $id_pembayaran_visa,
                    'nama_lengkap'          => $value,
                    'kewarganegaraan'       => $kw[$key],
                    'no_paspor'             => $nopas[$key],
                    'penerbit_paspor'       => $npp[$key],
                    'tgl_lahir'             => $tgl[$key],
                    'tgl_paspor_dibuat'     => $intgl[$key],
                    'tgl_paspor_kadaluarsa' => $exptgl[$key],
                    'tgl_keberangkatan'     => $tglbrk,
                    'domisili'              => $domisili[$key],
                );
                $execute2 = $this->M_visapaspor->insertData('pengajuan_visa', $addData);
                if ($execute2 >= 1) {
                    $res = "ok";
                } else {
                    $res = "error";
                }
            }
            if ($res == "ok") {
                redirect('VisaPaspor/pencarianvisapaspor');
            } else {
                echo "GAGAL";
            }
        } else {
            echo "error";
        }
    }

    public function isidatapaspor()
    {
        $id_tipe_paspor = $this->input->post('tipe_paspor');
        $pDewasa        = $this->input->post('pDewasa');
        $pAnak          = $this->input->post('pAnak');
        $pBayi          = $this->input->post('pBayi');
        $imigrasi       = $this->input->post('imigrasi');
        $durasi         = $this->input->post('durasiPaspor');

        $userdata         = $this->session->userdata('userdata_login');
        $data['userdata'] = $userdata;

        $data['jenis_faq'] = $this->M_tour->get_jenis_faq()->result();
        $data['faq']       = $this->M_tour->get_faq()->result();

        $harga_dewasa = $this->M_visapaspor->getHargaPaspor("where id_kantor_imigrasi = $imigrasi and id_umur_paspor = 1")->row();
        $harga_anak   = $this->M_visapaspor->getHargaPaspor("where id_kantor_imigrasi = $imigrasi and id_umur_paspor = 2")->row();
        $harga_bayi   = $this->M_visapaspor->getHargaPaspor("where id_kantor_imigrasi = $imigrasi and id_umur_paspor = 3")->row();

        $data['id_tipe_paspor'] = $id_tipe_paspor;
        $data['pDewasa']        = $pDewasa;
        $data['pAnak']          = $pAnak;
        $data['pBayi']          = $pBayi;
        $data['dataDewasa']     = $harga_dewasa;
        $data['dataAnak']       = $harga_anak;
        $data['dataBayi']       = $harga_bayi;
        $data['imigrasi']       = $this->M_visapaspor->getKantorImigrasi("where id_kantor_imigrasi = $imigrasi")->row();
        $data['harga']          = ($pDewasa * $harga_dewasa->harga) + ($pAnak * $harga_anak->harga) + ($pBayi * $harga_bayi->harga);
        $data['durasi']         = $durasi;
        $data['negara']         = $this->M_visapaspor->getNegara("where hapus = 0")->result();

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('visapaspor/isidatapaspor', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function addPaspor()
    {
        // echo "<pre>";
        // echo print_r($this->input->post());
        // echo "</pre>";

        $id_tipe_paspor = $this->input->post('id_tipe_paspor');
        $genderPem      = $this->input->post('genderPem');
        $ndPem          = $this->input->post('ndPem');
        $nbpem          = $this->input->post('nbpem');
        $email          = $this->input->post('email');
        $nohp           = $this->input->post('nohp');

        $id_umur_paspor  = $this->input->post('id_umur_paspor');
        $id_harga_paspor = $this->input->post('id_harga_paspor');
        $perpanjangan    = $this->input->post('perpanjangan');
        $gender          = $this->input->post('gender');
        $nama            = $this->input->post('nama');
        $tgl             = $this->input->post('tgl');
        $kw              = $this->input->post('kw');
        $nopas           = $this->input->post('nopas');
        $npp             = $this->input->post('npp');
        $intgl           = $this->input->post('intgl');
        $extgl           = $this->input->post('extgl');
        $domisili        = $this->input->post('domisili');

        $id_inv_paspor = 'TFPAS' . time();

        $add1 = array(
            'id_inv_paspor'    => $id_inv_paspor,
            'id_tipe_paspor'   => $id_tipe_paspor,
            'id_status_paspor' => 1,
            'nama_depan'       => $ndPem,
            'nama_belakang'    => $nbpem,
            'no_telp'          => $nohp,
            'email'            => $email,
        );
        $execute = $this->M_visapaspor->insertData('inv_paspor', $add1);
        if ($execute >= 1) {
            foreach ($nama as $key => $value) {
                $add2 = array(
                    'id_inv_paspor'   => $id_inv_paspor,
                    'id_harga_paspor' => $id_harga_paspor[$key],
                    'perpanjangan'    => $perpanjangan[$key],
                    'nama'            => $gender . $value,
                    'tanggal_lahir'   => date('Y-m-d', strtotime($tgl[$key])),
                    'kewarganegaraan' => $kw[$key],
                    'no_paspor'       => $nopas[$key],
                    'negara_penerbit' => $npp[$key],
                    'tanggal_buat'    => $intgl[$key],
                    'tanggal_exp'     => $extgl[$key],
                    'domisili'        => $domisili[$key],
                );
                $execute2 = $this->M_visapaspor->insertData('paspor', $add2);
                if ($execute2 >= 1) {
                    $res = "ok";
                } else {
                    $res = "error";
                }
            }
            if ($res == "ok") {
                redirect('VisaPaspor/pencarianvisapaspor');
                // echo "SUKSES";
            } else {
                echo "GAGAL";
            }
        } else {
            echo "error";
        }
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserPage extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->library('image_lib');
        date_default_timezone_set("Asia/Bangkok");

    }
    public function editprofile()
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['profile']  = $this->M_user->get_user($user)->row();

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('user/edit-profile', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function pointsaya()
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['profile']  = $this->M_user->get_user($user)->row();

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('user/point-saya', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    // public function pointsaya()
    // {
    //     $this->load->view('header');
    //     $this->load->view('head');
    //     $this->load->view('point-saya');
    //     $this->load->view('footer');
    //     $this->load->view('footer-js');
    // }

    public function pesanansaya()
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['tagihan']  = $this->M_konf_pem->getAllPembayaran("where d.status != 'paid off' and d.id_member = '$user'")->result();
        $data['log']      = $this->M_konf_pem->getAllPembayaran("where d.status = 'paid off' and d.id_member = '$user'")->result();
        $data['visa']     = $this->M_konf_pem->getListPemVisa("where a.id_member = '$user' and b.id_status_visa = 4 group by a.id_pembayaran_visa")->result();
        // $data['detailVisa'] = $this->M_konf_pem->getListPemVisa("where a.id_member = '$user' and b.id_status_visa = 4")->result();
        $data['detailVisa']   = $this->M_konf_pem->getListPemVisa("where a.id_member = '$user'")->result();
        $data['logVisa']      = $this->M_konf_pem->getListPemVisa("where a.id_member = '$user' and b.id_status_visa != 4 group by a.id_pembayaran_visa")->result();
        $data['paspor']       = $this->M_konf_pem->getPaspor("where a.email = '$user' and g.id_status_paspor = 1 group by a.id_inv_paspor")->result();
        $data['detailPaspor'] = $this->M_konf_pem->getPaspor("where a.email = '$user'")->result();
        $data['logPaspor']    = $this->M_konf_pem->getPaspor("where a.email = '$user' and g.id_status_paspor != 1 group by a.id_inv_paspor")->result();
        $data['profile']      = $this->M_user->get_user($user)->row();

        $this->load->view('template/header', $data);
        $this->load->view('template/head', $data);
        $this->load->view('user/pesanan-saya', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function pengaturanakun()
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['profile']  = $this->M_user->get_user($user)->row();

        $this->load->view('template/header', $data);
        $this->load->view('template/head', $data);
        $this->load->view('user/pengaturan-akun', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function konfirmasiPembayaran()
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['profile']  = $this->M_user->get_user($user)->row();

        $this->load->view('template/header', $data);
        $this->load->view('template/head', $data);
        $this->load->view('user/konfirmasi-pembayaran', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function updateProfile()
    {
        $userdata      = $this->session->userdata('userdata_login');
        $email         = $userdata['usermember'];
        $nama          = $this->input->post("nama");
        $alamat        = $this->input->post("alamat");
        $no_hp         = $this->input->post("no_hp");
        $kota          = $this->input->post("kota");
        $tanggal_lahir = $this->input->post("tanggal_lahir");

        $config['upload_path']   = './uploads/fotoProfil/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('gambar')) {
            $updateData = array(
                'nama'          => $nama,
                'alamat'        => $alamat,
                'no_hp'         => $no_hp,
                'kota'          => $kota,
                'tanggal_lahir' => date("Y-m-d", strtotime($tanggal_lahir)),
            );
            $where   = array('id_member' => $email);
            $execute = $this->M_user->updateData('member', $updateData, $where);
            if ($execute >= 1) {
                redirect('UserPage/editprofile');
            } else {
                echo "error";
            }
            // $error = array('error' => $this->upload->display_errors());
            // print_r($error);
        } else {
            $data       = array('upload_data' => $this->upload->data());
            $updateData = array(
                'nama'          => $nama,
                'alamat'        => $alamat,
                'no_hp'         => $no_hp,
                'foto'          => base_url() . "uploads/fotoProfil/" . $this->upload->data('file_name'),
                'kota'          => $kota,
                'tanggal_lahir' => $tanggal_lahir,
            );
            $where   = array('id_member' => $email);
            $execute = $this->M_user->updateData('member', $updateData, $where);
            if ($execute >= 1) {
                redirect('UserPage/editprofile');
            } else {
                echo "error";
            }
        }
    }

    public function updatePassword()
    {
        $sandiLama = $this->input->post('sandiLama');
        $sandiBaru = $this->input->post('sandiBaru');
        $konfSandi = $this->input->post('konfSandi');
        $user      = $this->input->post('email');
        $cek       = $this->M_user->get_user($user)->row();
        if (md5($sandiLama) == $cek->password) {
            if ($sandiBaru == $konfSandi) {
                $updateData = array(
                    'password' => md5($sandiBaru),
                );
                $where   = array('id_member' => $user);
                $execute = $this->M_user->updateData('member', $updateData, $where);
                if ($execute >= 1) {
                    $this->session->set_flashdata('err', 'Password Telah Sukses Diganti');
                    redirect('UserPage/pengaturanakun');
                } else {
                    $this->session->set_flashdata('err', 'Password Gagal Diganti');
                    redirect('UserPage/pengaturanakun');
                }
            } else {
                $this->session->set_flashdata('err', 'Pastikan Password Baru Tidak Sama');
                redirect('UserPage/pengaturanakun');
            }
        } else {
            $this->session->set_flashdata('err', 'Password Lama Tidak Sesuai');
            redirect('UserPage/pengaturanakun');
        }
    }

    public function konfVisa()
    {
        $id_pembayaran_visa = $this->input->post('id_pembayaran_visa');

        $config['upload_path']   = './uploads/buktiTFVisa/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('gambar')) {
            echo "tidak ada gambar";
            // $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $upd1 = array(
                'time_konf' => date('Y-m-d H:i:s'),
                // 'status'    => 'valid',
                'bukti'     => base_url() . "uploads/buktiTFVisa/" . $this->upload->data('file_name'),
            );
            $where    = array('id_pembayaran_visa' => $id_pembayaran_visa);
            $execute1 = $this->M_konf_pem->updateData('pembayaran_visa', $upd1, $where);
            if ($execute1 >= 1) {
                redirect('UserPage/pesanansaya');
                // $upd2 = array(
                //     'id_status_visa' => 1,
                // );
                // $where    = array('id_pembayaran_visa' => $id_pembayaran_visa);
                // $execute2 = $this->M_konf_pem->updateData('pengajuan_visa', $upd2, $where);
                // if ($execute2 >= 1) {
                //     redirect('UserPage/pesanansaya');
                // } else {
                //     echo "GAGAL";
                // }
            } else {
                echo "error";
            }
        }
    }

    public function konfPaspor()
    {
        $id_inv_paspor = $this->input->post('id_inv_paspor');

        $config['upload_path']   = './uploads/buktiTFPaspor/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = 2048;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('gambar1')) {
            echo "tidak ada gambar";
            // $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());
            $upd1 = array(
                'time_konf' => date('Y-m-d H:i:s'),
                'bukti'     => base_url() . "uploads/buktiTFPaspor/" . $this->upload->data('file_name'),
                'id_status_paspor' => 5,
            );
            $where    = array('id_inv_paspor' => $id_inv_paspor);
            $execute1 = $this->M_konf_pem->updateData('inv_paspor', $upd1, $where);
            if ($execute1 >= 1) {
                redirect('UserPage/pesanansaya');
            } else {
                echo "error";
            }
        }
    }

}

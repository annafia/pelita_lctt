<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {
	public function  __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('image_lib');
		date_default_timezone_set("Asia/Bangkok");
		
	}
	public function faq()
	{
		$userdata= $this->session->userdata('userdata_login');
		$data['userdata']= $userdata;
		$data['jenis_faq']= $this->M_tour->get_jenis_faq()->result();
		$data['faq']= $this->M_tour->get_faq()->result();
		$this->load->view('template/header');
		$this->load->view('template/head', $data);
		$this->load->view('template/faq', $data);
		$this->load->view('template/footer', $data);
		$this->load->view('template/footer-js', $data);
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Info extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->library('image_lib');
        date_default_timezone_set("Asia/Bangkok");

    }

    public function index()
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['event']    = $this->M_info->getEvent("where hapus = 0")->result();
        $data['berita']   = $this->M_info->getBerita("where hapus = 0")->result();
        $data['promo']    = $this->M_info->getPromo("where hapus = 0")->result();

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('beritapromoevent/berita-promo-event', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function detailEvent($id)
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['event']    = $this->M_info->getEvent("where id_event = $id and hapus = 0")->row();

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('beritapromoevent/detail-event', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function detailBerita($id)
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['berita']   = $this->M_info->getBerita("where id_berita = $id and hapus = 0")->row();

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('beritapromoevent/detail-berita', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

    public function detailPromo($id)
    {
        $userdata = $this->session->userdata('userdata_login');
        $user     = $userdata['usermember'];

        $data['userdata'] = $userdata;
        $data['promo']    = $this->M_info->getPromo("where id_slider = $id and hapus = 0")->row();
        $data['tour']     = $this->M_info->getTour("where a.hapus = 0 limit 4")->result();

        // echo "<pre>";
        // print_r($data['tour']);
        // echo "</pre>";

        $this->load->view('template/header');
        $this->load->view('template/head', $data);
        $this->load->view('beritapromoevent/detail-promo', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengisianData extends CI_Controller {
  public function  __construct(){
    parent:: __construct();
    $this->load->helper('url');
    $this->load->library('session');
    $this->load->library('upload');
    $this->load->library('image_lib');
    date_default_timezone_set("Asia/Bangkok");
    
  }
  public function isidata()
  {
    $this->load->view('template/header');
    $this->load->view('template/head');
    $this->load->view('isi-data');
    $this->load->view('template/footer');
    $this->load->view('template/footer-js');
  }

}

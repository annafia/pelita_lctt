<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tour extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');
        $this->load->library('image_lib');
        date_default_timezone_set("Asia/Bangkok");

    }
    public function detailtour($id_paket_tour, $tanggal_keberangkatan = null)
    {
        $userdata         = $this->session->userdata('userdata_login');
        $data['userdata'] = $userdata;
        if ($id_paket_tour != null) {
            $data_tour = $this->M_tour->data_tour($id_paket_tour);
            if ($data_tour->row() == null) {
                redirect("Home");
                exit();
            }
            $data['data_tour']   = $data_tour->row();
            $data['itinerary']   = $this->M_tour->itinerary_tour($id_paket_tour)->result();
            $data['harga_tour']  = $this->M_tour->harga_tour($id_paket_tour, $tanggal_keberangkatan)->result();
            $data['gambar_tour'] = $this->M_tour->gambar_tour($id_paket_tour)->result();
            $data['paket_tour']  = $this->M_tour->get_paket(null, $data_tour->row()->id_negara, null, " limit 3")->result();
            // $data['komentar']= $this->M_tour->get_komentar($id_paket_tour)->result();
            $this->load->view('template/header', $data);
            $this->load->view('template/head', $data);
            $this->load->view('tour/detail-tour2', $data);
            $this->load->view('template/footer', $data);
            $this->load->view('template/footer-js', $data);
        } else {
            redirect("Home");
        }
    }

    public function pencariantour()
    {
        $userdata           = $this->session->userdata('userdata_login');
        $data['userdata']   = $userdata;
        $data['paket_tour'] = "";
        // $data['paket_tour'] = $this->M_tour->get_paket("", "", "asc", 'limit 12')->result();
        if ($this->input->post()) {
            $tanggal            = $this->input->post('tanggal');
            $negara             = $this->input->post('negara');
            $filter_tampilan    = $this->input->post('filter_tampilan');
            $data['paket_tour'] = $this->M_tour->get_paket($tanggal, $negara, $filter_tampilan, 'limit 12')->result();
        }
        $data['negara'] = $this->M_tour->get_negara()->result();
        $data['benua'] = $this->M_tour->get_benua();
        $this->load->view('template/header', $data);
        $this->load->view('template/head', $data);
        $this->load->view('tour/pencarian-tour', $data);
        $this->load->view('template/footer', $data);
        $this->load->view('template/footer-js', $data);
        // echo print_r($data['paket_tour']);
    }
    public function getPencarian()
    {
        if ($this->input->post()) {
            // $tanggal            = $this->input->post('tanggal');
            $end             = $this->input->post('end');
            $start             = $this->input->post('start');
            $benua             = $this->input->post('benua');
            $negara             = $this->input->post('negara');
            $filter_tampilan    = $this->input->post('filter_tampilan');
            $data['paket_tour'] = $this->M_tour->get_paket($start, $end, $benua, $negara, $filter_tampilan, ' limit 12')->result();
        } else {
            $data['paket_tour'] = $this->M_tour->get_paket(null, null, null, null, null, ' limit 12')->result();
        }
        // echo "<pre>";
        // echo print_r($this->input->post());
        // echo "</pre>";
        $this->load->view('tour/div-pencarian', $data);
    }
    public function find_negara(){
        $benua             = $this->input->post('benua');
        
        $data = json_encode($this->M_tour->find_negara($benua));
        $this->output->set_content_type("application/json");
        $this->output->set_output($data);
    }
    public function booking($id_jadwal_tour, $id_harga_tour)
    {
        $userdata         = $this->session->userdata('userdata_login');
        $data['userdata'] = $userdata;
        if ($userdata != null) {
            $data['data_tour']    = $this->M_tour->get_data_tour($id_jadwal_tour, $id_harga_tour)->row();
            $data['data_pemesan'] = $this->M_user->get_user($userdata['usermember'])->row();
            if ($this->input->post()) {
                $lhs          = "LHS" . time();
                $booking_data = $this->input->post();
                // $list_hold= $this->M_tour->i_list_hold($userdata['usermember'], $id_harga_tour, $data['data_tour']->dewasa)->row();
                $list_hold = $this->M_tour->i_list_hold($lhs, $userdata['usermember'], $id_harga_tour, $data['data_tour']->dewasa);
                if ($list_hold >= 1) {
                    $nomor_id      = $this->input->post('nomor_id');
                    $nama_lengkap  = $this->input->post('nama_lengkap');
                    $tanggal_lahir = $this->input->post('tanggal_lahir');
                    $no_hp         = $this->input->post('no_hp');

                    foreach ($nama_lengkap as $key => $value) {
                        $keluarga = $this->M_tour->i_keluarga($lhs, $value, "a", "", $tanggal_lahir[$key], $no_hp[$key], $nomor_id[$key], "");
                    }
                    if ($keluarga >= 1) {
                    	redirect("Home");
                    }
                }

                // echo "<pre>";
                // print_r($list_hold);
                // echo "</pre>";
                // exit();
            }
            $this->load->view('template/header', $data);
            $this->load->view('template/head', $data);
            $this->load->view('tour/isi-data', $data);
            $this->load->view('template/footer', $data);
            $this->load->view('template/footer-js', $data);
        } else {
            redirect('Home');
        }
    }

    public function cek()
    {
    	$in = strtotime('2018-08-12 18:21:10');
    	$out = strtotime('2018-08-12 19:21:10');
    	echo $in."/".$out."=".time();
    	echo "<hr>";
    	if (time() < $in) {
    		echo "in";
    	} else {
    		echo "out";
    	}
    }

}
